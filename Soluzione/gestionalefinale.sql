-- phpMyAdmin SQL Dump
-- version 4.1.6
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1:3307
-- Generation Time: Ott 19, 2018 alle 11:03
-- Versione del server: 5.6.16
-- PHP Version: 5.5.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `gestionalefinale`
--

-- --------------------------------------------------------

--
-- Struttura della tabella `admin`
--

CREATE TABLE IF NOT EXISTS `admin` (
  `id` int(12) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL DEFAULT '',
  `cognome` varchar(255) NOT NULL DEFAULT '',
  `azienda` text NOT NULL,
  `email` varchar(200) NOT NULL,
  `username` varchar(200) NOT NULL,
  `password` varchar(200) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`),
  UNIQUE KEY `password` (`password`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dump dei dati per la tabella `admin`
--

INSERT INTO `admin` (`id`, `name`, `cognome`, `azienda`, `email`, `username`, `password`) VALUES
(4, 'Flavio', 'Coltelli', 'brainmood', 'flaviocolt@libero.it', 'Flavio', '098f6bcd4621d373cade4e832627b4f6');

-- --------------------------------------------------------

--
-- Struttura della tabella `brand`
--

CREATE TABLE IF NOT EXISTS `brand` (
  `id` int(12) NOT NULL AUTO_INCREMENT,
  `clienti_id` int(12) NOT NULL,
  `azienda` text NOT NULL,
  `settore` text NOT NULL,
  `email` text NOT NULL,
  `telefono` text,
  `descrizione` text,
  PRIMARY KEY (`id`),
  KEY `clienti_id` (`clienti_id`),
  FULLTEXT KEY `azienda` (`azienda`,`settore`,`email`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=53 ;

--
-- Dump dei dati per la tabella `brand`
--

INSERT INTO `brand` (`id`, `clienti_id`, `azienda`, `settore`, `email`, `telefono`, `descrizione`) VALUES
(49, 22, 'Fiat', 'Automobilistico2', 'info@fashion.com', '3333333333', 'Azienda di grande spessore in campo automobilistico '),
(52, 22, 'Ferrari', 'Automobilistico', 'info@fashion.com', '333333335', 'Automobili di Lusso');

-- --------------------------------------------------------

--
-- Struttura della tabella `clienti`
--

CREATE TABLE IF NOT EXISTS `clienti` (
  `id` int(12) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL DEFAULT '',
  `cognome` varchar(255) NOT NULL DEFAULT '',
  `indirizzo` text NOT NULL,
  `iva` text NOT NULL,
  `telefono` text NOT NULL,
  `email` varchar(200) NOT NULL,
  PRIMARY KEY (`id`),
  FULLTEXT KEY `name` (`name`,`cognome`,`email`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=23 ;

--
-- Dump dei dati per la tabella `clienti`
--

INSERT INTO `clienti` (`id`, `name`, `cognome`, `indirizzo`, `iva`, `telefono`, `email`) VALUES
(22, 'agnelli', 'Prean', 'via prampolin', 'prova1', '33455555', 'info@elisabettamalara.com');

-- --------------------------------------------------------

--
-- Struttura della tabella `data_task`
--

CREATE TABLE IF NOT EXISTS `data_task` (
  `id` int(11) NOT NULL,
  `id_task` int(11) NOT NULL,
  `data_inizio` varchar(255) NOT NULL,
  `data_scadenza` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id_task` (`id_task`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struttura della tabella `dipendenti`
--

CREATE TABLE IF NOT EXISTS `dipendenti` (
  `id` int(12) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `cognome` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `telefono` varchar(255) DEFAULT NULL,
  `codfiscale` varchar(255) DEFAULT NULL,
  `settore` varchar(255) DEFAULT NULL,
  `ruolo` text NOT NULL,
  `password` varchar(600) NOT NULL,
  PRIMARY KEY (`id`),
  FULLTEXT KEY `name` (`name`,`cognome`,`settore`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=10 ;

--
-- Dump dei dati per la tabella `dipendenti`
--

INSERT INTO `dipendenti` (`id`, `name`, `cognome`, `email`, `telefono`, `codfiscale`, `settore`, `ruolo`, `password`) VALUES
(9, 'Paolo', 'Ruffini', 'flaviocolt@libero.it', '+39 3386341758', 'Provadaniele4567', 'Officina', 'Meccanico', '189bbbb00c5f1fb7fba9ad9285f193d1');

-- --------------------------------------------------------

--
-- Struttura della tabella `numero_servizi`
--

CREATE TABLE IF NOT EXISTS `numero_servizi` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `numero` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=11 ;

--
-- Dump dei dati per la tabella `numero_servizi`
--

INSERT INTO `numero_servizi` (`id`, `numero`) VALUES
(10, 1);

-- --------------------------------------------------------

--
-- Struttura della tabella `relazione_dip_task`
--

CREATE TABLE IF NOT EXISTS `relazione_dip_task` (
  `id_dip` int(11) NOT NULL DEFAULT '0',
  `id_task` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id_dip`,`id_task`),
  KEY `task_id_external` (`id_task`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `relazione_dip_task`
--

INSERT INTO `relazione_dip_task` (`id_dip`, `id_task`) VALUES
(9, 7),
(9, 8);

-- --------------------------------------------------------

--
-- Struttura della tabella `servizi`
--

CREATE TABLE IF NOT EXISTS `servizi` (
  `id` int(12) NOT NULL AUTO_INCREMENT,
  `name` text NOT NULL,
  PRIMARY KEY (`id`),
  FULLTEXT KEY `name` (`name`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=14 ;

--
-- Dump dei dati per la tabella `servizi`
--

INSERT INTO `servizi` (`id`, `name`) VALUES
(10, 'siti web'),
(12, 'sviluppo softwer gestionali'),
(13, 'Sviluppo Sql');

-- --------------------------------------------------------

--
-- Struttura della tabella `servizi_brand`
--

CREATE TABLE IF NOT EXISTS `servizi_brand` (
  `id` int(12) NOT NULL AUTO_INCREMENT,
  `brand_id` int(12) NOT NULL,
  `servizi_id` int(11) NOT NULL,
  `prezzo` decimal(10,0) NOT NULL,
  `stato_pag` varchar(255) NOT NULL,
  `data_inizio` text NOT NULL,
  `data_fine` text NOT NULL,
  `stato_serv` text NOT NULL,
  PRIMARY KEY (`id`),
  KEY `brand_id` (`servizi_id`),
  KEY `clienti_id` (`brand_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Dump dei dati per la tabella `servizi_brand`
--

INSERT INTO `servizi_brand` (`id`, `brand_id`, `servizi_id`, `prezzo`, `stato_pag`, `data_inizio`, `data_fine`, `stato_serv`) VALUES
(5, 49, 10, '80', 'Saldato Acconto', '12-07-2015', '', 'passato'),
(6, 49, 10, '40', 'Non Saldato', '12-07-2015', '18-10-2015', 'attivo');

-- --------------------------------------------------------

--
-- Struttura della tabella `task`
--

CREATE TABLE IF NOT EXISTS `task` (
  `id` int(12) NOT NULL AUTO_INCREMENT,
  `brand_id` int(12) NOT NULL,
  `dipend_id` int(11) NOT NULL,
  `destinatario` text NOT NULL,
  `titolo` varchar(255) NOT NULL,
  `descrizione` text NOT NULL,
  `stato` text NOT NULL,
  `data_inizio` varchar(255) NOT NULL,
  `data_scadenza` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `brand_id` (`brand_id`),
  KEY `dipend_id` (`dipend_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=10 ;

--
-- Dump dei dati per la tabella `task`
--

INSERT INTO `task` (`id`, `brand_id`, `dipend_id`, `destinatario`, `titolo`, `descrizione`, `stato`, `data_inizio`, `data_scadenza`) VALUES
(7, 49, 0, '', 'Task prova2', '-prova1\r\n-prova2\r\n-prova3\r\n', '', '15/10/15', '15/11/15'),
(8, 49, 0, '', 'Inizia campagna adwords', '-prova1\r\n-prova2', '', '', ''),
(9, 49, 0, '', 'nuovo sito', 'test', '', '', '');

--
-- Limiti per le tabelle scaricate
--

--
-- Limiti per la tabella `brand`
--
ALTER TABLE `brand`
  ADD CONSTRAINT `cliente_id_external` FOREIGN KEY (`clienti_id`) REFERENCES `clienti` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Limiti per la tabella `data_task`
--
ALTER TABLE `data_task`
  ADD CONSTRAINT `datatask_id_external` FOREIGN KEY (`id_task`) REFERENCES `task` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Limiti per la tabella `relazione_dip_task`
--
ALTER TABLE `relazione_dip_task`
  ADD CONSTRAINT `dipendenti_id_external` FOREIGN KEY (`id_dip`) REFERENCES `dipendenti` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `task_id_external` FOREIGN KEY (`id_task`) REFERENCES `task` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Limiti per la tabella `servizi_brand`
--
ALTER TABLE `servizi_brand`
  ADD CONSTRAINT `bran_id_external` FOREIGN KEY (`brand_id`) REFERENCES `brand` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Limiti per la tabella `task`
--
ALTER TABLE `task`
  ADD CONSTRAINT `brand_id_external` FOREIGN KEY (`brand_id`) REFERENCES `brand` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
