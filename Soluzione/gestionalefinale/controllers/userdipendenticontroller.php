<?php
class userDipendenteController{
	public $username ='';
	private $_model = NULL;
	private $logged = false;
	public $user_dip_pass = '';

	public function __construct(){
		//session_start();
		$this->_model = new usermodel();
		$this->_startSecureSession();

		if($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_GET['action']) && $_GET['action'] == 'login'){
			$username = isset($_POST['username'])?$_POST['username']:false;
			$password = isset($_POST['password'])?$_POST['password']:false;
			$id = $this-> _model->getuserDipendente_id( $username,$password );
			if($username != false && $password != false && $id != false){
				$this->username = $username;
				$this->logged = true;

				$_SESSION['username'] = $username;
				$_SESSION['logged2'] = true;
				$_SESSION['message'] = 'Benvenuto';
			}else{
                $_SESSION['message'] = 'Login non riuscito';
			}
		}
		elseif(isset($_GET['action']) && $_GET['action']=='logout'){
			unset($_SESSION['username']);
			unset($_SESSION['logged2']);
			$_SESSION['message'] = 'Buona Giornata';
			header('Location:./login.php');
		}
		elseif(isset($_SESSION['username'])&& isset($_SESSION['logged2'])&& $_SESSION['logged2']){
			$this->username = $_SESSION['username'];
			$this->logged = true;
		}
		$this->redirecttodipendentearea(isset($id[0]['id'])?$id[0]['id']:'');
	}

	
	public function islogged(){
		return $this->logged;
	}
	public function redirecttodipendentearea($id){
		$script_file = basename($_SERVER['SCRIPT_NAME']);

		if($this->islogged() && $script_file == 'login2.php'){
			header('location:./dipendenti.php?action=detail&dipendente_id='.$id);
			die();
		}
		elseif(!$this->islogged() && ($script_file == 'dipendenti.php' && isset($GET_['action']) && $GET_['action']!='detail')){
			header('location:./login2.php');
			die();
		}
	}
	private function _startSecureSession(){
		ini_set('session.use_trans_sid', 0);
		ini_set('session.use_only_cookies', 1);

		$session_name ='sessionId';
		$cookie_defaults =session_get_cookie_params();
		session_set_cookie_params(
            0,
            $cookie_defaults['path'],
            $cookie_defaults['domain'],
            false,
            true
			);
			session_name($session_name);
			session_start();
			session_regenerate_id(true);	
	}
}