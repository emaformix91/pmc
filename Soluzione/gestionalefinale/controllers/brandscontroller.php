<?php
class brandscontroller extends SuperControllers {

	private $_model = NULL;

	public function __construct(){
		$this-> _model = new brandsmodel;
	}

	public function index(){
		global $user_controller;

		$brands = $this-> _model->getBrands();
		$clienti = $this-> _model->getCliente();
		if(isset( $_POST['submit'] )){
			$f = fopen("file.txt", "a+");
		$brand['cliente_id'] = (! empty($_POST['cliente_id'])) ? $_POST['cliente_id']:false;
        $brand['bazienda'] = (! empty($_POST['bazienda'])) ? $_POST['bazienda']:false;
        $brand['bsettore'] = (! empty($_POST['bsettore'])) ? $_POST['bsettore']:false;
        $brand['bemail'] = (! empty($_POST['bemail'])) ? $_POST['bemail']:false;
        $brand['btelefono'] = (! empty($_POST['btelefono'])) ? $_POST['btelefono']:false;
        $brand['bdescrizione'] = (! empty($_POST['bdescrizione'])) ? $_POST['bdescrizione']:false;
        	if(!($this->check_leng_string($brand['bazienda'],1,255))||!($this->check_string($brand['bazienda']))){fwrite($f, "1");
            $_SESSION['message'] = 'Il nome del cliente e troppo lungo o contiene caratteri non supportati';
            header('Location:./brands.php');
            return;
          	}
          	if(!($this->check_leng_string($brand['bsettore'],1,255))||!($this->check_string($brand['bsettore']))){fwrite($f, "2");
            $_SESSION['message'] = 'Il settore del cliente e troppo lungo o contiene caratteri non supportati';
            header('Location:./brands.php');
              return;
          	}
          	if(!($this->check_leng_string($brand['bemail'],1,255))||!($this->check_mail($brand['bemail']))){fwrite($f, "3");
            $_SESSION['message'] = 'email del cliente e troppo lungo o contiene caratteri non supportati';
            header('Location:./brands.php');
              return;
          	}
          	if(!($this->check_leng_string($brand['btelefono'],1,14))||!($this->check_telefono($brand['btelefono']))){fwrite($f, "4");
            $_SESSION['message'] = 'Il telefono del cliente e troppo lungo o contiene caratteri non supportati';
            header('Location:./brands.php');
              return;
          	}
       		else{
          	$brands[] = $this->_model->AddBrand(
          	$brand['cliente_id'], $brand['bazienda'], $brand['bsettore'], $brand['bemail'],$brand['btelefono'], $brand['bdescrizione']);
          	$_SESSION['message'] = 'brand inserito correttamente';
          	header('Location:./brands.php');
            return;
       		}
		}
		if( isset($_POST['update'])){
			if( !empty($brands) ){
				$brands = $_POST['brands'];
        foreach ($brands as  $brand) {
          if(!($this->check_leng_string($brand['email'],1,255))||!($this->check_mail($brand['email']))){fwrite($f, "3");
            $_SESSION['message'] = 'email  e troppo lunga o contiene caratteri non supportati';
            header('Location:./brands.php');
              return;
            }
            if(!($this->check_leng_string($brand['azienda'],1,255))||!(check_stringOneSpace($brand['azienda']))){fwrite($f, "3");
            $_SESSION['message'] = 'azienda  e troppo lunga o contiene caratteri non supportati';
            header('Location:./brands.php');
              return;
            }
            if(!($this->check_leng_string($brand['settore'],1,255))||!($this->check_stringOneSpace($brand['settore']))){fwrite($f, "3");
            $_SESSION['message'] = 'settore  e troppo lunga o contiene caratteri non supportati';
            header('Location:./brands.php');
              return;
            }
        }
				$this-> _model->UpdateBrands($brands);
				$_SESSION['message'] = 'Brand modificato correttamente';
          		header('Location:./brands.php');
            	return;
			}
		}
		if( isset($_POST['delete']) ){
			if( !empty($_POST['id']) ){
				$id = $_POST['id'] ;
				$this-> _model->DeleteBrand($id);
				$_SESSION['message'] = 'Brand eliminato correttamente';
          		header('Location:./brands.php');
            	return;
			}
			else{
				$_SESSION['message'] = 'Selezionare Brand da eliminare';
          		header('Location:./brands.php');
            	return;
			}
		}
    if(isset($_POST['cerca'])){
             $f = fopen("cerca.txt", "a+");
              $brand['azienda'] = ( !empty($_POST['azienda'])) ? $_POST['azienda'] : false;
              $brand['settore'] = ( !empty($_POST['settore'])) ? $_POST['settore'] : false;
              $brand['email'] = ( !empty($_POST['email'])) ? $_POST['email'] : false;
       $brands = $this-> _model->serch_brand( $brand['azienda'],$brand['settore'],$brand['email'] );
       if(!empty($brands)){
        header('Refresh:./brands.php');
        $_SESSION['message'] = 'ricerca riuscita'; 
       }
      else{
          header('Refresh:./brands.php');
        $_SESSION['message'] = 'non ci sono corrispondenze';
        } 
    }
		include('./views/brands_index.php');
	}
}