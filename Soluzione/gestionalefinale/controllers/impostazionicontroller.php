<?php
class impostazionicontroller extends SuperControllers{
	private $_model = NULL;
	public function __construct(){
		$this->_model = new impostazionimodel();
	}
	public function index(){
		global $user_controller;
		$admindate = $this-> _model->getAdmin();
		if(isset($_POST['submit1'])){
			$id = $_POST['id'];
			$admin['name'] = ( !empty($_POST['name']) ) ? $_POST['name'] : false;
      		$admin['cognome'] = ( !empty($_POST['cognome']) ) ? $_POST['cognome'] : false;
      		$admin['azienda'] = ( !empty($_POST['azienda']) ) ? $_POST['azienda'] : false;
      		$admin['email'] = ( !empty($_POST['email']) ) ? $_POST['email'] : false;
      		$admin['username'] = ( !empty($_POST['username']) ) ? $_POST['username'] : false;
      		$admin['password'] = ( !empty($_POST['password']) ) ? $_POST['password'] : false;
          if(!($this->check_leng_string($admin['name'],1,255))||!($this->check_string($admin['name']))){
            $_SESSION['message'] = 'Il nome del cliente e troppo lungo o contiene caratteri non supportati';
            header('Location:./impostazioni.php');
            return;
            }
            if(!($this->check_leng_string($admin['cognome'],1,255))||!($this->check_string($admin['cognome']))){
            $_SESSION['message'] = 'Il settore e troppo lungo o contiene caratteri non supportati';
            header('Location:./impostazioni.php');
              return;
            }
            if(!($this->check_leng_string($admin['email'],1,255))||!($this->check_mail($admin['email']))){
            $_SESSION['message'] = 'email  troppo lunga o contiene caratteri non supportati';
            header('Location:./impostazioni.php');
              return;
            }
            if(!($this->check_leng_string($admin['username'],1,255))||!($this->check_string($admin['username']))){
            $_SESSION['message'] = 'Username  errata';
            header('Location:./impostazioni.php');
              return;
            }
            if(!($this->check_leng_string($admin['azienda'],1,255))||!($this->check_stringOneSpace($admin['azienda']))){
            $_SESSION['message'] = 'il campo azienda contiene caratteri non supportati';
            header('Location:./impostazioni.php');
              return;
            }
      		if($admin['password']!=false){
        		$admindate = $this-> _model->Updateadmin($admin['name'],
                                                      $admin['cognome'],
                                                      $admin['email'],
                                                      $admin['azienda'],
                                                      $admin['username'],
                                                      $admin['password'],
                                                      $id
                                                      );
      		}
      		else{
        		$admindate = $this-> _model->Updateadmin_whithsamepass($admin['name'],
                                                      $admin['cognome'],
                                                      $admin['email'],
                                                      $admin['azienda'],
                                                      $admin['username'],
                                                      $id
                                                      );
      		}

      
        	$_SESSION['message'] = 'update riuscito';
        	header('Location:./impostazioni.php');
        	return;
		}
		
				include('./views/impostazioni_index.php');
	}
}
?>