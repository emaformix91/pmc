<?php
class brandcontroller extends SuperControllers {
	private $_model = NULL;

	public function __construct(){
	$this->_model = new brandmodel();
	}
	public function getbrand($cliente_id){
		global $user_controller;
	return $this->_model->getbrand($cliente_id);
	}
	public function addbrand(){
		$f = fopen("file.txt", "a+");
		global $user_controller;
      	if($_SERVER['REQUEST_METHOD']=='POST'){
      	$brand['cliente_id'] = (! empty($_POST['cliente_id'])) ? $_POST['cliente_id']:false;
        $brand['bazienda'] = (! empty($_POST['bazienda'])) ? $_POST['bazienda']:false;
        $brand['bsettore'] = (! empty($_POST['bsettore'])) ? $_POST['bsettore']:false;
        $brand['bemail'] = (! empty($_POST['bemail'])) ? $_POST['bemail']:false;
        $brand['btelefono'] = (! empty($_POST['btelefono'])) ? $_POST['btelefono']:false;
        $brand['bdescrizione'] = (! empty($_POST['bdescrizione'])) ? $_POST['bdescrizione']:false;
        	if(!($this->check_leng_string($brand['bazienda'],1,255))||!($this->check_string($brand['bazienda']))){fwrite($f, "1");
            $_SESSION['message'] = 'Il nome del cliente e troppo lungo o contiene caratteri non supportati';
            header('Location:./clienti.php?action=detail&cliente_id='.$brand['cliente_id']);
            return;
          	}
          	if(!($this->check_leng_string($brand['bsettore'],1,255))||!($this->check_string($brand['bsettore']))){fwrite($f, "2");
            $_SESSION['message'] = 'Il settore e troppo lungo o contiene caratteri non supportati';
            header('Location:./clienti.php?action=detail&cliente_id='.$brand['cliente_id']);
              return;
          	}
          	if(!($this->check_leng_string($brand['bemail'],1,255))||!($this->check_mail($brand['bemail']))){fwrite($f, "3");
            $_SESSION['message'] = 'email del cliente e troppo lunga o contiene caratteri non supportati';
            header('Location:./clienti.php?action=detail&cliente_id='.$brand['cliente_id']);
              return;
          	}
          	if(!($this->check_leng_string($brand['btelefono'],1,14))||!($this->check_telefono($brand['btelefono']))){fwrite($f, "4");
            $_SESSION['message'] = 'Il telefono  e errato';
            header('Location:./clienti.php?action=detail&cliente_id='.$brand['cliente_id']);
              return;
          	}
       		else{
          	$brands[] = $this->_model->addbrand(
          	$brand['cliente_id'], $brand['bazienda'], $brand['bsettore'], $brand['bemail'], $brand['btelefono'], $brand['bdescrizione']);
          	$_SESSION['message'] = 'brand inserito correttamente';
          	header('Location:./clienti.php?action=detail&cliente_id='.$brand['cliente_id']);
            return;
       		}
		}
	}
  public function updatebrands(){
    global $user_controller;
    if($_SERVER['REQUEST_METHOD']=='POST'){
      $f = fopen("update2_index.txt", "a+");
          if(!empty($_POST['brands'])){
            $brands = $_POST['brands'];
            foreach ($brands as $id => $brand) {
              if(!($this->check_leng_string($brand['azienda'],1,255))||!($this->check_stringOneSpace($brand['azienda']))){fwrite($f, "1");
                $_SESSION['message'] = 'nome non valido';
                return;
              }
              elseif(!($this->check_leng_string($brand['settore'],1,255))||!($this->check_stringOneSpace($brand['settore']))){fwrite($f, "2");
                return;
              }
              elseif(!($this->check_leng_string($brand['email'],1,255))||!($this->check_mail($brand['email']))){fwrite($f, "3");
                $_SESSION['message'] = 'email non valida';
                return;
              }
            }
            $this->_model->updatebrands($brands);
            $_SESSION['message'] = 'update riuscito';
            return;
          }
          else{
            $_SESSION['message'] = 'update non riuscito';
            return;
          }
    }
  }
  public function deletebrand(){
    global $user_controller;
    if($_SERVER['REQUEST_METHOD']=='POST'){
      $id = (!empty($_POST['id'])) ? $_POST['id']:false;
      if($id === false){
        $_SESSION['message'] = 'selezionare brand da eliminare';
        return;
      }
      else{
            $this->_model->deletebrand($id);
            $this->_model->deletedati_brand($id);
            $_SESSION['message'] = 'brand eliminato correttamente';
            return;
      }
    }
  }
	public function detail(){
		global $user_controller;
  		$id = ( isset( $_GET['brand_id'] )&& intval( $_GET['brand_id'] )!= 0) ? $_GET['brand_id']:false;
      $admin = $this-> _model->getAdminEmail();
  		if($id === false){
        	echo "<p>errore dettagli cliente non inseriti nel database</p>";
        	return;
        }
      if(isset($_POST['submit'])){
        $f = fopen("controller.txt", "a+");
        $servizio['brand_id'] = (! empty($_POST['brand_id'])) ? $_POST['brand_id']:false;
        $servizio['servizi_id'] = (! empty($_POST['servizi_id'])) ? $_POST['servizi_id']:false;
        $servizio['prezzo'] = (! empty($_POST['prezzo'])) ? $_POST['prezzo']:false;
        $servizio['stato_pag'] = (! empty($_POST['stato_pag'])) ? $_POST['stato_pag']:false;
        $servizio['data_inizio'] = (! empty($_POST['data_inizio'])) ? $_POST['data_inizio']:false;
        $servizio['stato_serv'] = (! empty($_POST['stato_serv'])) ? $_POST['stato_serv']:false;
          if(!($this->check_string($servizio['brand_id']))){fwrite($f, "1");
            $_SESSION['message'] = 'operazione non valida';
            header('Location:./brand.php?action=detail&cliente_id='.$id);
            return;
            }
            if(!($this->check_string($servizio['servizi_id']))){fwrite($f, "2");
            $_SESSION['message'] = 'Contattare un tecnico php';
             header('Location:./brand.php?action=detail&brand_id='.$id);
             return;
            }
            if(!($this->check_leng_string($servizio['prezzo'],1,255))||!($this->check_string($servizio['prezzo']))){fwrite($f, "3");
            $_SESSION['message'] = 'Il prezzo contiene caratteri non supportati';
              header('Location:./brand.php?action=detail&brand_id='.$id);
              return;
            }
            if(!($this->check_leng_string($servizio['stato_pag'],1,255))||!($this->check_stringOneSpace($servizio['stato_pag']))){fwrite($f, "4");
            $_SESSION['message'] = 'caratteri non supportati';
              header('Location:./brand.php?action=detail&brand_id='.$id);
              return;
            }
            if(!($this->check_leng_string($servizio['data_inizio'],1,12))||!($this->check_string($servizio['data_inizio']))){fwrite($f, "5");
            $_SESSION['message'] = 'caratteri non supportati';
              header('Location:./brand.php?action=detail&brand_id='.$id);
              return;
            }
            if(!($this->check_leng_string($servizio['stato_serv'],1,255))||!($this->check_stringOneSpace($servizio['stato_serv']))){fwrite($f, "6");
            $_SESSION['message'] = 'contiene caratteri non supportati';
              header('Location:./brand.php?action=detail&brand_id='.$id);
              return;
            }
          else{
            $servizi = $this->_model->addserv(
            $servizio['brand_id'], $servizio['servizi_id'], $servizio['prezzo'], $servizio['stato_pag'], $servizio['data_inizio'], $servizio['stato_serv']);
            $_SESSION['message'] = 'brand inserito correttamente';
            header('Location:./brand.php?action=detail&brand_id='.$id);
            return;
          }
        }
        if(isset($_POST['submit2'])){
        $f = fopen("submit2.txt", "a+");
        $task['brand_id'] = (! empty($_POST['brand_id'])) ? $_POST['brand_id']:false;
        $task['titolo'] = (! empty($_POST['titolo'])) ? $_POST['titolo']:false;
        $task['descrizione'] = (! empty($_POST['descrizione'])) ? $_POST['descrizione']:false;fwrite($f, $task['descrizione']);
          if(!($this->check_leng_string($task['brand_id'],1,255))||!($this->check_string($task['brand_id']))){fwrite($f, "1");
            $_SESSION['message'] = 'operazione non valida';
            header('Location:./brand.php?action=detail&brand_id='.$id);
            return;
            }
            if(!($this->check_leng_string($task['titolo'],1,255))||!($this->check_stringOneSpace($task['titolo']))){fwrite($f, "1");
            $_SESSION['message'] = 'Il titolo e troppo lungo o contiene caratteri non supportati';
            header('Location:./brand.php?action=detail&brand_id='.$id);
            return;
            }
            if(!($this->check_stringOneSpace($task['descrizione']))){fwrite($f, "1");
            $_SESSION['message'] = 'la Descrizione contiene caratteri non supportati';
            header('Location:./brand.php?action=detail&brand_id='.$id);
            return;
            }
          else{
            $tasks = $this->_model->addtask(
            $task['brand_id'], false, $task['titolo'], $task['descrizione']);
            $_SESSION['message'] = 'task inserita correttamente';
            header('Location:./brand.php?action=detail&brand_id='.$id);
            return;

          }
        }
        if(isset($_POST['assign'])){
        $dipendenti = $_POST['dipendenti'];
        $task_id = $_POST['task'];
        
        foreach ($dipendenti as  $dipendente) {
        $email = $_POST['email'.$dipendente];
        $name_dest = $_POST['name_dest'.$dipendente];
        $mail = new DipendentiSendMail( $email,$name_dest, $dipendente );
          foreach ($task_id as  $task) {
              $this-> _model->AddRelazione(
              $dipendente,$task );
          }
  }
        
        
            $_SESSION['message'] = 'task assegnata correttamente';
            header('Location:./brand.php?action=detail&brand_id='.$id);
            return;

        }
        if(isset($_POST['update'])){
          $brand['telefono'] = (!empty($_POST['telefono'])) ? $_POST['telefono']:false;
          $brand['email'] = (!empty($_POST['email'])) ? $_POST['email']:false;
          $brand['descrizione'] = (!empty($_POST['descrizione'])) ? $_POST['descrizione']:false;
          if(!($this->check_leng_string($brand['telefono'],1,14))||!($this->check_telefono($brand['telefono']))){fwrite($f, "1");
            $_SESSION['message'] = 'Telefono non valido';
            header('Location:./brand.php?action=detail&brand_id='.$id);
            return;
            }
            if(!($this->check_leng_string($brand['email'],1,255))||!($this->check_mail($brand['email']))){fwrite($f, "1");
            $_SESSION['message'] = 'Email non valida';
            header('Location:./brand.php?action=detail&brand_id='.$id);
            return;
            }
            if(!($this->check_stringOneSpace($brand['descrizione']))){fwrite($f, "1");
            $_SESSION['message'] = 'la Descrizione contiene caratteri non supportati';
            header('Location:./brand.php?action=detail&brand_id='.$id);
            return;
            }
          $brand = $this->_model->updatebrands_description($brand['telefono'],$brand['email'],$brand['descrizione'], $id);
          $_SESSION['message'] = 'update eseguito correttamente';
          header('Location:./brand.php?action=detail&brand_id='.$id);
          return;
        }
        if(isset($_POST['update2'])){
          if(!empty($_POST['servizi_stato'])){
            $servizi_stato = $_POST['servizi_stato'];
            /*foreach ($brands as $id => $brand) {
              if(!($this->check_leng_string($task['brand_id'],1,255))||!($this->check_string($task['brand_id']))){fwrite($f, "1");
            $_SESSION['message'] = 'Il nome del cliente e troppo lungo o contiene caratteri non supportati';
            header('Location:./brand.php?action=detail&brand_id='.$id);
            return;
            }
            if(!($this->check_leng_string($task['titolo'],1,255))||!($this->check_stringOneSpace($task['titolo']))){fwrite($f, "1");
            $_SESSION['message'] = 'Il titolo e troppo lungo o contiene caratteri non supportati';
            header('Location:./brand.php?action=detail&brand_id='.$id);
            return;
            }
            if(!($this->check_stringOneSpace($task['descrizione']))){fwrite($f, "1");
            $_SESSION['message'] = 'la Descrizione contiene caratteri non supportati';
            header('Location:./brand.php?action=detail&brand_id='.$id);
            return;
            }
          else{
            $tasks = $this->_model->addtask(
            $task['brand_id'], false, $task['titolo'], $task['descrizione']);
            $_SESSION['message'] = 'task inserita correttamente';
            header('Location:./brand.php?action=detail&brand_id='.$id);
            return;

          }
            }*/
            $servizi_stato = $this->_model->updateservizi_stato($servizi_stato);
            header('Location:./brand.php?action=detail&brand_id='.$id);
            $_SESSION['message'] = 'update riuscito';
            return;
          }
          else{
            $_SESSION['message'] = 'update non riuscito';
            header('Location:./brand.php?action=detail&brand_id='.$id);
            return;
          }
        }
        if(isset($_POST['update3'])){
          if(!empty($_POST['tasks'])){
            $tasks = $_POST['tasks'];
              $tasks = $this->_model->updatetasks($tasks);
              $_SESSION['message'] = 'task modificata correttamente';
              header('Location:./brand.php?action=detail&brand_id='.$id);
              return;
          }
          else{
            $_SESSION['message'] = 'selezionare la task da modificare';
            header('Location:./brand.php?action=detail&brand_id='.$id);
            return;
          }
        }
        if(isset($_POST['del'])){
          $servizio_stato['id'] = (!empty($_POST['id'])) ? $_POST['id']:false;
            if($servizio_stato['id'] === false){
              $_SESSION['message'] = 'selezionare servizio da eliminare';
              header('Location:./brand.php?action=detail&brand_id='.$id);
              return;
            }
            else{
              $this->_model->deleteservizio_stato($servizio_stato['id']);
              $_SESSION['message'] = 'servizio eliminato correttamente';
              header('Location:./brand.php?action=detail&brand_id='.$id);
              return;
            }
        }
        if(isset($_POST['del2'])){
          $task['id'] = (!empty($_POST['id'])) ? $_POST['id']:false;
            if($task['id'] === false){
              $_SESSION['message'] = 'selezionare task da eliminare';
              header('Location:./brand.php?action=detail&brand_id='.$id);
              return;
            }
            else{
              $task['id'] = $this->_model->deletetask($task['id']);
              $_SESSION['message'] = 'task eliminata correttamente';
              header('Location:./brand.php?action=detail&brand_id='.$id);
              return;
            }
        }
        if( isset($_POST['ripristina']) ){
          $task['id'] = (!empty($_POST['id'])) ? $_POST['id']:false;
            if($task['id'] === false){
              $_SESSION['message'] = 'selezionare task da ripristinare';
              header('Location:./brand.php?action=detail&brand_id='.$id);
              return;
            }
            else{
              $this->_model->RipristinaTask($task['id']);
              $_SESSION['message'] = 'task ripristinata correttamente';
              header('Location:./brand.php?action=detail&brand_id='.$id);
              return;
            }
        }
        $tasks= $this-> _model->gettask($id);
        $brand_dati = $this-> _model->getbrand_dati($id);
        $servizi = $this-> _model->getservizi(); 
        $servizi_attivi = $this-> _model->getstatoserv($id);
        $servizi_passati = $this-> _model->getstatoservpass($id);
        $dipendenti= $this-> _model->getDipendenti();
        $assign = $this-> _model->getDipendenteAssign();

        include('./views/brand_dettagli.php');
	}
}