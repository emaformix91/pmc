<?php 
class servizicontroller extends SuperControllers{
	private $_model = NULL;

	public function __construct(){
		$this-> _model = new servizimodel;
	}
	public function index(){
		global $user_controller;
		$servizi = $this-> _model->getServizi();
		if(isset($_POST['submit'])){
			$servizi['nome'] = ( !empty($_POST['nome'])) ? $_POST['nome'] : false;
			if(!($this->check_leng_string($servizi['nome'],1,600))||!($this->check_stringOneSpace($servizi['nome']))){
			header('Location:./servizi.php');
            $_SESSION['message'] = 'Il nome del del servizio e troppo lungo o contiene caratteri non supportati';
            return;
          	}
          	else{
          		$servizi = $this-> _model->AddServizio( $servizi['nome'] );
          		header('Location:./servizi.php');
          		$_SESSION['message'] = 'Servizio aggiunto correttamente';
            return;
          	}
		}
		if(isset($_POST['update'])){
			if(!empty($_POST['servizi'])){
            	$servizi = $_POST['servizi'];
            foreach ($servizi as $id => $servizio) {
              		if(!($this->check_leng_string($servizio['name'],1,600))||!($this->check_stringOneSpace($servizio['name']))){
					header('Location:./servizi.php');
            		$_SESSION['message'] = 'Il nome del del servizio e troppo lungo o contiene caratteri non supportati';
            		return;
              		}
              		
            }
            	$this-> _model->UpdateServizi($servizi);
            	header('Location:./servizi.php');
            	$_SESSION['message'] = 'update riuscito';
            	return;	
            }
            else{
             	$_SESSION['message'] = 'update non riuscito';
             	header('Location:./dipendenti.php');
            	return;	
            }
        }
        if(isset($_POST['delete'])){
          $servizio['id'] = (!empty($_POST['id'])) ? $_POST['id']:false;
            if($servizio['id'] === false){
              $_SESSION['message'] = 'selezionare servizio da eliminare';
              header('Location:./servizi.php');
              return;
            }
            else{
              $this->_model->deleteServizio($servizio['id']);
              $_SESSION['message'] = 'servizio eliminato correttamente';
              header('Location:./servizi.php');
              return;
            }
        }
        if(isset($_POST['cerca'])){
            $servizi['nome'] = ( !empty($_POST['nome'])) ? $_POST['nome'] : false;
			$servizi = $this-> _model->serch_servizio( $servizi['nome'] );
			if(!empty($servizi)){
            header('Refresh:./brands.php');
            $_SESSION['message'] = 'ricerca riuscita'; 
            }
            else{
              header('Refresh:./brands.php');
              $_SESSION['message'] = 'non ci sono corrispondenze';
            } 	 
        }
		include('./views/servizi_index.php');
	}
  
}
