<?php 
class dipendenticontroller extends SuperControllers{
	private $_model = NULL;

	public function __construct(){
		$this-> _model = new dipendentimodel;
	}
	public function index(){
		global $user_controller;
		$dipendenti = $this-> _model->getDipendenti();
		if(isset($_POST['submit'])){
			$dipendenti['nome'] = ( !empty($_POST['nome'])) ? $_POST['nome'] : false;
			$dipendenti['cognome'] = ( !empty($_POST['cognome'])) ? $_POST['cognome'] : false;
			$dipendenti['settore'] = ( !empty($_POST['settore'])) ? $_POST['settore'] : false;
			$dipendenti['ruolo'] = ( !empty($_POST['ruolo'])) ? $_POST['ruolo'] : false;
			$dipendenti['telefono'] = ( !empty($_POST['telefono'])) ? $_POST['telefono'] : false;
			$dipendenti['email'] = ( !empty($_POST['email'])) ? $_POST['email'] : false; 
			$dipendenti['codfiscale'] = ( !empty($_POST['codfiscale'])) ? $_POST['codfiscale'] : false;
      $dipendenti['password'] = ( !empty($_POST['pass'])) ? $_POST['pass'] : false;
			if(!($this->check_leng_string($dipendenti['nome'],1,255))||!($this->check_string($dipendenti['nome']))){
            $_SESSION['message'] = 'Il nome del dipendente e troppo lungo o contiene caratteri non supportati';
            header('Location: ./dipendenti.php');
            return;
          	}
          	if(!($this->check_leng_string($dipendenti['nome'],1,255))||!($this->check_string($dipendenti['nome']))){
            $_SESSION['message'] = 'Il nome del dipendente e troppo lungo o contiene caratteri non supportati';
            header('Location: ./dipendenti.php');
            return;
          	}
          	if(!($this->check_leng_string($dipendenti['cognome'],1,255))||!($this->check_string($dipendenti['cognome']))){
            $_SESSION['message'] = 'Il nome del dipendente e troppo lungo o contiene caratteri non supportati';
            header('Location: ./dipendenti.php');
            return;
          	}
          	if(!($this->check_leng_string($dipendenti['settore'],1,255))||!($this->check_stringOneSpace($dipendenti['settore']))){
            $_SESSION['message'] = 'Il nome del dipendente e troppo lungo o contiene caratteri non supportati';
            header('Location: ./dipendenti.php');
            return;
          	}
          	if(!($this->check_leng_string($dipendenti['ruolo'],1,255))||!($this->check_stringOneSpace($dipendenti['ruolo']))){
            $_SESSION['message'] = 'Il nome del dipendente e troppo lungo o contiene caratteri non supportati';
            header('Location: ./dipendenti.php');
            return;
          	}
          	if(!($this->check_leng_string($dipendenti['ruolo'],1,14))||!($this->check_telefono($dipendenti['telefono']))){
            $_SESSION['message'] = 'Il nome del dipendente e troppo lungo o contiene caratteri non supportati';
            header('Location: ./dipendenti.php');
            return;
          	}
          	if(!($this->check_leng_string($dipendenti['codfiscale'],1,255))||!($this->check_string($dipendenti['codfiscale']))){
            $_SESSION['message'] = 'Il nome del dipendente e troppo lungo o contiene caratteri non supportati';
            header('Location: ./dipendenti.php');
            return;
          	}
            if(!($this->check_leng_string($dipendenti['password'],1,255))||!($this->check_string($dipendenti['password']))){
            $_SESSION['message'] = 'Il nome del dipendente e troppo lungo o contiene caratteri non supportati';
            header('Location: ./dipendenti.php');
            return;
            }
          	else{
          		$dipendenti = $this-> _model->AddDipendente( $dipendenti['nome'],$dipendenti['cognome'],$dipendenti['settore'],$dipendenti['ruolo'],$dipendenti['email'],$dipendenti['telefono'],$dipendenti['codfiscale'],$dipendenti['password'] );
          		header('Location:./dipendenti.php');
          		$_SESSION['message'] = 'Dipendente aggiunto correttamente';
            return;
          	}
		}
		if(isset($_POST['update'])){
			if(!empty($_POST['dipendenti'])){
            	$dipendenti = $_POST['dipendenti'];
            foreach ($dipendenti as $id => $dipendente) {
              		if(!($this->check_leng_string($dipendente['name'],1,255))||!($this->check_string($dipendente['name']))){
                	header('Location:./dipendenti.php');
                	$_SESSION['message'] = 'nome non valido';
                	return;
              		}
              		if(!($this->check_leng_string($dipendente['cognome'],1,255))||!($this->check_string($dipendente['cognome']))){
                	header('Location:./dipendenti.php');
                	$_SESSION['message'] = 'cognome non valido';
                	return;
              		}
              		elseif(!($this->check_leng_string($dipendente['settore'],1,255))||!($this->check_stringOneSpace($dipendente['settore']))){
              		header('Location:./dipendenti.php');
              		$_SESSION['message'] = 'settore non valido';
                	return;
              		}
              		elseif(!($this->check_leng_string($dipendente['email'],1,255))||!($this->check_mail($dipendente['email']))){
                	header('Location:./dipendenti.php');
                	$_SESSION['message'] = 'email non valida';
                	return;
              		}
              		elseif(!($this->check_leng_string($dipendente['ruolo'],1,255))||!($this->check_stringOneSpace($dipendente['ruolo']))){
              		header('Location:./dipendenti.php');
              		$_SESSION['message'] = 'ruolo non valido';
                	return;
              		}
            }
            	$this-> _model->UpdateDipendenti($dipendenti);
            	header('Location:./dipendenti.php');
            	$_SESSION['message'] = 'update riuscito';
            	return;	
            }
            else{
             	$_SESSION['message'] = 'update non riuscito';
             	header('Location:./dipendenti.php');
            	return;	
            }
        }
        if(isset($_POST['delete'])){
          $dipendente['id'] = (!empty($_POST['id'])) ? $_POST['id']:false;
            if($dipendente['id'] === false){
              $_SESSION['message'] = 'selezionare dipendente da eliminare';
              header('Location:./dipendenti.php');
              return;
            }
            else{
              $this->_model->deleteDipendente($dipendente['id']);
              $_SESSION['message'] = 'dipendente eliminato correttamente';
              header('Location:./dipendenti.php');
              return;
            }
        }
        if(isset($_POST['cerca'])){
            $dipendente['nome'] = ( !empty($_POST['nome'])) ? $_POST['nome'] : false;
			     $dipendente['cognome'] = ( !empty($_POST['cognome'])) ? $_POST['cognome'] : false;
			     $dipendente['settore'] = ( !empty($_POST['settore'])) ? $_POST['settore'] : false;
			     $dipendenti = $this-> _model->serch_dipendente( $dipendente['nome'],$dipendente['cognome'],$dipendente['settore'] );
			     if(!empty($dipendenti)){
            header('Refresh:./brands.php');
            $_SESSION['message'] = 'ricerca riuscita'; 
            }
            else{
              header('Refresh:./brands.php');
              $_SESSION['message'] = 'non ci sono corrispondenze';
            } 	 
        }
		include('./views/dipendenti_index.php');
	}
  public function detail(){
    global $user_controller;
    global $userdip_controller;
    $id = ( isset( $_GET['dipendente_id'] )&& intval( $_GET['dipendente_id'] )!= 0) ? $_GET['dipendente_id']:false;

    if($id === false){
          echo "<p>errore dettagli cliente non inseriti nel database</p>";
          return;
        }
    if(isset($_POST['update'])){
      $dipendente['telefono'] = ( !empty($_POST['telefono']) ) ? $_POST['telefono'] : false;
      $dipendente['email'] = ( !empty($_POST['email']) ) ? $_POST['email'] : false;
      $dipendente['codfiscale'] = ( !empty($_POST['codfiscale']) ) ? $_POST['codfiscale'] : false;
      $dipendente['settore'] = ( !empty($_POST['settore']) ) ? $_POST['settore'] : false;
      $dipendente['ruolo'] = ( !empty($_POST['ruolo']) ) ? $_POST['ruolo'] : false;
      $dipendente['password'] = ( !empty($_POST['pass']) ) ? $_POST['pass'] : false;
      if(!($this->check_leng_string($dipendente['settore'],1,255))||!($this->check_stringOneSpace($dipendente['settore']))){
                  header('Location:./dipendenti.php?action=detail&dipendente_id='.$id);
                  $_SESSION['message'] = 'settore non valido';
                  return;
                  }
      elseif(!($this->check_leng_string($dipendente['email'],1,255))||!($this->check_mail($dipendente['email']))){
                  header('Location:./dipendenti.php?action=detail&dipendente_id='.$id);
                  $_SESSION['message'] = 'email non valida';
                  return;
                  }
      elseif(!($this->check_leng_string($dipendente['ruolo'],1,255))||!($this->check_stringOneSpace($dipendente['ruolo']))){
                  header('Location:./dipendenti.php?action=detail&dipendente_id='.$id);
                  $_SESSION['message'] = 'ruolo non valido';
                  return;
                  }
      elseif(!($this->check_leng_string($dipendenti['ruolo'],1,14))||!($this->check_telefono($dipendente['telefono']))){
            $_SESSION['message'] = 'telefono errato';
            header('Location:./dipendenti.php?action=detail&dipendente_id='.$id);
            return;
            }
      elseif(!($this->check_codfiscale($dipendente['codfiscale']))||!($this->check_string($dipendente['codfiscale']))){
            $_SESSION['message'] = 'cod.fiscale errato';
            header('Location:./dipendenti.php?action=detail&dipendente_id='.$id);
            return;
            }
      if($dipendente['password']!=false){
        $dipendenti = $this-> _model->UpdateDipendente($dipendente['telefono'],
                                                      $dipendente['email'],
                                                      $dipendente['codfiscale'],
                                                      $dipendente['settore'],
                                                      $dipendente['ruolo'],
                                                      $dipendente['password'],
                                                      $id
                                                      );
      }
      else{
        $dipendenti = $this-> _model->UpdateDipendente_whithsamepass($dipendente['telefono'],
                                                      $dipendente['email'],
                                                      $dipendente['codfiscale'],
                                                      $dipendente['settore'],
                                                      $dipendente['ruolo'],
                                                      $id
                                                      );
      }

      
              $_SESSION['message'] = 'update riuscito';
              header('Location:./dipendenti.php?action=detail&dipendente_id='.$id);
              return;
        }
    if( isset($_POST['completata']) ){
      $task['id'] = (!empty($_POST['id'])) ? $_POST['id']:false;
      $task['titolo'] = (!empty($_POST['titolo'])) ? $_POST['titolo']:false;
            if($task['id'] === false){
              $_SESSION['message'] = 'selezionare task completata';
              header('Location:./dipendenti.php?action=detail&dipendente_id='.$id);
              return;
            }
            else{
              $task['id'] = $this->_model->TaskCompletata($task['id']);
              $_SESSION['message'] = 'situazione task aggiornata';
              header('Location:./dipendenti.php?action=detail&dipendente_id='.$id);
              return;
            }
    }
    if( isset($_POST['ripristina']) ){
          $task['id'] = (!empty($_POST['id'])) ? $_POST['id']:false;
            if($task['id'] === false){
              $_SESSION['message'] = 'selezionare task da ripristinare';
              header('Location:./dipendenti.php?action=detail&dipendente_id='.$id);
              return;
            }
            else{
               $this->_model->RipristinaTask($task['id']);
              $_SESSION['message'] = 'task ripristinata correttamente';
              header('Location:./dipendenti.php?action=detail&dipendente_id='.$id);
              return;
            }
        }
    $dipendente = $this-> _model->getDipendente($id);
    $tasks_dip = $this-> _model->getTaskDip($id);
    include('./views/dipendente_detail.php');
  }
  
}
