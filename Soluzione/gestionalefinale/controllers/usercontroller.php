<?php
class usercontroller{
	public $username ='';
	private $_model = NULL;
	private $logged = false;

	public function __construct(){
		//session_start();
		$this->_model = new usermodel();
		$this->_startSecureSession();

		if($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_GET['action']) && $_GET['action'] == 'login'){
			$username = isset($_POST['username'])?$_POST['username']:false;
			$password = isset($_POST['password'])?$_POST['password']:false;

			if($username != false && $password != false && $this->checklogin($username,$password)){
				$this->username = $username;
				$this->logged = true;

				$_SESSION['username'] = $username;
				$_SESSION['logged'] = true;
				$_SESSION['message'] = 'Benvenuto';
			}else{
                $_SESSION['message'] = 'Login non riuscito piccolo problemino';
			}
		}
		elseif(isset($_GET['action']) && $_GET['action']=='logout'){
			unset($_SESSION['username']);
			unset($_SESSION['logged']);
			$_SESSION['message'] = 'Buona Giornata';
			header('Location:./login.php');
		}
		elseif(isset($_SESSION['username'])&& isset($_SESSION['logged'])&& $_SESSION['logged']){
			$this->username = $_SESSION['username'];
			$this->logged = true;
		}
		$this->redirecttoproperarea();
	}
	public function checklogin($username,$password){
		$admin = $this->_model->getuser_pass();
		foreach ($admin as $value) {
		  return($value['username'] == $username && $value['password'] == md5($password));
		}
		
	}
	public function islogged(){
		return $this->logged;
	}
	public function redirecttoproperarea(){
		$script_file = basename($_SERVER['SCRIPT_NAME']);

		if($this->islogged() && $script_file == 'login.php'){
			header('location:./clienti.php');
			die();
		}
		elseif(!$this->islogged() && ($script_file == 'clienti.php' && isset($GET_['action']) && $GET_['action']!='index' && $GET_['action']!='detail')){
			header('location:./login.php');
			die();
		}
	}
	private function _startSecureSession(){
		ini_set('session.use_trans_sid', 0);
		ini_set('session.use_only_cookies', 1);

		$session_name ='sessionId';
		$cookie_defaults =session_get_cookie_params();
		session_set_cookie_params(
            0,
            $cookie_defaults['path'],
            $cookie_defaults['domain'],
            false,
            true
			);
			session_name($session_name);
			session_start();
			session_regenerate_id(true);	
	}
}