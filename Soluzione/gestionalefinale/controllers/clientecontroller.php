<?php
  class clientecontroller extends SuperControllers {
  	private $_model = NULL;

  	public function __construct(){
  		$this->_model = new clientemodel();
  	}
  	public function index(){
      global $user_controller;
      $admin = $this->_model->getadmin();
      $servizi = $this->_model->getservizi();
      $clienti = $this->_model->getclienti();
        if(isset($_POST['submit1'])){
          $admin['name'] = (! empty($_POST['name'])) ? $_POST['name']:false;
          $admin['cognome'] = (! empty($_POST['cognome'])) ? $_POST['cognome']:false;
          $admin['azienda'] = (! empty($_POST['azienda'])) ? $_POST['azienda']:false;
          $admin['email'] = (! empty($_POST['email'])) ? $_POST['email']:false;
          $admin['username'] = (! empty($_POST['username'])) ? $_POST['username']:false;
          $admin['password'] = (! empty($_POST['password'])) ? $_POST['password']:false;
          if(!($this->check_leng_string($admin['name'],1,255))||!($this->check_string($admin['name']))){
            $_SESSION['message'] = 'Il nome del cliente e troppo lungo o contiene caratteri non supportati';
            header('Location:./clienti.php');
            return;
          }
          if(!($this->check_leng_string($admin['cognome'],1,255))||!($this->check_string($admin['cognome']))){
            $_SESSION['message1'] = 'Il cognome del cliente e troppo lungo o contiene caratteri non supportati';
            header('Location:./clienti.php');
            return;
          }
          if(!($this->check_leng_string($admin['azienda'],1,255))||!($this->check_stringOneSpace($admin['azienda']))){
            $_SESSION['message2'] = 'Il nome del cliente e troppo lungo o contiene caratteri non supportati';
            header('Location:./clienti.php');
            return;
          }
          if(!($this->check_leng_string($admin['email'],1,255))||!($this->check_mail($admin['email']))){
            $_SESSION['message3'] = 'Il nome del cliente e troppo lungo o contiene caratteri non supportati';
            isset($_SESSION['message']);
            header('Location:./clienti.php');
            return;
          }
          if(!($this->check_leng_string($admin['username'],1,255))||!($this->check_string($admin['username']))){
            $_SESSION['message4'] = 'Il nome del cliente e troppo lungo o contiene caratteri non supportati';
            header('Location:./clienti.php');
            return;
          }
          else{
            $admin = $this->_model->addadmin(
            $admin['name'], $admin['cognome'], $admin['azienda'], $admin['email'],$admin['username'],$admin['password']);
            $_SESSION['message'] = 'Benvenuto '.$admin['name'];
            header('Location:./clienti.php');
            return;
          }
        }
        if(isset($_POST['submit2'])){
          $num_serv['num_serv'] = (! empty($_POST['num_serv'])) ? $_POST['num_serv']:false;
          if($num_serv['num_serv'] === false){
            $_SESSION['message'] = 'Non hai selezinato nessun valore';
            header('Location:./clienti.php');
            return;
          }
          else{
            $num_serv = $this->_model->addnumserv(
            $num_serv['num_serv']);
            header('Location:./clienti.php');
            return;
          }
        }
        if(isset($_POST['submit3'])){
          $servizi['name'] = (! empty($_POST['name'])) ? $_POST['name']:false;
          if($servizi['name'] === false){
            $_SESSION['message'] = 'Il nome del servizio e un campo obbligatorio';
            header('Location:./clienti.php');
            return;
          }
          else{
            $servizi = $this->_model->AddServizio(
            $servizi['name']);
            $_SESSION['message'] = 'Il campo '.$servizi.' e stato aggiunto correttamente';
            header('Location:./clienti.php');
            return;
          }
        }
        if(isset($_POST['submit4'])){
          if(! empty($_POST['servizi'])){
          $names = $_POST['servizi'];
          $servizi = $this->_model->addmultiserv($names);
          $_SESSION['message']=='Benvenuto inserire accaunt e password';
          header('Location:./login.php');
          return;
          }
          else{
            $_SESSION['message'] = 'compilare i campi';
            header('Location:./clienti.php');
            return;
          }
        }

        if(isset($_POST['submit5'])){
          $f = fopen("file.txt", "a+");
          $cliente['cname'] = (!empty($_POST['cname'])) ? $_POST['cname']:false;
          $cliente['ccognome'] = (!empty($_POST['ccognome'])) ? $_POST['ccognome']:false;
          $cliente['cindirizzo'] = trim(!empty($_POST['cindirizzo'])) ? $_POST['cindirizzo']:false;
          $cliente['civa'] = (!empty($_POST['civa'])) ? $_POST['civa']:false;
          $cliente['ctelefono'] = (!empty($_POST['ctelefono'])) ? $_POST['ctelefono']:false;
          $cliente['cemail'] = (!empty($_POST['cemail'])) ? $_POST['cemail']:false;
          
          if(!($this->check_leng_string($cliente['cname'],1,255))&&!($this->check_string($cliente['cname']))){
            $_SESSION['message'] = 'Il nome del cliente e troppo lungo o contiene caratteri non supportati';
            header('Location:./clienti.php');
            return;
          }
          if(!($this->check_leng_string($cliente['ccognome'],1,255))||!($this->check_string($cliente['ccognome']))){
            $_SESSION['message'] = 'Il cognome del cliente e troppo lungo o contiene caratteri non supportati';
            header('Location:./clienti.php');
            return;
          }
          if(!($this->check_stringOneSpace($cliente['cindirizzo']))){fwrite($f, "2");
          $_SESSION['message'] = 'Il indirizzo del cliente e troppo lungo o contiene caratteri non supportati';
            header('Location:./clienti.php');
            return;
          }
          if(!($this->check_leng_string($cliente['civa'],1,255))||!($this->check_string($cliente['civa']))){
            $_SESSION['message'] = 'Il Partita iva del cliente e troppo lungo o contiene caratteri non supportati';
            header('Location:./clienti.php');
            return;
          }
          if(!($this->check_leng_string($cliente['ctelefono'],1,255))||!($this->check_telefono($cliente['ctelefono']))){fwrite($f, "4");
          $_SESSION['message'] = 'Il telefono del cliente e troppo lungo o contiene caratteri non supportati';
            header('Location:./clienti.php');
            return;
          }
          if(!($this->check_leng_string($cliente['cemail'],1,255))||!($this->check_mail($cliente['cemail']))){
            $_SESSION['message'] = 'email del cliente e troppo lungo o contiene caratteri non supportati';
            header('Location:./clienti.php');
            return;
          }
          else{
            $clienti = $this->_model->addcliente(
            $cliente['cname'], $cliente['ccognome'], $cliente['cindirizzo'], $cliente['civa'], $cliente['ctelefono'], $cliente['cemail']);
            $_SESSION['message'] = 'Cliente aggiunto ';
            header('Location:./clienti.php');
            return;
          }
        }
        if(isset($_POST['del'])){
          $id = (! empty($_POST['id'])) ? $_POST['id']:false;
          if($id==false){
            $_SESSION['message'] = 'pulsante da abilitare';
            header('Location:./clienti.php');
          }
          else{
            $num_serv = $this->_model->delete_numserv($id);
            header('Location:./clienti.php');
          }
        }
        if(isset($_POST['update'])){
          $f = fopen("update_index.txt", "a+");
          if(!empty($_POST['clienti'])){
            $clienti = $_POST['clienti'];

            foreach ($clienti as $id => $cliente) {
              if(!($this->check_leng_string($cliente['name'],1,255))||!($this->check_string($cliente['name']))){fwrite($f, "1");
                $_SESSION['message'] = 'nome non valido';
                header('Location:./clienti.php');
                return;
              }
              elseif(!($this->check_leng_string($cliente['cognome'],1,255))||!($this->check_string($cliente['cognome']))){fwrite($f, "2");
                $_SESSION['message'] = 'cognome non valido';
                header('Location:./clienti.php');
                return;
              }
              elseif(!($this->check_leng_string($cliente['email'],1,255))||!($this->check_mail($cliente['email']))){fwrite($f, "3");
                $_SESSION['message'] = 'email non valida';
                header('Location:./clienti.php');
                return;
              }
            }
            $this->_model->updateclienti($clienti);
            $_SESSION['message'] = 'update riuscito';
            header('Location:./clienti.php');
            return;
            }
          else{
            $_SESSION['message'] = 'update non riuscito';
            header('Location:./clienti.php');
            return;
          }
        }
        if(isset($_POST['delete'])){
          
          $cliente['id'] = (!empty($_POST['id'])) ? $_POST['id']:false;
          if($cliente['id'] === false){
            $_SESSION['message'] = 'selezionare cliente da eliminare';
          }
          elseif(isset($_POST['ajax'])){
            echo 'prova';
            exit();
          }
          else{
            $this->_model->deletecliente($cliente['id']);
            $_SESSION['message'] = 'cliente eliminato correttamente';
             header('Location:./clienti.php');
            return;
          }
        
        }
        if(isset($_POST['cerca'])){
             $f = fopen("cerca.txt", "a+");
              $cliente['name'] = ( !empty($_POST['name'])) ? $_POST['name'] : false;
              $cliente['cognome'] = ( !empty($_POST['cognome'])) ? $_POST['cognome'] : false;
              $cliente['email'] = ( !empty($_POST['email'])) ? $_POST['email'] : false;
       $clienti = $this-> _model->serch_cliente( $cliente['name'],$cliente['cognome'],$cliente['email'] );
        if(!empty($clienti)){
        header('Refresh:./clienti.php');
        $_SESSION['message'] = 'ricerca riuscita'; 
        }
        else{
          header('Refresh:./clienti.php');
        $_SESSION['message'] = 'non ci sono corrispondenze';
        }    
      }
      $num_serv = $this->_model->getnumserv();

  		include('./views/cliente_index.php');
  	}
  	public function detail(){
      global $user_controller;
  		$id = ( isset( $_GET['cliente_id'] )&& intval( $_GET['cliente_id'] )!= 0) ? $_GET['cliente_id']:false;
      
        if($id === false){
        	echo "<p>errore dettagli brand non inseriti nel database</p>";
        	return;
        }
        $cliente =$this->_model->getcliente($id);
        $dati_brand =$this->_model->getbrand_servizi($id);
        $variation_controller = new brandcontroller();
        $brands = $variation_controller->getbrand($id);
        if(isset($_POST['submit'])){
          $brands = $variation_controller->addbrand($id);
          if($brands==false){
            header('Location:./clienti.php?action=detail&cliente_id='.$id);
            return;
          }
        }
        $servizi = $this->_model->getservizi();
        if(isset($_POST['update'])){
          $f = fopen("update_detail.txt", "a+");
            $cliente['indirizzo'] = (!empty($_POST['indirizzo'])) ? $_POST['indirizzo']:false;
            $cliente['iva'] = (!empty($_POST['iva'])) ? $_POST['iva']:false;
            $cliente['telefono'] = (!empty($_POST['telefono'])) ? $_POST['telefono']:false;
            $cliente['email'] = (!empty($_POST['email'])) ? $_POST['email']:false;

              if(!($this->check_leng_string($cliente['email'],1,255))||!($this->check_mail($cliente['email']))){fwrite($f, "3");
                $_SESSION['message'] = 'email non valida';
                header('Location:./clienti.php?action=detail&cliente_id='.$id);
                return;
              }
              else{
                $this->_model->updateclienti_dettail($cliente['indirizzo'], $cliente['iva'], $cliente['telefono'], $cliente['email'], $id);
                $_SESSION['message'] = 'update riuscito';
                header('Location:./clienti.php?action=detail&cliente_id='.$id);
                return;
              }
        }
        if(isset($_POST['update2'])){
          $brands = $variation_controller->updatebrands();
          header('Location:./clienti.php?action=detail&cliente_id='.$id);
          if($brands==false){
            header('Location:./clienti.php?action=detail&cliente_id='.$id);
            return;
          }
        }
        if(isset($_POST['del'])){
          $brand['id'] = $variation_controller->deletebrand();
          header('Location:./clienti.php?action=detail&cliente_id='.$id);
          if($brand['id']==false){
            header('Location:./clienti.php?action=detail&cliente_id='.$id);
            return;
          }
        }
      if(isset($_POST['cerca'])){
             $f = fopen("cerca.txt", "a+");
              $brand['azienda'] = ( !empty($_POST['azienda'])) ? $_POST['azienda'] : false;
              $brand['settore'] = ( !empty($_POST['settore'])) ? $_POST['settore'] : false;
              $brand['email'] = ( !empty($_POST['email'])) ? $_POST['email'] : false;
       $brands = $this-> _model->serch_brand2( $brand['azienda'],$brand['settore'],$brand['email'] );
        if(!empty($brands)){
        header('Refresh:./brands.php');
        $_SESSION['message'] = 'ricerca riuscita'; 
        }
        else{
          header('Refresh:./brands.php');
        $_SESSION['message'] = 'non ci sono corrispondenze';
        }    
      }
      include('./views/cliente_dettagli.php');
  	}
    public function edit(){
      global $user_controller;
      if($_SERVER['REQUEST_METHOD']=='POST'){
        if(! empty($_POST['clienti'])){
          $clienti = $_POST['clienti'];

          foreach ($clienti as $id => $cliente){
            if(empty($cliente['name'])){
              $_SESSION['message'] = 'nome'.$id.'obbligatorio';
              include('./view/cliente_edit.php');
              return;
            }
            elseif(empty($cliente['cognome'])){
              $_SESSION['message'] = 'cognome'.$id.'obbligatorio';
              include('./view/cliente_edit.php');
              return;
            }
            elseif(empty($cliente['azienda'])){
              $_SESSION['message'] = 'azienda'.$id.'obbligatorio';
              include('./view/cliente_edit.php');
              return;
            }
          }
          $this->_model->updateclienti($clienti);
          $_SESSION['message'] = 'aggiornamento riuscito con successo';
          header('Location:./clienti.php');
        }
        else{
          $_SESSION['message'] = 'cliente non esistente';
          include('./view/clienti_edit.php');
        }
      }
      else{
        $clienti = $this->_model->getclienti();
      include('./view/clienti_edit.php');
      }  
    }
    
  } 