<?php
  class DbConnector{
  	private $_conn = NULL;
    private $_statement = NULL;
  	public function __construct(){
  		$this->_conn = mysqli_connect(DB_HOST, DB_USERNAME, DB_PASSWORD, DB_NAME);
  	}
public function insert($tabella, $fields, $values){
      $query="INSERT INTO ".$tabella." ";
      $query.="(" .implode(',',$fields). ")";
      $query.=" VALUES ";
      $query.="(" .implode(',', $values). ")";
      $f=fopen('insert.txt', 'a+');
      fwrite($f,$query ."\n\r");
      return mysqli_query($this->_conn, $query);
    }

  	public function select($fields, $tabella, $condizioni ='1'){
  		$query = "SELECT ";
  		$query.= implode(', ',$fields);
  		$query.=" FROM ";
  		$query.= implode(', ', $tabella);
  		$query.=" WHERE ";
  		$query.= $condizioni;
      $f=fopen('select.txt', 'a+');
      fwrite($f,$query ."\n\r");
  		return mysqli_query($this->_conn, $query);
  	}
    public function select_inner($fields, $tabella, $tabella2=false, $relations2=false, $tabella3=false, $relations3=false, $tabella4=false, $relations4=false, $tabella5=false, $relations5=false, $tabella6=false, $relations6=false, $condizioni ='1', $condizioni2=false, $condizioni3=false){
      $query = "SELECT ";
      $query.= implode(', ',$fields);
      $query.=" FROM ";
      $query.= implode(', ', $tabella);
      if ($tabella2==true){
        $query.=" INNER JOIN ".$tabella2;
        $query.= " ON ";
        foreach ($relations2 as $name => $value) {
        $query.=$name. " = ".$value;
        }
      }
      if ($tabella3==true){
        $query.=" INNER JOIN ".$tabella3;
        $query.= " ON ";
        if($relations3==true){
          foreach ($relations3 as $name => $value) {
          $query.=$name. " = ".$value;
          }
        }
      }
      if ($tabella4==true){
        $query.=" INNER JOIN ".$tabella4;
        $query.= " ON ";
        if($relations4==true){
          foreach ($relations4 as $name => $value) {
          $query.=$name. " = ".$value;
          }
        }
      }
      if ($tabella5==true){
        $query.=" INNER JOIN ".$tabella5;
        $query.= " ON ";
        if($relations5==true){
          foreach ($relations5 as $name => $value) {
          $query.=$name. " = ".$value;
          } 
        }
      }
      if ($tabella6==true){
        $query.=" INNER JOIN ".$tabella6;
        $query.= " ON ";
        if($relations6==true){
          foreach ($relations6 as $name => $value) {
          $query.=$name. " = ".$value;
          } 
        }
      }
      $query.=" WHERE ";
      $query.= $condizioni;
    if($condizioni2==true){
      $query.=" AND ";
      $query.= $condizioni2;
    }
    if($condizioni3==true){
      $query.=" AND ";
      $query.= $condizioni3;
    }
      $f=fopen('select_inner.txt', 'a+');
      fwrite($f,$query ."\n\r");
      return mysqli_query($this->_conn, $query);
    }
   
    public function update($tabella, $values, $condizioni ='1'){
      $first = true;

      $query = "UPDATE ".$tabella;
      $query.= " SET ";

      foreach ($values as $name => $value) {
        if(!$first){
         $query.=", "; 
        }
        $query.=$name. " = ".$value;
        $first = false;
      }

      $query.=" WHERE ";
      $query.=$condizioni;
     $f=fopen('update.txt', 'a+');
      fwrite($f,$query ."\n\r");
      return mysqli_query($this->_conn, $query);
    }
    public function delete($tabella,$condizioni='0'){
      $query = "DELETE FROM".$tabella;
      $query.= " WHERE ";
      $query.=$condizioni;
      $f=fopen('delete.txt', 'a+');
      fwrite($f,$query ."\n\r");
      return mysqli_query($this->_conn, $query);
    }
    public function serch( $match, $against, $tabella ){
      $query = "SELECT *, ";
      $query.= " MATCH(";
      $query.= implode(', ', $match);
      $query.= ")";
      $query.= "AGAINST('";
      $query.= implode(', ', $against);
      $query.="') ";
      $query.=" as score FROM ";
      $query.= implode(', ', $tabella);
      $query.= " WHERE ";
      $query.= " MATCH(";
      $query.= implode(', ', $match);
      $query.= ")";
      $query.= "AGAINST('";
      $query.= implode(', ', $against);
      $query.="') ";
      $query.= "ORDER BY score DESC";
      $f=fopen('serch_top.txt', 'a+');
      fwrite($f,$query ."\n\r");
      return mysqli_query($this->_conn, $query);
    }
    
  	public function execQuery($query){
  		return mysqli_query($this->_conn, $query);
  	}
    public function prepareStatement($statement){
       $this->_statement = $this->_conn->prepare($statement);
    }
    public function bindParamsToStatement( $type, $params ){
            call_user_func_array( array( $this->_statement, 'bind_param' ), array_merge( (array) $type, $params ) );
    }
   public function execStatement(){
    return $this->_statement->execute();
    }
    public function closeStatement(){
      $this->_statement->close();
    }
    public function LastId(){
      return $this->_conn->insert_id;
    }

  	public function escape($string){
  		return mysqli_real_escape_string($this->_conn, $string);
  	}
  	public function fetch_assoc($res){
  		return mysqli_fetch_assoc($res);
  	}
  	public function __destruct(){
  		mysqli_close($this->_conn);
  	}
  }