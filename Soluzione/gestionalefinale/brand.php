<?php

include('./include/config.php');
include( './include/variabili.php' );
if(! empty($brand_include_list)){
  foreach ($brand_include_list as $to_include) {
  include ($to_include);
  } 
}


    $user_controller = new usercontroller();
    $brand_controller = new brandcontroller();

    $valid_actions = array( 'detail','create', 'edit', 'delete' );
    $action = isset( $_GET[ 'action' ] ) && in_array( $_GET[ 'action' ], $valid_actions ) ? $_GET[ 'action' ] : 'detail';

    // exec requested action
    $brand_controller->$action();
    include( './views/footer.php' );