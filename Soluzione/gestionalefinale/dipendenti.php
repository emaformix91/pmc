<?php 
include('./include/config.php');
include('./include/variabili.php');
if(!empty($dipendenti_include_list)){
	foreach ($dipendenti_include_list as $to_include) {
	include($to_include);
	}
}

$user_controller = new usercontroller();


$dipendenti_controller = new dipendenticontroller();

$valid_actions = array('index','detail','SendMail');

$action = isset( $_GET[ 'action' ] ) && in_array( $_GET[ 'action' ], $valid_actions ) ? $_GET[ 'action' ] : 'index';


$dipendenti_controller->$action();
include('./views/footer.php');

