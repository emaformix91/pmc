<!DOCTYPE html>
<html>
<?php if( $user_controller->isLogged() ): ?> 
<body>
    <?php if( !empty( $clienti) ):?>
	 <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
        <div class="serch">
        <div class="col-md-3 col-sm-3">
            <h1 style="text-align:center;margin-top:6px;">Ricerca Rapida:</h1>
        </div>
        <div class="col-md-9  col-sm-9">
        <form action="" method="POST">
                <input type="text" name="azienda" placeholder="Nome Azienda" value=""/>
                <input type="text" name="settore" placeholder="Settore Azienda" value=""/>
                <input type="text" name="email" placeholder="Email" value=""/>
                <input type="submit" class="btn btn-primary btn-material-blue-700" name="cerca" value="Cerca Brand"/>
        </form>
        </div>
    </div>
        <button class="btn btn-lg btn-primary btn-block btn-material-blue-700" data-toggle="modal" data-target="#simple-dialog">Aggiungi Nuovo Brand</button>
    <div id="simple-dialog" class="modal fade" tabindex="-1">
            <div class="modal-dialog">
              <div class="modal-content">
                <div class="modal-body">
     <form method="POST"  class="form-horizontal"  action="">
         <fieldset>
                    <div class="form-group">
                        <div class="col-md-12">
                        <label for="name">Nome Cliente</label></br>
                        
                        <select name="cliente_id" class="form-control floating-label login-field">
                            <?php foreach ($clienti as $cliente):?>
                                     <option value="<?php echo isset($cliente['id']) ? $cliente['id']: '' ?>"><?php echo $cliente['name']; ?></option>
                            <?php endforeach; ?>
                                  </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-12">
                        <label for="name">Nome</label></br>
                        <input type="text" class="form-control floating-label login-field" id="focusedInput" placeholder="Nome Brand" type="text" id="login-azienda" name="bazienda" value="<?php echo isset($brand['bazienda']) ? $brand['bazienda']: '' ?>"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-12">
                        <label for="name">Settore</label></br>
                        <input type="text" class="form-control floating-label login-field" placeholder="Settore Brand"  name="bsettore"  value="<?php echo isset($brand['bsettore']) ? $brand['bsettore']: '' ?>"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-12">
                        <label for="name">Telefono</label></br>
                        <input type="text" name="btelefono" class="form-control floating-label login-field" placeholder="Telefono Brand"  value="<?php echo isset($brand['btelefono']) ? $brand['btelefono']: '' ?>"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-12">
                        <label for="name">Email</label>
                        <input type="text" name="bemail" class="form-control floating-label login-field" placeholder="Email Brand" value="<?php echo isset($brand['bemail']) ? $brand['bemail']: '' ?>"/>
                        </div>
                      </div>
                    <div class="form-group">
                        <div class="col-md-12">
                        <label for="name">Descrizione</label></br>
                        <textarea type="text" name="bdescrizione" class="form-control floating-label login-field" placeholder="Descrizione Brand"  value="<?php echo isset($brand['bdescrizione']) ? $brand['bdescrizione']: '' ?>"/></textarea>
                        </div>
                    </div>
        <input type="submit" class="btn btn-primary btn-lg btn-block btn-material-blue-700" name="submit" value="aggiungi brand"/>
        </fieldset>
    </form>
    </div>
              </div>
            </div>
          </div>
          <?php if( isset( $_SESSION[ 'message' ] ) ){
                        echo '<h1 style="color:green;text-align:center;">'.$_SESSION[ 'message' ].'</h1>';
                        unset( $_SESSION[ 'message' ] );
                    } ?>

        <?php if(!empty($brands)):?>
	 	 <form method="POST" action="">
        <input type="submit" class="btn btn-primary btn-material-blue-700" name="update" value="modifica brand"/>
        <input type="submit" class="btn btn-primary btn-material-blue-700" name="delete" value="elimina brand"/>
    <div class="table-responsive">
    <table class="table table-striped">
        <thead> 
        <tr> 
            <th>#</th>
            <th>Nome Cliente</th>
            <th>Azienda</th>
            <th>Settore</th>
            <th>Email</th>
            <th>Modifica</th>
            <th>Seleziona</th>
        </tr>
        </thead>    
        <tbody>
            <?php $indice = 1;?>
        <?php foreach($brands as $brand):?>
        <tr>  
            <td>
                <?php echo $indice; $indice++; ?>
            </td> 
            <td>
                <?php foreach ($clienti as $cliente):
                if( $brand['clienti_id'] == $cliente['id'] ):
                    echo $cliente ['name'].' '.$cliente['cognome'];
                endif;
                endforeach; ?>
            </td>     
            <td>
                <input type="text" name="brands[<?php echo $brand['id'];?>][azienda]" value="<?php echo isset($brand['azienda']) ? $brand['azienda']: '' ?>"/>
            </td>
            <td>
                <input type="text" name="brands[<?php echo $brand['id'];?>][settore]" value="<?php echo isset($brand['settore']) ? $brand['settore']: '' ?>"/>
            </td>
            <td>
                <input type="text" name="brands[<?php echo $brand['id'];?>][email]" value="<?php echo isset($brand['email']) ? $brand['email']: '' ?>"/>
            </td>
            <td>
                <a href="brand.php?action=detail&brand_id=<?php echo $brand['id']?>">
                            <input type="button" class="btn btn-lg btn-primary btn-material-blue-grey-500" value="visualizza">
                        </a>
            </td>
            <td>
                <div class="togglebutton">
                  <label>
                    <input type="checkbox" name="id" value="<?php echo isset($brand['id']) ? $brand['id']: ''?>"/>
                      
                    </label>
                  </div>
            </td>  
        </tr>    
                <?php endforeach;?> 
    </tbody>
    </table>
    </div>
        <input type="submit" class="btn btn-primary btn-material-blue-700" name="update" value="modifica brand"/>
        <input type="submit" class="btn btn-primary btn-material-blue-700" name="delete" value="elimina brand"/>
    </form>
<?php endif; ?>
	 </div>
<?php endif; ?>
<?php if( empty($clienti)): ?>
    <h1 style="color:green;text-align:center;">Creare prima il cliente da associare dalla voce del menu 'clienti'</h1>
<?php endif; ?>

</body>
<?php else: header('Location:login.php');?>
<?php endif; ?> 
</html>