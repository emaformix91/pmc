<body>

    <?php
    
    $user_model = new usermodel();
    $serviziempty =$user_model->getServizi();
 if(!empty($serviziempty)):?>
<header><!-- HEADER START -->
    <div class="wrap">
    <nav class="navbar navbar-inverse navbar-fixed-top">
      <div class="container-fluid">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="#"><?php echo $autor; ?></a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
          <ul class="nav navbar-nav navbar-right">
            <?php for( $i=0;$i<count($top_menu_list);$i++ ):?>
            <li><a href="<?php echo $top_menu_list[$i]['link']?>"><?php echo $top_menu_list[$i]['name']?></a></li>
            <?php endfor;?>
          </ul>
           </div>
      </div>
    </nav>
            <div id="topbar"><!-- TOPBAR START -->
                <div class="container">
                    <div class="col-sm-3 col-md-2 sidebar">

                <?php 

                if( function_exists('printSocial')) {
                    printSocial($show_social, $socials_list); 
                }
                

                ?>  

                    <nav class="<?php echo $class_menu_alignment; ?>">
                        <ul class="nav nav-sidebar">
                            <li class="active"><a href="#">Overview Clienti<span class="sr-only">(current)</span></a></li>
                            <?php
                            $i = 0;
                            
                            while( $i < count( $menu_list ) ) {

                                $has_submenu = isset( $menu_list[$i]['submenu'] ) ;

                                if( $has_submenu ) echo "<li class='has-submenu'>";
                                else echo '<li>';

                                echo '<a href="'.$menu_list[$i]['link'].'" title="'.$menu_list[$i]['title'].'"><i class="fa fa-adjust"></i>'.$menu_list[$i]['name'].'</a>';
                                
                                if( $has_submenu ) {

                                    $submmenu = $menu_list[$i]['submenu'];

                                    echo '<ul class="submenu">';
                                   
                                   $r = 0;

                                    do {
                                       echo '<a href="#" title="'.$submmenu[$r].'">'.$submmenu[$r].'</a>';  
                                       $r++;   
                                    }
                                    while ( $r < count( $submmenu ) ) ;

                                    
                                    echo '</ul>';

                                }

                                echo "</li></br>";

                                $i++;
                                
                            }

                            // stampo i link relativi alla gestione utenti
                            

                            ?>    
                        </ul>
                    </nav>
                </div>
            </div><!-- END TOPBAR-->

        </header><!-- HEADER END -->
        <?php endif;?>