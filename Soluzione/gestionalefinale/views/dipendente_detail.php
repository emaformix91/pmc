
<!DOCTYPE html>

<?php if( $user_controller->isLogged() || $_SESSION['logged2'] == true ): ?>
<div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main custom-main">
          <div class="row placeholders custom-desc">
            <div class="col-md-12 col-xs-12 col-sm-12 placeholder">
              <div class="message">
          <?php if( isset( $_SESSION[ 'message' ] ) ){
                        echo '<h1 text-align:center;">'.$_SESSION[ 'message' ].'</h1>';
                        unset( $_SESSION[ 'message' ] );
                    } ?>
                  </div>
              <?php foreach ($dipendente as $dipendenteinfo): ?>
              <h1 class="page-header"><?php echo htmlspecialchars(stripcslashes($dipendenteinfo['name'])).' '.htmlspecialchars(stripcslashes($dipendenteinfo['cognome']));?>(Controlli sull update da fare)</h1>
            </div>
          </div>
          <form action="" id="infodipendente"class="form-signin" method="POST">
              <h3 class="text-left">info:</h3>
              <label  id="tel" for="password">Telefono:</label>
            <input type="text" autofocus=""  placeholder="Non disponibile" class="form-control" id="telefono" name="telefono" value="<?php echo isset($dipendenteinfo['telefono']) ? $dipendenteinfo['telefono']: '' ?>"/></br>
            <label  id="mail" for="password">Email:</label>
            <input type="text" autofocus=""  placeholder="Non disponibile" class="form-control" id="email" name="email" value="<?php echo isset($dipendenteinfo['email']) ? $dipendenteinfo['email']: '' ?>"/></br>
            <label  id="fiscal" for="password">Cod.Fiscale:</label>
             <input type="text" autofocus=""  placeholder="Non disponibile" class="form-control" id="codfiscale" name="codfiscale" value="<?php echo isset($dipendenteinfo['codfiscale']) ? $dipendenteinfo['codfiscale']: '' ?>"/></br>
             <label  id="settor" for="password">Settore:</label>
             <input type="text" autofocus=""  placeholder="Non disponibile" class="form-control" id="settore" name="settore" value="<?php echo isset($dipendenteinfo['settore']) ? $dipendenteinfo['settore']: '' ?>"/></br>
              <label id="role" for="password">Ruolo:</label>
             <input type="text" autofocus=""  placeholder="Non disponibile" class="form-control" id="ruolo" name="ruolo" value="<?php echo isset($dipendenteinfo['ruolo']) ? $dipendenteinfo['ruolo']: '' ?>"/></br>
              <label id="role" for="password">Change Password:</label>
             <input type="text" autofocus=""  placeholder="Inserire Nuova Password" class="form-control" id="pass" name="pass" value=""/></br>
              <input type="submit" name="update" id="update" value="modifica" class="btn btn-primary btn-lg btn-block btn-material-blue-700">
          </form>
            <?php endforeach;?> 
     <?php if(!empty($tasks_dip)):?>
     <form action="" method="POST">
   <div class="row placeholders custom-desc">
              <div class="col-md-12 col-xs-12 col-sm-12 placeholder">
    
    <input type="submit" name="completata" value="Completata" class="btn btn-primary btn-material-blue-700" data-toggle="modal"/>
    <input type="submit" name="ripristina" value="Ripristina" class="btn btn-primary btn-material-blue-700" data-toggle="modal"/>
  </div></div>
  <div class="row placeholders custom-desc">
              <div class="col-md-12 col-xs-12 col-sm-12 placeholder">

  <?php $collapseList = 0; ?> 
<?php foreach ($tasks_dip as $task):?>
  <?php  $collapseList++;?>
  <div class="col-md-2 col-xs-2 col-sm-2 placeholder">
                <div class="checkbox">
                  <label>
                    <input type="checkbox" name="id" value="<?php echo isset($task['id']) ? $task['id']: ''?>"/>
                    <span class="ripple"></span>
                     Checkbox
                  </label>
                </div>
</div>
 <div class="col-md-10 col-xs-10 col-sm-10 placeholder">               
                <!-- ACCORDION TASK -->

                <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                  

                  <div class="panel panel-default">
                     <?php if( $task['stato']== 'completata'):?>
                    <div class="panel-heading" style="background-color: #62B864;" role="tab" id="headingOne<?php echo $collapseList;?>">
                      <h4 class="panel-title">
                        <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne<?php echo $collapseList;?>" aria-expanded="" aria-controls="collapseOne<?php echo $collapseList;?>">
                          <input type="text" name="tasks[<?php echo $task['id']?>][titolo]" value="<?php echo isset($task['titolo']) ? $task['titolo']: '' ?>"/>
                        </a>
                      </h4>
                    </div>
                  <?php elseif ( $task['stato'] !== 'completata' && !empty($task['data_scadenza']) && $task['data_scadenza'] >= date('d/m/y')) :
                    foreach ($dipendente as  $dipendenteinfo) {
                    $mail = new DipendentiSendMail( $dipendenteinfo['email'],$task['titolo'],$dipendenteinfo['name'],$dipendenteinfo['id'] );
                    }
                    
                  ?>
                  <div class="panel-heading" style="background-color: red;" role="tab" id="headingOne<?php echo $collapseList;?>">
                      <h4 class="panel-title">
                        <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne<?php echo $collapseList;?>" aria-expanded="" aria-controls="collapseOne<?php echo $collapseList;?>">
                          <input type="text" name="tasks[<?php echo $task['id']?>][titolo]" value="<?php echo isset($task['titolo']) ? $task['titolo']: '' ?>"/>
                        </a>
                      </h4>
                    </div>
                  <?php else:?>
                  <div class="panel-heading" role="tab" id="headingOne<?php echo $collapseList;?>">
                      <h4 class="panel-title">
                        <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne<?php echo $collapseList;?>" aria-expanded="" aria-controls="collapseOne<?php echo $collapseList;?>">
                          <input type="text" name="tasks[<?php echo $task['id']?>][titolo]" value="<?php echo isset($task['titolo']) ? $task['titolo']: '' ?>"/>
                        </a>
                      </h4>
                    </div>
                <?php endif;?>
    <div id="collapseOne<?php echo $collapseList;?>" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
                      <div class="panel-body">
                        <div class="col-md-6">
                        <label for="name">Data Inizio</label>
                        <input type="text" autofocus=""  placeholder="Non disponibile" class="form-control" name="tasks[<?php echo $task['id']?>][data_inizio]" value="<?php echo isset($task['data_inizio']) ? $task['data_inizio']: '' ?>"/>
                        </div>
                        <div class="col-md-6">
                        <label for="name">Data Scadenza</label>
                        <input type="text" autofocus=""  placeholder="Non disponibile" class="form-control" name="tasks[<?php echo $task['id']?>][data_scadenza]" value="<?php echo isset($task['data_scadenza']) ? $task['data_scadenza']: '' ?>"/>
                        </div>
                        <label for="name">Appunti</label>
                        <textarea type="text" name="tasks[<?php echo $task['id']?>][descrizione]" value="<?php echo isset($task['descrizione']) ? $task['descrizione']: '' ?>"/><?php echo $task['descrizione'];?></textarea>
                      </div>
                    </div>
                  </div>
                  
                </div>
              </div>
              <?php endforeach; ?>
            </div>
    <input type="submit" name="completata" value="Completata" class="btn btn-primary btn-material-blue-700" data-toggle="modal"/>
    <input type="submit" name="ripristina" value="Ripristina" class="btn btn-primary btn-material-blue-700" data-toggle="modal"/>
              </div>
            </div>
            </form>
            </div> 
    <?php endif; ?>
	</div>
<?php else: header('Location:./login.php');
 endif;?>