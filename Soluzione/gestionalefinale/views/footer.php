<?php
if(!$show_footer)  return;
?>
 <footer>
    <a href="?action=logout">Logout</a>
      <a href="?action=index">Back to home</a>       
 <?php if(isset($js_list)): ?>

    <?php foreach($js_list as $name) :?>

                <script type="text/javascript" src="assets/js/<?php echo $name; ?>.js"></script>

               

    <?php endforeach; ?>

<?php endif; ?>
<script type="text/javascript">

$(document).ready(function() {
    
    $.material.init();
});
/*
function myFunction4() {
    document.getElementById("addtask").style.display = "block";
}
$(function() {
    $('.custom-datepicker').datepicker({
        format: 'dd/mm/yyyy',
        language: 'it'
    });
});
/*
$(function(){
    $('button').on('click',function()
    {
        $('.body').load('brand.php .message')
        });
});


$(function() {
$('button.json').on('click',function()
{
    $.getJSON('',function (data)
    {
        insertItems(data);
    });
});
var insertItems = function (data)
{
    var cont = $('.container').empty();
    var ul = $('<ul />');
    $(data).each(function(i, item)
    {
        var text = '<b>'+item.Titolo + '</b>-' +item.testo;
        $('<li />',{html:text}).append(ul);
    })
    ul.uppendTo(cont);
}
});
$(document).ready(function() {
    $(function(){
$('#ajaxform').on('submit',function(e){
    e.preventDefault();
    var id = $(':checked').val();
    $.ajax({
        url:'prova.php',
        type:'POST',
        dataType: 'json',
        data: 'id='+ id + '&send=1&ajax=1',
    }).done(function (data) {
        insertItems(data);
        
    }).fail(function (err) {
        console.log(err);
    }).always(function () {
        console.log('Ciao');
    });
});
var insertItems = function (data)
{
    var cont = $('.message').empty();
    var h1 = $('<h1 />');
    $(data).each(function(i)
    {   
        $('<h1 />', { html: data[i].message }).appendTo(h1);   
    })
    h1.appendTo(cont);
}
});
$(function() {
    $('.custom-datepicker').datepicker({
        format: 'dd/mm/yyyy',
        language: 'it'
    });
});
});$.noConflict();
jQuery( document ).ready(function( $ ) {
  // Code that uses jQuery's $ can follow here.
});
*/

$(document).ready(function(){


                var form = $('form#infodipendente'),
                    submit = $('#update'),
                    valueSubmit = submit.val(),

                    addMessage = function(message) {
                        var message = $('<div />').addClass('message').html('<p>' + message + '</p>');

                        form.find('label').before( message );
                    },

                    addError = function(el, message) {
                        el.addClass('error');
                    };

                form

                    // name
                    .on( 'focusout', '#telefono', function() {
                        $(this).removeClass('error').prev().find('.error').remove();
                        $('#tel').removeClass('error').find('.error').remove();
                        $('#tel').removeClass('ok').find('.ok').remove();

                        if ( $(this).val() == '' ) {
                            $(this).addClass('error');
                            $('#tel').append( '<span class="error">Inserire il telefono</span>' );
                        }
                        else{
                            $('#tel').append( '<span class="ok">OK</span>' ); 
                        }
                        
                    })

                    // email
                    .on( 'focusout', '#email', function() {
                        $(this).removeClass('error').prev().find('.error').remove();
                        $('#mail').removeClass('error').find('.error').remove();
                        $('#mail').removeClass('ok').find('.ok').remove();

                        var re = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;

                        if ( $(this).val() == '' || ! re.test( $(this).val() ) ) {
                            $(this).addClass('error');
                            $('#mail').append( '<span class="error">Inserire la mail corretta</span>' );
                        }
                        else{
                            $('#mail').append( '<span class="ok">OK</span>' ); 
                        }
                    })

                    // subject
                    .on( 'focusout', '#codfiscale', function() {
                        $(this).removeClass('error').prev().find('.error').remove();
                        $('#fiscal').removeClass('error').find('.error').remove();
                        $('#fiscal').removeClass('ok').find('.ok').remove();

                        if ( $(this).val() == '' ) {
                            $(this).addClass('error');
                            $('#fiscal').append( '<span class="error">Inserire il codfiscale</span>' );
                        }
                        else{
                            $('#fiscal').append( '<span class="ok">OK</span>' ); 
                        }
                    })

                    // message
                    .on( 'focusout', '#settore', function() {
                        $(this).removeClass('error').prev().find('.error').remove();
                        $('#settor').removeClass('error').find('.error').remove();
                        $('#settor').removeClass('ok').find('.ok').remove();

                        if ( $(this).val() == '' ) {
                            $(this).addClass('error');
                            $('#settor').append( '<span class="error">Inserire il settore</span>' );
                        }
                        else{
                            $('#settor').append( '<span class="ok">OK</span>' ); 
                        }
                    })

                    .on( 'focusout', '#ruolo', function() {
                        $(this).removeClass('error').prev().find('.error').remove();
                        $('#role').removeClass('error').find('.error').remove();
                        $('#role').removeClass('ok').find('.ok').remove();

                        if ( $(this).val() == '' ) {
                            $(this).addClass('error');
                            $('#role').append( '<span class="error">Inserire il ruolo</span>' );
                        }
                        else{
                            $('#role').append( '<span class="ok">OK</span>' ); 
                        }
                    })
                    .on( 'submit', function(){
                        $(this).find('#telefono').trigger( 'focusout' );
                        $(this).find('#email').trigger( 'focusout' );
                        $(this).find('#codfiscale').trigger( 'focusout' );
                        $(this).find('#settore').trigger( 'focusout' );
                        $(this).find('#ruolo').trigger( 'focusout' );

                        if ( $(this).find('.error').length != 0 ) {
                            return false;
                        }
                    });

            });
</script>
        </footer>


