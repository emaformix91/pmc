<!DOCTYPE html>
<html>
<?php if(empty($admin)):?>
    <div class="container">
                <div class="row">
    <div class="col-sm-8 col-sm-offset-2 col-md-8 col-md-offset-2 main">
    <form method="POST" class="form-signin" action="">
        <label for="name">Nome</label></br>
        <input type="text" autofocus=""  placeholder="Nome" class="form-control" name="name" id="name" value="<?php echo isset($admin['name']) ? $admin['name']: '' ?>"/></br>
        <label for="name">Cognome</label></br>
        <input type="text" autofocus=""  placeholder="Cognome" class="form-control" name="cognome" id="cognome" value="<?php echo isset($admin['cognome']) ? $admin['cognome']: '' ?>"/></br>
        <label for="name">Email</label></br>
        <input type="text" autofocus=""  placeholder="Email" class="form-control" name="email" id="email" value="<?php echo isset($admin['email']) ? $admin['email']: '' ?>"/></br>
        <label for="name">Azienda</label></br>
        <input type="text" autofocus=""  placeholder="Azienda" class="form-control" name="azienda" id="azienda" value="<?php echo isset($admin['azienda']) ? $admin['azienda']: '' ?>"/></br>
        <label for="name">Username</label></br>
        <input type="text" autofocus=""  placeholder="Username" class="form-control" name="username" id="username" value="<?php echo isset($admin['username']) ? $admin['username']: '' ?>"/></br>
        <label for="name">Password</label></br>
        <input type="text" autofocus=""  placeholder="Password" class="form-control" name="password" id="password" value="<?php echo isset($admin['password']) ? $admin['password']: '' ?>"/></br>
        <input type="submit" class="btn btn-primary btn-lg btn-block btn-material-blue-700" name="submit1" value="Save"/>
    </form>
</div>
</div>
</div>
<?php else:?>
    <?php if( $user_controller->isLogged() ): ?>
        <?php if(empty($num_serv)):?>
        <div class="container">
                <div class="row">
        <div class="col-sm-8 col-sm-offset-2 col-md-8 col-md-offset-2 main">
            <form method="POST" class="form-signin align" action="">
                    <label for="numero_servizi"><h3>Selezionare il numero dei Servizi offerti</h3></label></br>
                    <input type="number" name="num_serv"  min="1" max="2000" value=""/></br>
                    <input type="submit" class="btn btn-primary btn-lg btn-block btn-material-blue-700" name="submit2" value="Save"/>
                </form>
        </div>
        </div>
        </div>  
        <?php else:?>
            <?php if(empty($servizi)):?>
                <?php foreach ($num_serv as $number):?>
                <?php if($number['numero']==1):?>
                <div class="wrap">
            <div class="container">
                <div class="row">
                <div class="col-sm-8 col-sm-offset-2 col-md-8 col-md-offset-2 main">
                    <div class="message">
                    </div>
                    <h1>Hai selezionato un servizio</h1>
                    <form method="POST"  action="">
                        <input type="hidden" name="id"  value="<?php echo isset($number['id']) ? $number['id']: '' ?>"/>
                        </br>
                        <input type="submit" class="btn btn-primary btn-lg btn-block btn-material-blue-700" name="del" value="Torna alla sessione precedente"/>
                    </form>
            
               
                    <?php for ($i=1; $i <$number['numero']+1 ; $i++):?>
                    <form method="POST" class="form-signin" action="">
                        <?php echo '<h2>Selezionare il Nome del servizio offerto</h2></br>' ;?>
                        <label for="servizi">Servizio<?php echo $i;?></label></br>
                        <input type="text" autofocus=""  placeholder="Nome" class="form-control" name="name" id="servizio<?php echo $i;?>" value="<?php echo isset($servizi['name']) ? $servizi['name']: '' ?>"/></br>
                        <input type="submit"  class="btn btn-primary btn-lg btn-block btn-material-blue-700" name="submit3" value="Conferma"/></br>
                    </form>
                    <?php endfor;?> 
                </div> 
                 </div>
            </div>
        </div>  
                <?php else:?>

                <h1>Hai selezionato<?php echo ' '.$number['numero'].' ';?>servizi</h1>
                 <div class="container">
                <div class="row">
                <div class="col-sm-8 col-sm-offset-2 col-md-8 col-md-offset-2 main">
                <form method="POST" action="">
                    <input type="hidden" name="id"  value="<?php echo isset($number['id']) ? $number['id']: '' ?>"/>
                    </br>
                    <input type="submit" class="btn btn-primary btn-lg btn-block btn-material-blue-700" name="del" value="Torna alla sessione precedente"/>
                </form>
                <?php echo '<h2>Selezionare il Nome dei '.$number['numero'].' servizi offerti</h2></br>' ;?>
                <form method="POST" action="">
                <?php for ($i=1; $i <$number['numero']+1 ; $i++):?>
                    <label for="servizi_<?php echo $i;?>">Servizio<?php echo $i;?></label></br>
                    <input type="text" autofocus=""  placeholder="Nome" class="form-control" name="servizi[<?php echo $i;?>][name]" id="servizio<?php echo $i;?>" value="<?php echo isset($servizi['name']) ? $servizi['name']: '' ?>"/></br>
                <?php endfor;?>     
                    <input type="submit" class="btn btn-primary btn-lg btn-block btn-material-blue-700" name="submit4" value="Conferma"/>
                </form>        
                <?php endif;?> 
                <?php endforeach;?>
            <?php else:?>
            <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
          <h1 class="page-header">Dashboard</h1>

          <div class="row placeholders">
            <div class="col-xs-6 col-sm-3 placeholder">
              <img data-src="holder.js/200x200/auto/sky" class="img-responsive" alt="200x200" src="data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9InllcyI/PjxzdmcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB3aWR0aD0iMjAwIiBoZWlnaHQ9IjIwMCIgdmlld0JveD0iMCAwIDIwMCAyMDAiIHByZXNlcnZlQXNwZWN0UmF0aW89Im5vbmUiPjxkZWZzLz48cmVjdCB3aWR0aD0iMjAwIiBoZWlnaHQ9IjIwMCIgZmlsbD0iIzBEOEZEQiIvPjxnPjx0ZXh0IHg9Ijc1LjUiIHk9IjEwMCIgc3R5bGU9ImZpbGw6I0ZGRkZGRjtmb250LXdlaWdodDpib2xkO2ZvbnQtZmFtaWx5OkFyaWFsLCBIZWx2ZXRpY2EsIE9wZW4gU2Fucywgc2Fucy1zZXJpZiwgbW9ub3NwYWNlO2ZvbnQtc2l6ZToxMHB0O2RvbWluYW50LWJhc2VsaW5lOmNlbnRyYWwiPjIwMHgyMDA8L3RleHQ+PC9nPjwvc3ZnPg==" data-holder-rendered="true">
              <h4>Clienti Raggiunti: <?php echo htmlspecialchars(stripslashes(count($clienti)));?></h4>
            </div>
          </div>
           <div class="serch">
        <div class="col-md-3 col-sm-3">
            <h1 style="text-align:center;margin-top:6px;">Ricerca Rapida:</h1>
        </div>
        <div class="col-md-9  col-sm-9">
        <form action="" method="POST">
                <input type="text" name="name" placeholder="Nome Cliente" value=""/>
                <input type="text" name="cognome" placeholder="Cognome cliente" value=""/>
                <input type="text" name="email" placeholder="Email" value=""/>
                <input type="submit" class="btn btn-primary btn-material-blue-700" name="cerca" value="Cerca Cliente"/>
        </form>
        </div>
    </div>
     <button class="btn btn-primary btn-lg btn-block btn-material-blue-700" data-toggle="modal" data-target="#simple-dialog">Aggiungi Nuovo Cliente</button>    
     <div id="simple-dialog" class="modal fade" tabindex="-1">
            <div class="modal-dialog">
              <div class="modal-content">
                <div class="modal-body">
     <form method="POST"  class="form-horizontal"  action="">
         <fieldset>
                    <div class="form-group">
                        <div class="col-md-12">
                        <label for="name">Nome</label></br>
                        <input type="text" class="form-control floating-label login-field" id="focusedInput" placeholder="Nome Cliente" type="text" id="login-name" name="cname" value="<?php echo isset($cliente['cname']) ? $cliente['cname']: '' ?>"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-12">
                        <label for="name">Cognome</label></br>
                        <input type="text" class="form-control floating-label login-field" placeholder="Cognome Cliente"  name="ccognome"  value="<?php echo isset($cliente['ccognome']) ? $cliente['ccognome']: '' ?>"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-12">
                        <label for="name">Indirizzo</label></br>
                        <input type="text" name="cindirizzo" class="form-control floating-label login-field" placeholder="Indirizzo Cliente" id="indirizzo" value="<?php echo isset($cliente['cindirizzo']) ? $cliente['indirizzo']: '' ?>"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-12">
                        <label for="name">Partita iva</label></br>
                        <input type="text" name="civa" class="form-control floating-label login-field" placeholder="Partita iva Cliente" id="iva" value="<?php echo isset($cliente['civa']) ? $cliente['civa']: '' ?>"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-12">
                        <label for="name">Telefono</label></br>
                        <input type="text" name="ctelefono" class="form-control floating-label login-field" placeholder="telefono" id="telefono" value="<?php echo isset($cliente['ctelefono']) ? $cliente['ctelefono']: '' ?>"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-12">
                        <label for="name">Email</label>
                        <input type="text" name="cemail" class="form-control floating-label login-field" placeholder="Email" value="<?php echo isset($cliente['cemail']) ? $cliente['cemail']: '' ?>"/>
                        </div>
                      </div>
        <input type="submit" class="btn btn-primary btn-lg btn-block btn-material-blue-700" name="submit5" value="aggiungi"/>
        </fieldset>
    </form>
    </div>
              </div>
            </div>
          </div>
          <div class="message">
          <?php if( isset( $_SESSION[ 'message' ] ) ){
                        echo '<h1 class="message" style="color:green;text-align:center;">'.$_SESSION[ 'message' ].'</h1>';
                        unset( $_SESSION[ 'message' ] );
                    } ?>
                </div>
    <form method="POST" action="" id="ajaxform">
        <input type="submit" class="btn btn-primary btn-material-blue-700" name="update" value="modifica cliente"/>
        <input type="submit" class="btn btn-primary btn-material-blue-700" name="delete" value="elimina cliente"/>
    <div class="table-responsive">
    <table class="table table-striped">
        <thead> 
        <tr> 
            <th>#</th>
            <th>Nome</th>
            <th>Cognome</th>
            <th>Email</th>
            <th>Modifica</th>
            <th>Seleziona</th>
        </tr>
        </thead>    
        <tbody>
            <?php $indice = 1; ?>
        <?php foreach($clienti as $cliente):?>
        <tr>  
            <td>
                <?php echo $indice; $indice++; ?>
            </td>      
            <td>
                <input type="text" name="clienti[<?php echo $cliente['id'];?>][name]" value="<?php echo isset($cliente['name']) ? $cliente['name']: '' ?>"/>
            </td>
            <td>
                <input type="text" name="clienti[<?php echo $cliente['id'];?>][cognome]" value="<?php echo isset($cliente['cognome']) ? $cliente['cognome']: '' ?>"/>
            </td>
            <td>
                <input type="text" name="clienti[<?php echo $cliente['id'];?>][email]" value="<?php echo isset($cliente['email']) ? $cliente['email']: '' ?>"/>
            </td>
            <td>
                <a href="?action=detail&cliente_id=<?php echo $cliente['id']?>">
                            <input type="button" class="btn btn-lg btn-primary btn-material-blue-grey-500" value="visualizza">
                        </a>
            </td>
            <td>
                <div class="togglebutton">
                  <label>Seleziona Cliente
                    <input type="checkbox" id="id" name="id" value="<?php echo isset($cliente['id']) ? $cliente['id']: ''?>"/>
                      
                    </label>
                  </div>
            </td>  
        </tr>    
                <?php endforeach;?> 
    </tbody>
    </table>
    </div>
        <input type="submit" class="btn btn-primary btn-material-blue-700" name="update" value="modifica cliente"/>
        <input type="submit" class="btn btn-primary btn-material-blue-700" name="delete" id="delete" value="elimina cliente"/>
    </form>
            <?php endif;?>           
        <?php endif;?>
    <?php else: header('Location:login.php');?>   
    <?php endif;?>

            
<?php endif;?>
    	</body>
       
    	</html>	