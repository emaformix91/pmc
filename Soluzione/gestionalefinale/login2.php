<?php
    include('./include/config.php');
include( './include/variabili.php' );
if(! empty($login2_include_list)){
  foreach ($login2_include_list as $to_include) {
  include ($to_include);
  } 
}
    $userdip_controller = new userDipendenteController();
?>
<!DOCTYPE html>
<html lang="it">
    <head>
        <meta charset="utf8">
        <title>Login</title>
    </head>
    <body>
        <div class="message">
            <?php
                if( isset( $_SESSION[ 'message' ] ) ){
                    echo '<h1>'.$_SESSION[ 'message' ].'<h1>';
                    unset($_SESSION[ 'message' ] );
                }
            ?>
        </div>
 <div class="wrap">
        <div class="container">
            <div class="row">
                <div class="col-md-4 col-md-offset-4 panel panel-default" style="margin-top: 250px;">
        <form method="POST" action="?action=login" class="form-signin">
            <h2 class="form-signin-heading">Accedi:</h2>
            <label class="sr-only" for="usermane">Email</label>
            <input type="text" autofocus="" required="" placeholder="Email" class="form-control" id="username" name="username"/><br/>
            <label class="sr-only" for="password">Passowrd</label>
            <input type="password" required="" placeholder="Password" class="form-control" id="password" name="password" kl_virtual_keyboard_secure_input="on"/><br/>
            <input type="submit" class="btn btn-lg btn-primary btn-block btn-material-blue-700" value="Login"/>
        </form>

        <p>
            <a href="./clienti.php">Back to home</a></br><a href="./login.php">Click per accedere come Admin</a>
        </p>
    </body>
</html>
