<head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title><?php echo TITOLO_PAGINA;?></title>

        <?php if(isset($css_list)): ?>

            <?php foreach($css_list as $name) :?>

                <link rel="stylesheet" href="assets/css/<?php echo $name; ?>.css">
               

            <?php endforeach; ?>

        <?php endif; ?>

    <link href='http://fonts.googleapis.com/css?family=Roboto:100,300,400,500' rel='stylesheet' type='text/css'>
    <link href="http://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
</head>