<?php
class DipendentiSendMail extends SuperSendMail{
	
	public function __construct( $mail, $titolo = false, $name_dest, $id ){
		if( $titolo = false ){
			$oggetto = 'Task Assegnata';
      		$messaggio = '<html>
							<head>
 								<title>Nuova Task Assegnata</title>
							</head>
							<body>
								<h1>Salve '.$name_dest.'</h1></br>
								<p>Per visualizzare la task da fare urgentemente fare click su<a href:http://localhost/gestionaleprova/dipendenti.php?action=detail&brand_id='.$id.'>GO</a></p>
							</body>
						</html>';
			$intestazioni  = "MIME-Version: 1.0\r\n";
      		$intestazioni .= "Content-type: text/html; charset=iso-8859-1\r\n";
      		$intestazioni .= "To: ".$name_dest." ".$mail."\r\n";
      		$intestazioni .= "From:  <compleanni@example.com>\r\n";
			$SendMail = new SuperSendMail( $mail,$oggetto,$messaggio,$intestazioni );
			return $SendMail;
		}
		else{
			$oggetto = 'Task Scaduta '.$titolo;
      		$messaggio = '<html>
							<head>
 								<title>Promemoria Scadenza Task</title>
							</head>
							<body>
								<h1>Salve '.$name_dest.'</h1></br>
								<p>Per visualizzare la task da fare urgentemente fare click su<a href:http://localhost/gestionaleprova/dipendenti.php?action=detail&brand_id='.$id.'>GO</a></p>
							</body>
						</html>';
						$intestazioni  = "MIME-Version: 1.0\r\n";
      					$intestazioni .= "Content-type: text/html; charset=iso-8859-1\r\n";
      					$intestazioni .= "To: ".$name_dest." ".$mail."\r\n";
      					$intestazioni .= "From:  <compleanni@example.com>\r\n";
						$SendMail = new SuperSendMail( $mail,$oggetto,$messaggio,$intestazioni );
						return $SendMail;
		}
    }
}