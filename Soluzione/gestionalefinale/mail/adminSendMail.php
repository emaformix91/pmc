<?php
class AdminSendMail extends SuperSendMail{
	private $mail;
	private $titolo;
	private $name_dest;
	private $id;

	public function __construct( $m,$t,$n,$i ){
		$this-> mail = $m;
		$this-> titolo = $t;
		$this-> name_dest = $n;
		$this-> id = $i;
    
  	}
  	public function Scadenza(){
  		$oggetto = 'Task Scaduta '.$this-> titolo;
      	$messaggio = '<html>
						<head>
 							<title>Promemoria Scadenza Task</title>
						</head>
						<body>
							<h1>Salve '.$this-> name_dest.'</h1></br>
								<p>task scaduta '.$this-> titolo.'</p>
						</body>
					</html>';
				$intestazioni  = "MIME-Version: 1.0\r\n";
      			$intestazioni .= "Content-type: text/html; charset=iso-8859-1\r\n";
      			$intestazioni .= "To: ".$this-> name_dest." ".$this-> mail."\r\n";
      			$intestazioni .= "From:  <compleanni@example.com>\r\n";
				$SendMail = new SuperSendMail( $this-> mail,$oggetto,$messaggio,$intestazioni );
				return $SendMail;
  	}
  	public function Completata(){
  		$oggetto = 'Task Completata '.$this-> titolo;
      	$messaggio = '<html>
						<head>
 							<title>Avviso Task</title>
						</head>
						<body>
							<h1>Salve '.$this-> name_dest.'</h1></br>
								<p>Task '.$this-> titolo.' completata</p>
						</body>
					</html>';
				$intestazioni  = "MIME-Version: 1.0\r\n";
      			$intestazioni .= "Content-type: text/html; charset=iso-8859-1\r\n";
      			$intestazioni .= "To: ".$this-> name_dest." ".$this-> mail."\r\n";
      			$intestazioni .= "From:  <compleanni@example.com>\r\n";
				$SendMail = new SuperSendMail( $this-> mail,$oggetto,$messaggio,$intestazioni );
				return $SendMail;
  	}
}
?>