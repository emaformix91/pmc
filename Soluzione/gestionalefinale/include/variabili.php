<?php
/* HEAD */

$css_list = array( 'bootstrap.min' , 'ripples.min' , 'material-wfont.min' , 'font-awesome.min' , 'dashboard' , 'datepicker' , 'custom') ;

/* FINE HEAD */

/* HEADER */

// SOCIAL

$show_social = true;

$socials_list['fb'] = 'facebook';
$socials_list['tw'] = 'twitter';
$socials_list['yt'] = 'youtube';

//TOP MENU
$autor = 'Flavio';

$link_impostazioni = array(

    "link" => '/'.ROOT_NAME.'/impostazioni.php',
    "title" => 'impostazioni',
    "name" => 'Impostazioni',

);



$link_esci = array(

    "link" => '?action=logout',
    "title" => 'esci',
    "name" => 'Esci',

);

$top_menu_list[0] = $link_impostazioni;



$top_menu_list[1] = $link_esci;


//LEFT MENU

$link_clienti = array(

    "link" => '/'.ROOT_NAME.'/clienti.php',
    "title" => 'clienti',
    "name" => 'Clienti',

);

$link_brand = array(

    "link" => '/'.ROOT_NAME.'/brands.php',
    "title" => 'brands',
    "name" => 'Brands',

);

$link_dipendenti = array(

    "link" => '/'.ROOT_NAME.'/dipendenti.php',
    "title" => 'dipendenti',
    "name" => 'Dipendenti',

);


$link_servizi = array(

    "link" => '/'.ROOT_NAME.'/servizi.php',
    "title" => 'servizi',
    "name" => 'Servizi',

);



$menu_list[0] = $link_clienti;
$menu_list[1] = $link_brand;
$menu_list[2] = $link_dipendenti;
$menu_list[3] = $link_servizi;


// 0 = center , 1 = left , 2 = right
$setting_menu_alignent = '0';

$class_menu_alignment =  "right";


switch ($setting_menu_alignent) {
    case 0 :
        $class_menu_alignment = "center";
        break;
    case 1 :
        $class_menu_alignment = "left";
        break;
    case 2 :
        $class_menu_alignment = "right";
        break;
    default:
        $class_menu_alignment = "right";
}

/* FINE HEADER */

/* FOOTER */

$show_footer = true;

$js_list = array( 'jquery-1.10.2.min' , 'bootstrap.min' , 'bootstrap-datepicker' , 'locales/bootstrap-datepicker.it' , 'ripples.min' , 'material.min') ;

$footer_text = 'Gestionale Flavio WEB';

/* END FOOTER */


/* GESTIONE UTENTI */


$tipologie[] = 'Admin';
$tipologie[] = 'User';

/* GESTIONE INCLUDE */
$header_include_list = array('./lib/DbConnector.php', './models/usermodel.php' );

$impostazioni_include_list = array('./lib/DbConnector.php', './models/usermodel.php','./controllers/usercontroller.php','./controllers/SuperControllers.class.php','./controllers/impostazionicontroller.php','./models/SuperModels.class.php' ,'./models/impostazionimodel.php', './head/head.php','./views/header.php');

$client_include_list = array('./lib/DbConnector.php','./controllers/usercontroller.php','./controllers/SuperControllers.class.php','./controllers/clientecontroller.php','./models/SuperModels.class.php' , './models/clientemodel.php', './models/usermodel.php', './controllers/brandcontroller.php','./models/brandmodel.php','./head/head.php','./views/header.php');

$login_include_list = array('./lib/DbConnector.php' , './controllers/usercontroller.php' , './models/usermodel.php' , './head/head.php');

$login2_include_list = array('./lib/DbConnector.php' , './controllers/userdipendenticontroller.php' , './models/usermodel.php' , './head/head.php');

$brand_include_list = array('./lib/DbConnector.php' , './controllers/usercontroller.php' , './controllers/SuperControllers.class.php', './controllers/brandcontroller.php' ,'./mail/SuperSendMail.class.php','./mail/dipendentiSendMail.php', './models/usermodel.php' , './models/SuperModels.class.php' , './models/brandmodel.php' , './head/head.php' , './views/header.php');

$brands_include_list = array('./lib/DbConnector.php' , './controllers/usercontroller.php' , './controllers/SuperControllers.class.php', './controllers/brandscontroller.php' , './controllers/brandcontroller.php' , './models/usermodel.php' , './models/SuperModels.class.php' , './models/brandsmodel.php' , './models/brandmodel.php' , './head/head.php' , './views/header.php');

$dipendenti_include_list = array('./lib/DbConnector.php' , './controllers/usercontroller.php' , './controllers/SuperControllers.class.php' , './controllers/dipendenticontroller.php' ,'./mail/SuperSendMail.class.php','./mail/dipendentiSendMail.php', './models/usermodel.php' , './models/SuperModels.class.php' , './models/dipendentimodel.php' ,'./head/head.php' , './views/header.php');

$servizi_include_list = array('./lib/DbConnector.php','./controllers/usercontroller.php','./controllers/SuperControllers.class.php','./controllers/servizicontroller.php','./models/SuperModels.class.php' , './models/usermodel.php', './models/servizimodel.php','./head/head.php','./views/header.php');

/* STATO SERVIZI INPUT OPTION */


$stato_servizi[] = 'Saldato Acconto';
$stato_servizi[] = 'Saldato Totale';
$stato_servizi[] = 'Non Saldato';