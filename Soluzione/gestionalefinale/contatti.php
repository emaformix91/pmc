<html>

<head>
  <link href="http://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css" rel="stylesheet">
  <link href="css/ripples.min.css" rel="stylesheet">
  <link href="css/material-wfont.min.css" rel="stylesheet">
  <link href="http://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
  <link href="http://getbootstrap.com/examples/dashboard/dashboard.css" rel="stylesheet">
  <link href="css/datepicker.css" rel="stylesheet">
  <link href="css/custom.css" rel="stylesheet">
  <style type="text/css">
    .checkbox {
      margin-bottom: 23px;
    }
    .panel-group {
      margin-top: 8px;
    }
  </style>
</head>

<body>

  <div class="wrap">
    <nav class="navbar navbar-inverse navbar-fixed-top">
      <div class="container-fluid">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="#">BrainMood</a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
          <ul class="nav navbar-nav navbar-right">
            <li><a href="#">Dashboard</a></li>
            <li><a href="#">Impostazioni</a></li>
            <li><a href="#">Esci</a></li>
          </ul>
       <!--  <form class="navbar-form navbar-right">
            <input type="text" class="form-control" placeholder="Search...">
          </form> -->
        </div>
      </div>
    </nav>
    <div class="container-fluid">
      <div class="row">

        <div class="col-sm-3 col-md-2 sidebar">
          <ul class="nav nav-sidebar">
            <li class="active"><a href="#">Overview Clienti<span class="sr-only">(current)</span></a></li>
            <li><a href="#"><i class="fa fa-adjust"></i>La Coppia Garbata</a></li>
            <li><a href="#"><i class="fa fa-adjust"></i>KreaKonsulting</a></li>
            <li><a href="#"><i class="fa fa-adjust"></i>EU Aviation Academy</a></li>
          </ul>
          <ul class="nav nav-sidebar">
            <li><a href="#"><i class="fa fa-adjust"></i>The Body Shop</a></li>
            <li><a href="#"><i class="fa fa-adjust"></i>Fenix</a></li>
            <li><a href="#"><i class="fa fa-adjust"></i>Lynnebay</a></li>
            <li><a href="#"><i class="fa fa-adjust"></i>Cliente</a></li>
            <li><a href="#"><i class="fa fa-adjust"></i>Cliente</a></li>
          </ul>
          <ul class="nav nav-sidebar">
            <li><a href="#"><i class="fa fa-adjust"></i>Cliente</a></li>
            <li><a href="#"><i class="fa fa-adjust"></i>Cliente</a></li>
            <li><a href="#"><i class="fa fa-adjust"></i>Cliente</a></li>
          </ul>
          <ul style="box-shadow:0 1px rgba(0, 0, 0, 0.1) inset; padding-top: 8px; text-align: center;" class="list-inline">
            <li><a href="#">Privacy</a></li>
            <li><a href="#">Termini</a></li>
            <li><a href="#">Guida</a></li>
          </ul>
        </div>

        <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main custom-main">

          <div class="row placeholders custom-desc">
            <div class="col-md-12 col-xs-12 col-sm-12 placeholder">
              <h1 class="page-header">The Body Shop Italia</h1>
              <p class="custom-address"><span>Indirizzo email:</span> <span>info@the-body-shop.it</span></p>
            </div>
          </div>

          <div class="row placeholders custom-desc">
            <div class="col-md-12 col-xs-12 col-sm-12 placeholder">
              <h3 class="text-left">Descrizione dell'attivit&agrave;</h3>
              <p class="text-left">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi sodales ipsum vitae justo vehicula fringilla. Pellentesque sed sapien nec diam viverra tempor vel eu nisl. Vestibulum vitae quam at magna cursus dictum. Pellentesque sapien lorem, elementum quis eros nec, ornare maximus libero. Nulla aliquam libero ut sapien rhoncus varius. Proin nec iaculis mauris. Sed maximus sem at porttitor posuere.
                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi sodales ipsum vitae justo vehicula fringilla. Pellentesque sed sapien nec diam viverra tempor vel eu nisl. Vestibulum vitae quam at magna cursus dictum. Pellentesque sapien lorem, elementum quis eros nec, ornare maximus libero. Nulla aliquam libero ut sapien rhoncus varius. Proin nec iaculis mauris. Sed maximus sem at porttitor posuere.</p>
              </div>
            </div>

            <div class="row placeholders custom-desc">
              <div class="col-md-2 col-xs-12 col-sm-6 placeholder">
                <h3 class="text-left margin-bottom">Servizi Attivi</h3>
                <ul class="text-left">
                  <li>SEO</li>  
                </ul>
              </div>

              <div class="col-md-2 col-xs-12 col-sm-6 placeholder">
                <h3 class="text-center margin-bottom">Data di Attivazione</h3>
                <span>
                  <input class="custom-datepicker center-block" />
                </span>
              </div>
              <div class="col-md-2 col-xs-12 col-sm-6 placeholder border-right">
                <h3 class="text-center">Termina Servizio</h3>
                <button class="btn btn-primary btn-material-blue-700" data-toggle="modal" data-target="#simple-dialog">Conferma</button>
              </div>



              <div class="col-md-2 col-xs-12 col-sm-6 placeholder">
              <h3 class="text-left margin-bottom">Servizi Passati</h3>
                <ul class="text-left">
                  <li>Sito Web</li>   
                </ul>
              </div>

              <div class="col-md-4 col-xs-12 col-sm-6 placeholder">
                <h3 class="text-left margin-bottom">Durata:</h3>
                <div class="pull-left input-daterange">
                  <ul class="list-inline">
                    <li>
                      <span class="add-on">Da</span>
                      <input class="custom-datepicker" />
                    </li>
                    <li>
                      <span class="add-on">a</span>
                      <input class="custom-datepicker" />
                    </li>
                  </ul>
                </div>
              </div>
            </div>
            <div class="row placeholders custom-desc">
              <div class="col-md-12 col-xs-12 col-sm-12 placeholder">
                <button class="btn btn-primary btn-lg btn-block btn-material-blue-700" type="submit">Salva Modifiche</button>
              </div>
            </div>
            <div class="row placeholders custom-desc">
              <div class="col-md-12 col-xs-12 col-sm-12 placeholder">
                <h3 class="text-left">Task</h3>
              </div>

              <div class="col-md-2 col-xs-2 col-sm-2 placeholder">
                <div class="checkbox">
                  <label>
                    <input type="checkbox"> Checkbox
                  </label>
                </div>
                <div class="checkbox">
                  <label>
                    <input type="checkbox"> Checkbox
                  </label>
                </div>
                <div class="checkbox">
                  <label>
                    <input type="checkbox"> Checkbox
                  </label>
                </div>
                <div class="checkbox">
                  <label>
                    <input type="checkbox"> Checkbox
                  </label>
                </div>
                <div class="checkbox">
                  <label>
                    <input type="checkbox"> Checkbox
                  </label>
                </div>
                <div class="checkbox">
                  <label>
                    <input type="checkbox"> Checkbox
                  </label>
                </div>
              </div>

              <div class="col-md-10 col-xs-10 col-sm-10 placeholder">               
                <!-- ACCORDION TASK -->
                <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                  <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="headingOne">
                      <h4 class="panel-title">
                        <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                          Collapsible Group Item #1
                        </a>
                      </h4>
                    </div>
                    <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
                      <div class="panel-body">
                        Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
                      </div>
                    </div>
                  </div>
                  <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="headingTwo">
                      <h4 class="panel-title">
                        <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                          Collapsible Group Item #2
                        </a>
                      </h4>
                    </div>
                    <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
                      <div class="panel-body">
                        Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
                      </div>
                    </div>
                  </div>
                  <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="headingThree">
                      <h4 class="panel-title">
                        <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                          Collapsible Group Item #3
                        </a>
                      </h4>
                    </div>
                    <div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
                      <div class="panel-body">
                        Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
                      </div>
                    </div>
                  </div>
                  <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="headingFour">
                      <h4 class="panel-title">
                        <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
                          Collapsible Group Item #4
                        </a>
                      </h4>
                    </div>
                    <div id="collapseFour" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingFour">
                      <div class="panel-body">
                        Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
                      </div>
                    </div>
                  </div>
                  <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="headingFive">
                      <h4 class="panel-title">
                        <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseFive" aria-expanded="false" aria-controls="collapseFive">
                          Collapsible Group Item #5
                        </a>
                      </h4>
                    </div>
                    <div id="collapseFive" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingFive">
                      <div class="panel-body">
                        Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
                      </div>
                    </div>
                  </div>
                  <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="headingSix">
                      <h4 class="panel-title">
                        <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseSix" aria-expanded="false" aria-controls="collapseSix">
                          Collapsible Group Item #6
                        </a>
                      </h4>
                    </div>
                    <div id="collapseSix" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingSix">
                      <div class="panel-body">
                        Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
                      </div>
                    </div>
                  </div>
                </div>
                <!-- END ACCORDION TASK -->

              </div>

            </div>
            <div class="row placeholders custom-desc">
              <div class="col-md-12 col-xs-12 col-sm-12 placeholder">
                <button class="btn btn-primary btn-material-blue-700" data-toggle="modal" data-target="#simple-dialog">Aggiungi Task</button>
                <button class="btn btn-primary btn-material-blue-700" data-toggle="modal" data-target="#simple-dialog">Modifica Task</button>
                <button class="btn btn-primary btn-material-blue-700" data-toggle="modal" data-target="#simple-dialog">Elimina Task</button>
                <button class="btn btn-primary btn-material-blue-700" data-toggle="modal" data-target="#simple-dialog">Completata</button>
                <button class="btn btn-primary btn-material-blue-700" data-toggle="modal" data-target="#simple-dialog">Ripristina</button>
              </div>
            </div>
          </div>

        </div>
      </div>


    <!-- <p class="bs-component">
        <a href="javascript:void(0)" class="btn btn-default">Default</a>
        <a href="javascript:void(0)" class="btn btn-primary">Primary</a>
        <a href="javascript:void(0)" class="btn btn-success">Success</a>
        <a href="javascript:void(0)" class="btn btn-info">Info</a>
        <a href="javascript:void(0)" class="btn btn-warning">Warning</a>
        <a href="javascript:void(0)" class="btn btn-danger">Danger</a>
        <a href="javascript:void(0)" class="btn btn-link">Link</a>
      </p> -->
    </div>
    <!-- Your site ends -->

    <script src="http://code.jquery.com/jquery-1.10.2.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
    <script src="js/bootstrap-datepicker.js" charset="UTF-8"></script>
    <script type="text/javascript" src="js/locales/bootstrap-datepicker.it.js" charset="UTF-8"></script>
    <script type="text/javascript">
      $(function() {
        $('.custom-datepicker').datepicker({
          format: 'dd/mm/yyyy',
          language: 'it'
        });
      });
    </script>
    <script src="js/ripples.min.js"></script>
    <script src="js/material.min.js"></script>
    <script>
      $(document).ready(function() {
        $.material.init();
      });
    </script>
  </body>

  </html>
