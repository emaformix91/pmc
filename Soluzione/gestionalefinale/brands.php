<?php
include('./include/config.php');
include( './include/variabili.php' );
if(! empty($brands_include_list)){
  foreach ($brands_include_list as $to_include) {
  include ($to_include);
  } 
}


    $user_controller = new usercontroller();
    $brands_controller = new brandscontroller();

    $valid_actions = array( 'index','detail','create', 'edit', 'delete' );
    $action = isset( $_GET[ 'action' ] ) && in_array( $_GET[ 'action' ], $valid_actions ) ? $_GET[ 'action' ] : 'index';

    // exec requested action
    $brands_controller->$action();
    include( './views/footer.php' );