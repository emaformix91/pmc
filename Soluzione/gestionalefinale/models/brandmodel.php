<?php 
class brandmodel extends SuperModels{
	
	public function getbrand($cliente_id){
    $res = parent::ReturnArray($this-> select(
      array( '*' ),
      array('brand'),
      'clienti_id = '.$this->_DbConnector->escape($cliente_id)));
    return $res;
	}
  public function getDipendenti(){
    $res = parent::ReturnArray($this-> select(
      array( '*' ),
      array('dipendenti')));
    return $res;
  }
  public function getMail( $id ){
    $res = parent::ReturnArray($this-> select(
      array( 'email' ),
      array('dipendenti'),
      'id = '.$id));
    return $res;
  }
  public function getAdminEmail(){
     $res = parent::ReturnArray($this-> select(
      array( 'email' ),
      array('admin')));
    return $res;
  }
   public function addbrand($clienti_id, $azienda, $settore, $email, $telefono, $descrizione){
    $this-> insert('brand',
      array( 'clienti_id', 'azienda', 'settore','email', 'telefono', 'descrizione' ),
      array("'".$this->_DbConnector->escape($clienti_id)."'","'".$this->_DbConnector->escape($azienda)."'", "'".$this->_DbConnector->escape($settore)."'", "'".$this->_DbConnector->escape($email)."'", "'".$this->_DbConnector->escape($telefono)."'", "'".$this->_DbConnector->escape($descrizione)."'")
      );
    return;
  }

	
  public function addserv($brand_id, $servizi_id, $prezzo, $stato_pag, $data_inizio, $stato_serv){
    $this-> insert('servizi_brand',
      array( 'brand_id, servizi_id, prezzo, stato_pag, data_inizio, stato_serv' ),
      array("'".$this->_DbConnector->escape($brand_id)."'","'".$this->_DbConnector->escape($servizi_id)."'","'".$this->_DbConnector->escape($prezzo)."'","'".$this->_DbConnector->escape($stato_pag)."'","'".$this->_DbConnector->escape($data_inizio)."'","'".$this->_DbConnector->escape($stato_serv)."'")
      );
    return;
  }
  public function addtask($brand_id, $destinatario, $titolo, $descrizione){
    $this-> insert('task',
      array( 'brand_id, destinatario, titolo, descrizione' ),
      array("'".$this->_DbConnector->escape($brand_id)."'","'".$this->_DbConnector->escape($destinatario)."'","'".$this->_DbConnector->escape($titolo)."'","'".$this->_DbConnector->escape($descrizione)."'")
      );
    return;
  }
  public function AddRelazione( $dip_id,$task_id ){
    $this-> insert( 'relazione_dip_task',
    array( 'id_dip,id_task' ),
    array("'".$this->_DbConnector->escape($dip_id)."'","'".$this->_DbConnector->escape($task_id)."'"));
    return;
  }
	public function getbrand_dati($brand_id){
     $res = parent::ReturnArray($this-> select(
      array( '*' ),
      array('brand'),
      'id = '.$this->_DbConnector->escape($brand_id)));
    return $res;
    }
    public function getservizi(){
      $res = parent::ReturnArray($this-> select(
      array( 'id, name' ),
      array('servizi')));
    return $res;
    }
    public function getstatoserv($brand_id){
      $res = parent::ReturnArray($this-> select(
      array( '*' ),
      array('servizi_brand'),
      'brand_id = '.$this->_DbConnector->escape($brand_id).' AND stato_serv = "attivo"'));
    return $res;
    }
    public function getstatoservpass($brand_id){
      $res = $this->_DbConnector->select(array('*'),
        array('servizi_brand'),
        'brand_id = ' .$this->_DbConnector->escape($brand_id).' AND stato_serv = "passato"'
        );
      $servizi_stato = array();
      if( $res!==false){
        while( $res_assoc = $this->_DbConnector->fetch_assoc($res)){
          $servizi_stato[] = $res_assoc;
        }
        return $servizi_stato;
      }
    }
    public function GetServiziPassati($brand_id){
      return $res = $this-> _DbConnector->select(array('*'),
        array('servizi_brand'),
        'brand_id = '.$this-> _DbConnector->escape($brand_id).' AND stato_serv = "passato"'
        );
    } 
    public function gettask($brand_id){
      $res = $this->_DbConnector->select(array('*'),
        array('task'),
        'brand_id = '.$this->_DbConnector->escape($brand_id)
        );
      $tasks = array();
      if( $res!==false){
        while( $res_assoc = $this->_DbConnector->fetch_assoc($res)){
          $tasks[] = $res_assoc;
        }
        return $tasks;
      }
    }
    public function getTasks($brand_id){
      $res = $this-> _DbConnector->select_inner(
      array('task.id,task.titolo,task.descrizione,task.stato'), 
        array('relazione_dip_task'),
        'dipendenti',
        array('dipendenti.id' => 'relazione_dip_task.id_dip'),
        'task',
        array('task.id' => 'relazione_dip_task.id_task'),
        'brand',
        array('task.brand_id' => 'brand.id'),
        '',
        array('' => ''),
        '',
        array('' => ''),
        'task.brand_id = '.$this->_DbConnector->escape($brand_id),
        '',
        '');
      $tasks = array();
      if( $res!==false){
        while( $res_assoc = $this->_DbConnector->fetch_assoc($res)){
          $tasks[] = $res_assoc;
        }
      }
      return $tasks;
    }
    public function getDipendenteAssign(){
      $res = parent::ReturnArray($this-> select_inner(
      array( '*' ),
      array('relazione_dip_task'),
      'task',
      array('relazione_dip_task.id_task' => 'task.id'),
      'dipendenti',
      array('relazione_dip_task.id_dip' => 'dipendenti.id'),
      '',
      array(''=>''),
      '',
      array(''=>''),
      '',
      array(''=>''),
      '1',
      '',
      ''));
    return $res;
    }
    public function updatebrands($brands){
      $id = 0;
      $azienda = '';
      $settore = '';
      $email = '';

      $query = "UPDATE brand SET azienda = ?, settore = ?,  email = ? WHERE id = ?";

      $param_type = 'sssi';

      $this->_DbConnector->prepareStatement($query);
      $this->_DbConnector->bindParamsToStatement($param_type, array(&$azienda, &$settore, &$email, &$id));
      foreach ($brands as $key => $brand) {
        extract($brand);
        $id = $key;
        $this->_DbConnector->execStatement();
      }
      $this->_DbConnector->closeStatement();
    }
     public function updateservizi_stato($servizi_stato){
     $id = 0;
      $prezzo = '';
      $stato_pag = '';
      $data_inizio = '';
      $data_fine = '';
      $stato_serv = '';

      $query = "UPDATE servizi_brand SET prezzo = ?, stato_pag = ?,  data_inizio = ?, data_fine = ?, stato_serv = ? WHERE id = ?";

      $param_type = 'sssssi';

      $this->_DbConnector->prepareStatement($query);
      $this->_DbConnector->bindParamsToStatement($param_type, array(&$prezzo, &$stato_pag, &$data_inizio, &$data_fine, &$stato_serv, &$id));
      foreach ($servizi_stato as $key => $servizio_stato) {
        extract($servizio_stato);
        $id = $key;
        $this->_DbConnector->execStatement();
      }
      $this->_DbConnector->closeStatement();
    }
    public function updatetasks($tasks){
     $id = 0;
      $destinatario = '';
      $titolo = '';
      $descrizione = '';
      $stato = '';
      $data_inizio = '';
      $data_scadenza = '';

      $query = "UPDATE task SET destinatario = ?, titolo = ?,  descrizione = ?, stato = ?, data_inizio = ?, data_scadenza = ? WHERE id = ?";

      $param_type = 'ssssssi';

      $this->_DbConnector->prepareStatement($query);
      $this->_DbConnector->bindParamsToStatement($param_type, array(&$destinatario, &$titolo, &$descrizione, &$stato, &$data_inizio, &$data_scadenza, &$id));
      foreach ($tasks as $key => $task) {
        extract($task);
        $id = $key;
        $this->_DbConnector->execStatement();
      }
      $this->_DbConnector->closeStatement();
    }
    public function updatebrands_description( $telefono,$email,$descrizione, $id ){
      $res = $this->_DbConnector->update('brand',
        array('telefono' => "'".$this->_DbConnector->escape($telefono)."'",'email' => "'".$this->_DbConnector->escape($email)."'",'descrizione' => "'".$this->_DbConnector->escape($descrizione)."'"),
        'id = '.$this->_DbConnector->escape($id));
      $brand = array();
      if($res!==false){
        while($res_assoc = $this->_DbConnector->fetch_assoc($res)){
          $brand[] = $res_assoc;
        }
        return $brand;
      }
    }
    public function deletebrand( $id ){
      return $this->delete( ' brand',$id );
    }
    public function deletedati_brand( $id ){
      return $this->delete( ' dati_brand',$id );
    }
    public function deleteservizio_stato( $id ){
      return $this->delete( ' servizi_brand',$id );
    }
    public function deletetask($id){
      $res = $this->_DbConnector->delete(' task ',
        'id = '.$this->_DbConnector->escape($id)
        );
      return $res;
    }
    public function RipristinaTask($id){
        $res = parent::ReturnArray($this-> update( 'task',
                        array('stato' => '"ripristinata"'),
                        'id ='.$this-> _DbConnector->escape($id)
                        ));
        return $res; 
    }
}