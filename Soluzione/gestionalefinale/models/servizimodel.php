<?php
Class servizimodel extends SuperModels {

	public function getServizi(){
		$res = parent::ReturnArray(
			$this-> select(
			array( '*' ),
			array( 'servizi' )
			)
		); 
		return $res;
	}

	public function getDipendente( $id ){
		$res = parent::ReturnArray(
			$this-> select(
			array( '*' ),
			array( 'dipendenti' ),
			'id ='.$this-> _DbConnector->escape( $id )
			)
		); 
		return $res;
	}

	public function getTaskDip( $id ){
	$res = parent::ReturnArray(
		$this-> select_inner(
			array('*'),
			array('relazione_dip_task'),
			'task',
			array('relazione_dip_task.id_task' => 'task.id'),
			'',
        	array('' => ''),
        	'',
        	array('' => ''),
        	'',
        	array('' => ''),
        	'',
        	array('' => ''),
        	'relazione_dip_task.id_dip = '.$this->_DbConnector->escape($id),
        	'',
        	''));
        	return $res;	
	}

	public function AddServizio( $nome ){
		$this-> insert('servizi',
			array( 'name' ),
			array("'".$this-> _DbConnector->escape($nome)."'")
			);
		return;
	}

	public function UpdateServizi( $servizi ){
		$id = 0;
      $name = ''; 

      $query = "UPDATE servizi SET name = ? WHERE id = ?";

      $param_type = 'si';

      $this-> _DbConnector->prepareStatement($query);
      $this-> _DbConnector->bindParamsToStatement($param_type, array(&$name, &$id));
      foreach ($servizi as $key => $servizo) {
        extract($servizo);
        $id = $key;
        $this-> _DbConnector->execStatement();
      }
      $this-> _DbConnector->closeStatement();
	}
	public function deleteServizio( $id ){
    	return $this->delete( ' servizi',$id );
    }
   
	public function serch_servizio( $name ){
    $match1 = array('name');
    $tabella = array('servizi');
    $value = array( $name );
    $res = SuperModels::ReturnArray( $this->serch_withsingoldate( $match1, $value, $tabella));
    return $res;
  }

}