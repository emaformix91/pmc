<?php
class brandsmodel extends SuperModels {
	

	

	public function getBrands(){
		$res = parent::ReturnArray($this-> select(
			array('id' , 'clienti_id' , 'azienda' , 'settore' , 'email'),
			array('brand'))); 
		return $res;
	}
	public function getCliente(){
		$res = parent::ReturnArray($this-> select(
			array(  'id','name','cognome' ),
			array('clienti')));
		return $res;
	}
	public function AddBrand( $clienti_id, $azienda, $settore, $email , $telefono, $descrizione){
	  $this-> insert( 'brand',
	 	array('clienti_id', 'azienda', 'settore','email', 'telefono', 'descrizione'),
	 	array("'".$this->_DbConnector->escape($clienti_id)."'","'".$this->_DbConnector->escape($azienda)."'", "'".$this->_DbConnector->escape($settore)."'", "'".$this->_DbConnector->escape($email)."'","'".$this->_DbConnector->escape($telefono)."'", "'".$this->_DbConnector->escape($descrizione)."'"));
	  return;
	}
	public function UpdateBrands($brands){
      $id = 0;
      $azienda = '';
      $settore = '';
      $email = '';

      $query = "UPDATE brand SET azienda = ?, settore = ?,  email = ? WHERE id = ?";

      $param_type = 'sssi';

      $this-> _DbConnector->prepareStatement($query);
      $this-> _DbConnector->bindParamsToStatement($param_type, array(&$azienda, &$settore, &$email, &$id));
      foreach ($brands as $key => $brand) {
        extract($brand);
        $id = $key;
        $this-> _DbConnector->execStatement();
      }
      $this-> _DbConnector->closeStatement();
    }
    public function DeleteBrand( $id ){
    	return $this->delete( ' brand',$id );
    }
    public function serch_brand( $azienda,$settore,$email ){
    $match1 = 'azienda';
    $match2 = 'settore';
    $match3 = 'email';
    $tabella = array('brand');
    $res = SuperModels::ReturnArray( $this->serch( $match1, $match2, $match3, $azienda, $settore, $email, $tabella));
    return $res;
  }
}