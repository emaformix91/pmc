<?php
Class impostazionimodel extends SuperModels {

	public function getAdmin(){
		$res = parent::ReturnArray(
			$this-> select(
			array( '*' ),
			array( 'admin' )
			)
		); 
		return $res;
	}
	public function Updateadmin( $nome, $cognome, $email, $azienda,  $username, $pass,$id ){
		$res = parent::ReturnArray($this-> update(  'admin',
				array('name'=>"'".$this-> _DbConnector->escape($nome)."'",
						'cognome'=>"'".$this-> _DbConnector->escape($cognome)."'",
						'azienda'=>"'".$this-> _DbConnector->escape($azienda)."'",
						'email'=>"'".$this-> _DbConnector->escape($email)."'",
						'username'=>"'".$this-> _DbConnector->escape($username)."'",
						'password'=>"'".md5($this-> _DbConnector->escape($pass))."'"),
						'id ='.$this-> _DbConnector->escape($id)
				));
		return $res;
	}
	public function Updateadmin_whithsamepass( $nome, $cognome, $email, $azienda,  $username, $id ){
		$res = parent::ReturnArray($this-> update(  'admin',
				array('name'=>"'".$this-> _DbConnector->escape($nome)."'",
						'cognome'=>"'".$this-> _DbConnector->escape($cognome)."'",
						'azienda'=>"'".$this-> _DbConnector->escape($azienda)."'",
						'email'=>"'".$this-> _DbConnector->escape($email)."'",
						'username'=>"'".$this-> _DbConnector->escape($username)."'"),
						'id ='.$this-> _DbConnector->escape($id)
				));
		return $res;
	}

}