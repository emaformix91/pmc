<?php
Class dipendentimodel extends SuperModels {

	public function getDipendenti(){
		$res = parent::ReturnArray(
			$this-> select(
			array( '*' ),
			array( 'dipendenti' )
			)
		); 
		return $res;
	}

	public function getDipendente( $id ){
		$res = parent::ReturnArray(
			$this-> select(
			array( '*' ),
			array( 'dipendenti' ),
			'id ='.$this-> _DbConnector->escape( $id )
			)
		); 
		return $res;
	}

	public function getTaskDip( $id ){
	$res = parent::ReturnArray(
		$this-> select_inner(
			array('*'),
			array('relazione_dip_task'),
			'task',
			array('relazione_dip_task.id_task' => 'task.id'),
			'',
        	array('' => ''),
        	'',
        	array('' => ''),
        	'',
        	array('' => ''),
        	'',
        	array('' => ''),
        	'relazione_dip_task.id_dip = '.$this->_DbConnector->escape($id),
        	'',
        	''));
        	return $res;	
	}

	public function AddDipendente( $nome,$cognome,$settore,$ruolo,$email,$telefono,$codfiscale,$pass ){
		$this-> insert('dipendenti',
			array( 'name , cognome , email, telefono , codfiscale , settore , ruolo, password' ),
			array("'".$this-> _DbConnector->escape($nome)."'","'".$this-> _DbConnector->escape($cognome)."'","'".$this-> _DbConnector->escape($email)."'","'".$this-> _DbConnector->escape($telefono)."'","'".$this-> _DbConnector->escape($codfiscale)."'","'".$this-> _DbConnector->escape($settore)."'","'".$this-> _DbConnector->escape($ruolo)."'","'".md5($this-> _DbConnector->escape($pass))."'")
			);
		return;
	}

	public function UpdateDipendenti( $dipendenti ){
		$id = 0;
      $name = ''; 
      $cognome = ''; 
      $email = ''; 
      $settore = '';
      $ruolo = '';

      $query = "UPDATE dipendenti SET name = ?, cognome = ?, email = ? ,  settore = ? , ruolo = ? WHERE id = ?";

      $param_type = 'sssssi';

      $this-> _DbConnector->prepareStatement($query);
      $this-> _DbConnector->bindParamsToStatement($param_type, array(&$name, &$cognome, &$email, &$settore, &$ruolo, &$id));
      foreach ($dipendenti as $key => $dipendente) {
        extract($dipendente);
        $id = $key;
        $this-> _DbConnector->execStatement();
      }
      $this-> _DbConnector->closeStatement();
	}
	public function UpdateDipendente( $telefono, $email, $codfiscale, $settore, $ruolo, $pass,$id ){
		$res = parent::ReturnArray($this-> update(  'dipendenti',
				array('telefono'=>"'".$this-> _DbConnector->escape($telefono)."'",
						'email'=>"'".$this-> _DbConnector->escape($email)."'",
						'codfiscale'=>"'".$this-> _DbConnector->escape($codfiscale)."'",
						'settore'=>"'".$this-> _DbConnector->escape($settore)."'",
						'ruolo'=>"'".$this-> _DbConnector->escape($ruolo)."'",
						'password'=>"'".md5($this-> _DbConnector->escape($pass))."'"),
						'id ='.$this-> _DbConnector->escape($id)
				));
		return $res;
	}
	public function UpdateDipendente_whithsamepass( $telefono, $email, $codfiscale, $settore, $ruolo,$id ){
		$res = parent::ReturnArray($this-> update(  'dipendenti',
				array('telefono'=>"'".$this-> _DbConnector->escape($telefono)."'",
						'email'=>"'".$this-> _DbConnector->escape($email)."'",
						'codfiscale'=>"'".$this-> _DbConnector->escape($codfiscale)."'",
						'settore'=>"'".$this-> _DbConnector->escape($settore)."'",
						'ruolo'=>"'".$this-> _DbConnector->escape($ruolo)."'"),
						'id ='.$this-> _DbConnector->escape($id)
				));
		return $res;
	}
	public function deleteDipendente( $id ){
    	return $this->delete( ' dipendenti',$id );
    }
    public function TaskCompletata( $id ){
		$res = parent::ReturnArray($this-> update(  'task',
				array('stato'=> '"completata"'),
						'id ='.$this-> _DbConnector->escape($id)
					));
		return $res;
	}
	public function RipristinaTask($id){
        $res = parent::ReturnArray($this-> update( 'task',
                        array('stato' => '"ripristinata"'),
                        'id ='.$this-> _DbConnector->escape($id)
                        ));
        return $res; 
    }
    public function serch_dipendente( $nome,$cognome,$settore ){
    $match1 = 'name';
    $match2 = 'cognome';
    $match3 = 'settore';
    $tabella = array('dipendenti');
    $res = SuperModels::ReturnArray( $this->serch( $match1, $match2, $match3, $nome, $cognome, $settore, $tabella));
    return $res;
  }

}