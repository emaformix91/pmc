﻿(function () {
    'use strict';

    angular
        .module('app')
        .controller('LoginController', LoginController);

    LoginController.$inject = ['$location', 'AuthenticationService', 'FlashService'];
    function LoginController($location, AuthenticationService, FlashService) {
        var vm = this;

        vm.login = login;

        (function initController() {
            // reset login status
            AuthenticationService.ClearCredentials();
        })();

        function login() {
            vm.dataLoading = true;
            AuthenticationService.Login(vm.username, vm.password).then(
                  function (response) {
                    if (response.data.success) {
                        console.log(response)
                        vm.dataLoading = false;
                       AuthenticationService.SetCredentials(vm.username, vm.password);
                       $location.path('/dashboard');
                    } else {
                        console.log('RESPONSE ERROR:',response)
                        FlashService.Error(response.data.message);
                        vm.dataLoading = false;
                        }
                  }
                );
        };
    }

})();
