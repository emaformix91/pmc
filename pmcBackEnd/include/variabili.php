<?php




$link_esci = array(

    "link" => '?action=logout',
    "title" => 'esci',
    "name" => 'Esci',

);



//LEFT MENU

$link_clienti = array(

    "link" => '/'.ROOT_NAME.'/clienti.php',
    "title" => 'clienti',
    "name" => 'Clienti',

);



// 0 = center , 1 = left , 2 = right
$setting_menu_alignent = '0';

$class_menu_alignment =  "right";


switch ($setting_menu_alignent) {
    case 0 :
        $class_menu_alignment = "center";
        break;
    case 1 :
        $class_menu_alignment = "left";
        break;
    case 2 :
        $class_menu_alignment = "right";
        break;
    default:
        $class_menu_alignment = "right";
}

/* FINE HEADER */

/* FOOTER */

$show_footer = true;

$js_list = array( 'jquery-1.10.2.min' , 'bootstrap.min' , 'bootstrap-datepicker' , 'locales/bootstrap-datepicker.it' , 'ripples.min' , 'material.min') ;

$footer_text = 'Project Monitoring And Control';

/* END FOOTER */


/* GESTIONE UTENTI */


$tipologie[] = 'Admin';
$tipologie[] = 'User';

/* GESTIONE INCLUDE */
$header_include_list = array('./lib/DbConnector.php', './models/usermodel.php' );

$impostazioni_include_list = array('./lib/DbConnector.php', './models/usermodel.php','./controllers/usercontroller.php','./controllers/SuperControllers.class.php','./controllers/impostazionicontroller.php','./models/SuperModels.class.php' ,'./models/impostazionimodel.php', './head/head.php','./views/header.php');

$client_include_list = array('./lib/DbConnector.php','./controllers/usercontroller.php','./controllers/SuperControllers.php','./controllers/clientecontroller.php','./models/SuperModels.class.php' , './models/clientemodel.php', './models/usermodel.php', './controllers/brandcontroller.php','./models/brandmodel.php');

$dashboard_include_list = array('./lib/DbConnector.php','./controllers/SuperControllers.php','./controllers/dashboardcontroller.php');

$login_include_list = array('./lib/DbConnector.php' ,'./controllers/SuperControllers.php', './controllers/usercontroller.php' ,'./models/SuperModels.class.php', './models/usermodel.php');
$utente_include_list = array('./lib/DbConnector.php' ,'./controllers/SuperControllers.php', './controllers/usercontroller.php' ,'./models/SuperModels.class.php', './models/usermodel.php');

$risorsa_include_list = array('./lib/DbConnector.php' ,'./controllers/SuperControllers.php','./controllers/risorsecontroller.php','./models/SuperModels.class.php','./models/risorsamodel.php');


/* STATO SERVIZI INPUT OPTION */


$stato_servizi[] = 'Saldato Acconto';
$stato_servizi[] = 'Saldato Totale';
$stato_servizi[] = 'Non Saldato';