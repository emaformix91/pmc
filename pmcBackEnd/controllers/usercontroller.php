<?php
class usercontroller extends SuperControllers{
	public $username ='';
	private $_model = NULL;
	private $logged = false;

	public function __construct(){
		//session_start();
		$this->_model = new usermodel();
		$this->_startSecureSession();
	}

	public function login(){

		if($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_GET['action']) && $_GET['action'] == 'login'){
			$data = json_decode(file_get_contents("php://input"),true);
			$username = isset($data['username'])?$data['username']:false;
			$password = isset($data['password'])?$data['password']:false;

			if($username != false && $password != false && $this->checklogin($username,$password)){
				$this->username = $username;
				$this->logged = true;

				$_SESSION['username'] = $username;
				$_SESSION['logged'] = true;
				$_SESSION['message'] = 'Benvenuto';

			$this->responseSuccess($this->_model->getuserDipendente_id($username,$password));
			}else{
                $_SESSION['message'] = 'Login non riuscito piccolo problemino';
            	// header('HTTP/1.1 400');
        		$this->responseError('','Login non riuscito piccolo problemino');

			}
		}
		// elseif(isset($_SESSION['username'])&& isset($_SESSION['logged'])&& $_SESSION['logged']){
		// 	$this->username = $_SESSION['username'];
		// 	$this->logged = true;
		// }
		
		
	}

	public function logout(){

		if(isset($_GET['action']) && $_GET['action']=='logout'){
			unset($_SESSION['username']);
		unset($_SESSION['logged']);
		$_SESSION['message'] = 'Buona Giornata';
		}
		$this->responseSuccess('logout');
	}

	public function checklogin($username,$password){
		$admin = $this->_model->getuser_pass();
		foreach ($admin as $value) {
		  return($value['username'] == $username && $value['password'] == md5($password));
		}
		
	}

	public function getUtenzaById(){

		if($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_GET['action']) && $_GET['action'] == 'getUtenzaById'){
			$data = json_decode(file_get_contents("php://input"),true);
			$id = isset($data['id'])?$data['id']:false;

			$this->responseSuccess($this->_model-> getUtenzaById($id));
			
		}
	}

	public function addUtenza(){

		if($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_GET['action']) && $_GET['action'] == 'addUtenza'){
			$data = json_decode(file_get_contents("php://input"),true);
			$username = isset($data['nome'])?$data['nome']:false;
			$password = isset($data['cognome'])?$data['cognome']:false;
			$risorsa = isset($data['risorsa'])?$data['risorsa']:false;

			$this->responseSuccess($this->_model-> addUtenza(uniqid(),$username,md5($password),$risorsa));
			
		}
	}

	public function updateUtenza(){

		if($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_GET['action']) && $_GET['action'] == 'updateUtenza'){
			$data = json_decode(file_get_contents("php://input"),true);
			$risorse = isset($data['risorse'])?$data['risorse']:false;

			$this->responseSuccess($this->_model->updateRisorse($risorse));
			
		}
	}

	public function islogged(){
		return $this->logged;
	}

	private function _startSecureSession(){
		ini_set('session.use_trans_sid', 0);
		ini_set('session.use_only_cookies', 1);

		$session_name ='sessionId';
		$cookie_defaults =session_get_cookie_params();
		session_set_cookie_params(
            0,
            $cookie_defaults['path'],
            $cookie_defaults['domain'],
            false,
            true
			);
			session_name($session_name);
			session_start();
			session_regenerate_id(true);	
	}
}