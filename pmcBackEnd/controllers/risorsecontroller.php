<?php
class risorsecontroller extends SuperControllers{
	private $_model = NULL;

	public function __construct(){
		$this->_model = new risorsamodel();
	}

	public function getRisorse(){

		if($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_GET['action']) && $_GET['action'] == 'getRisorse'){

			$this->responseSuccess($this->_model->getRisorsa());
			
		}
	}

	public function getRisorsaById(){

		if($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_GET['action']) && $_GET['action'] == 'getRisorsaById'){
			$data = json_decode(file_get_contents("php://input"),true);
			$id = isset($data['id'])?$data['id']:false;

			$this->responseSuccess($this->_model->getRisorsaById($id));
			
		}
	}

	public function addRisorsa(){

		if($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_GET['action']) && $_GET['action'] == 'addRisorsa'){
			$data = json_decode(file_get_contents("php://input"),true);
			$nome = isset($data['nome'])?$data['nome']:false;
			$cognome = isset($data['cognome'])?$data['cognome']:false;
			$email = isset($data['email'])?$data['email']:false;

			$this->responseSuccess($this->_model->addRisorsa($nome,$cognome,uniqid(),'',$email));
			
		}
	}

	public function updateRisorse(){

		if($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_GET['action']) && $_GET['action'] == 'updateRisorse'){
			$data = json_decode(file_get_contents("php://input"),true);
			$risorse = isset($data['risorse'])?$data['risorse']:false;

			$this->responseSuccess($this->_model->updateRisorse($risorse));
			
		}
	}

}