<?php 
abstract class SuperControllers{

	public abstract function __construct();

	public function check_leng_string($str,$min,$max){
      $leng = strlen($str);
      return $leng >= $min && $leng <= $max;
    }
    public function check_string($str){
      return preg_match("/^[a-zA-Z0-9_']+$/", $str);
    }
    public function check_mail($str){
      return preg_match("/^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/", $str);
    }
    public function check_stringOneSpace($str){
      return preg_match("/^[a-zA-Z0-9_' ]+$/", $str);
    }
    public function check_codfiscale($str){
      $leng = strlen($str);
      return $leng == 16;
    }
    public function check_telefono($str){
      return preg_match("/^[0-9+ ]{2,6}+[0-9]{2,20}$/", $str);
    }
    public function check_prezzo($str){
      return preg_match("/^[0-9+ ]{2,60}$/", $str);
    }

    public function responseSuccess($data){
      header('Content-Type: application/json; charset=UTF-8');
      $response = new \stdClass();
      $response->success = True;
      $response->data = $data;
      echo json_encode($response);
    }

    public function responseError($data,$message){
      header('Content-Type: application/json; charset=UTF-8');
      $response = new \stdClass();
      $response->success = False;
      $response->message = $message;
      $response->data = $data;
      echo json_encode($response);
    }
}