<?php 
require_once '../includes/db.php'; // The mysql database connection script

if(isset($_GET['clients'])){
$query="select * from clienti";
$result = $mysqli->query($query) or die($mysqli->error.__LINE__);

$arr = array();
if($result->num_rows > 0) {
	while($row = $result->fetch_assoc()) {
		$arr[] = $row;	
	}
}
# JSON-encode the response
echo $json_response = json_encode($arr);
}

if(isset($_GET['getClientName'])){
	//Receive the RAW post data.
	$content = trim(file_get_contents("php://input"));
	 
	//Attempt to decode the incoming RAW post data from JSON.
	$decoded = json_decode($content, true);
	 
	//If json_decode failed, the JSON is invalid.
	if(!is_array($decoded)){
	    throw new Exception('Received content contained invalid JSON!');
	}

	$decoded['nome'] = "Oronzo";

	echo $json_response = json_encode($decoded);
}


?>