<?php 
class init{
	public $to_include = array();

	public function __construct($array){
		$this-> to_include = $array; 
	}
	public function getInclude(){
		if(! empty($this-> to_include)){
			foreach ($this-> to_include as $class) {
					 echo include($class);
			}
		}
	}
	
}