<?php

	class risorsamodel extends SuperModels{

		public function getRisorsa(){
		  $res = parent::ReturnArray($this-> select( array('*'), array('risorsa')));
      return $res;
    }

    public function getRisorsaById($id){
      $res = $this-> select( 
        array('*'), 
        array('risorsa'), 
        'id='.$this->getDbConnector()->escape( $id ));

      $risorsa = array();
        if( $res!==false){
          while( $res_assoc = $this->getDbConnector()->fetch_assoc($res)){
            $risorsa[] = $res_assoc;
          }
        }
        return $risorsa;
    }

    public function addRisorsa($name, $cognome, $cod, $telefono, $email){
     return  $this-> insert('risorsa',
        array( 'nome', 'cognome', 'cod_risorsa' ),
        array("'".$this->getDbConnector()-> escape($name)."'", "'".$this->getDbConnector()-> escape($cognome)."'", "'".$this->getDbConnector()-> escape($cod)."'")
        );
    }

    public function updateRisorse($risorse){
      $id = 0;
      $name = '';
      $cognome = '';
      $cod_risorsa = '';
      $query = "UPDATE risorsa SET nome = ?, cognome = ? WHERE cod_risorsa = ?";

      $param_type = 'ssi';
      $this-> getDbConnector()-> prepareStatement($query);
      $this-> getDbConnector()-> bindParamsToStatement($param_type, array(&$nome, &$cognome, &$cod_risorsa));
      foreach ($risorse as $key => $risorsa) {
        extract($risorsa);
        $this-> getDbConnector()-> execStatement();
      }
      $this-> getDbConnector()-> closeStatement();
    }

	}