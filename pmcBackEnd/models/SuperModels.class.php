<?php
class SuperModels {
	protected $_DbConnector = NULL;

	public function __construct(){
		$this-> _DbConnector = new DbConnector();
	}

	public function select( $fields,$table,$conditions = 1 ){
		$res = $this-> _DbConnector->select( $fields,$table,$conditions );
      return $res;
      
	}
	public function select_inner( $fields, $tabella, $tabella2=false, $relations2=false, $tabella3=false, $relations3=false, $tabella4=false, $relations4=false, $tabella5=false, $relations5=false, $tabella6=false, $relations6=false, $condizioni ='1', $condizioni2=false, $condizioni3=false ){
		$res = $this-> _DbConnector->select_inner( $fields, $tabella, $tabella2, $relations2, $tabella3, $relations3, $tabella4, $relations4, $tabella5, $relations5, $tabella6, $relations6, $condizioni, $condizioni2, $condizioni3 );
      return $res;
	}
	public function insert( $table,$fields,$values ){
		$res = $this-> _DbConnector->insert( $table, $fields, $values);
        return (!$res) ? $res:$this-> _DbConnector->LastId();
	}
	public function update( $table,$fields,$conditions ){
		$res = $this-> _DbConnector->update( $table,$fields,$conditions );
	}
	public function delete( $table,$conditions ){
		$res = $this-> _DbConnector->delete( $table,'id ='.$this->_DbConnector->escape($conditions) );
      return $res;
	}
	public function search( $match1, $match2, $match3, $against1, $against2, $against3, $tabella ){
		$match = array( $match1, $match2, $match3 );
		$against = array( $this-> _DbConnector->escape($against1), $this-> _DbConnector->escape($against2), $this-> _DbConnector->escape($against3) );
		$res = $this-> _DbConnector->serch( $match, $against, $tabella );
      return $res;
	}
	public function search_withsingoldate( $match1, $against1, $tabella ){
		$res = $this-> _DbConnector->serch( $match1, $against1, $tabella );
      return $res;
	}
	public function ReturnArray( $res ){
		$variabile = array();
		if( $res!==false){
        while( $res_assoc = $this->_DbConnector->fetch_assoc($res)){
          $variabile[] = $res_assoc;
        }
      }
      return $variabile;
	}

	public function getDbConnector(){
		return $this-> _DbConnector;
	}
}