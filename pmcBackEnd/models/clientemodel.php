<?php
  class clientemodel extends SuperModels {
  	public function getadmin(){
    $res = parent::ReturnArray(
      $this-> select(
      array( 'id, name, cognome, azienda, email' ),
      array( 'admin' )
      )
    ); 
    return $res;
  }
    public function getnumserv(){
    $res = parent::ReturnArray(
      $this-> select(
      array( 'id, numero' ),
      array( 'numero_servizi' )
      )
    ); 
    return $res;
  }
   public function getservizi(){
    $res = parent::ReturnArray(
      $this-> select(
      array( 'id, name' ),
      array( 'servizi' )
      )
    ); 
    return $res;
  } 
   public function getclienti(){
    $res = parent::ReturnArray(
      $this-> select(
      array( 'id,  name, cognome, indirizzo, iva, email' ),
      array( 'clienti' )
      )
    ); 
    return $res;
  } 
  public function getcliente( $id ){
    $res = parent::ReturnArray(
      $this-> select(
      array( '*' ),
      array( 'clienti' ),
      'id ='.$this-> _DbConnector->escape( $id )
      )
    ); 
    return $res;
  }
  public function getbrand_servizi( $id ){
  $res = parent::ReturnArray(
    $this-> select_inner(
      array('brand.clienti_id, brand.azienda, brand.email, servizi_brand.stato_serv, servizi_brand.brand_id'),
      array('brand'),
      'servizi_brand',
      array('brand.id' => 'servizi_brand.brand_id'),
      '',
          array('' => ''),
          '',
          array('' => ''),
          '',
          array('' => ''),
          '',
          array('' => ''),
          'brand.id = servizi_brand.brand_id',
          '',
          ''));
          return $res;  
  }
  public function addadmin( $name, $cognome, $azienda, $email, $username, $password ){
    $this-> insert('admin',
      array( 'name', 'cognome', 'azienda','email','username','password' ),
      array("'".$this->_DbConnector->escape($name)."'", "'".$this->_DbConnector->escape($cognome)."'", "'".$this->_DbConnector->escape($azienda)."'","'".$this->_DbConnector->escape($email)."'","'".$this->_DbConnector->escape($username)."'","'".md5($this->_DbConnector->escape($password))."'")
      );
    return;
  }
  public function addnumserv($num_serv){
    $this-> insert('numero_servizi',
      array( 'numero' ),
      array("'".$this->_DbConnector->escape($num_serv)."'")
      );
    return;
  }
  public function AddServizio( $nome ){
    $this-> insert('servizi',
      array( 'name' ),
      array("'".$this-> _DbConnector->escape($nome)."'")
      );
    return;
  }
    public function addmultiserv($names){
       $name = '';

       $query = "INSERT INTO servizi(name) VALUES (?)";
       
       $param_type = 's';

       $this->_DbConnector->prepareStatement($query);
       $this->_DbConnector->bindParamsToStatement($param_type, array(&$name));
       foreach ($names as $name) {
         extract($name);
         $this->_DbConnector->execStatement();
       }
       $this->_DbConnector->closeStatement();
    }
     public function addcliente($name, $cognome, $indirizzo, $iva, $telefono, $email){
    $this-> insert('clienti',
      array( 'name', 'cognome', 'indirizzo','iva','telefono','email' ),
      array("'".$this->_DbConnector->escape($name)."'", "'".$this->_DbConnector->escape($cognome)."'", "'".$this->_DbConnector->escape($indirizzo)."'", "'".$this->_DbConnector->escape($iva)."'", "'".$this->_DbConnector->escape($telefono)."'", "'".$this->_DbConnector->escape($email)."'")
      );
    return;
  }
    public function updateclienti($clienti){
      $id = 0;
      $name = '';
      $cognome = '';
      $email = '';

      $query = "UPDATE clienti SET nome = ?, cognome = ?,  email = ? WHERE id = ?";

      $param_type = 'sssi';

      $this->_DbConnector->prepareStatement($query);
      $this->_DbConnector->bindParamsToStatement($param_type, array(&$nome, &$cognome, &$email, &$id));
      foreach ($clienti as $key => $cliente) {
        extract($cliente);
        $id = $key;
        $this->_DbConnector->execStatement();
      }
      $this->_DbConnector->closeStatement();
    }
    public function updateclienti_dettail($indirizzo, $iva, $telefono, $email, $id){
      $res = $this->_DbConnector->update('clienti',
        array('indirizzo' =>"'".$this->_DbConnector->escape($indirizzo)."'", 'iva' =>"'".$this->_DbConnector->escape($iva)."'", 'telefono' =>"'".$this->_DbConnector->escape($telefono)."'", 'email' =>"'".$this->_DbConnector->escape($email)."'"),
        'id = '.$id);
      return;
    }
    public function delete_numserv($idnum_serv){
      return $this->delete( ' numero_servizi',$idnum_serv );
    }
    public function delete_serv($idserv){
      return $this->delete( ' servizi',$idserv );
    }
   public function deletecliente($id){
      return $this->delete( ' clienti',$id );
    }
    public function serch_servizio( $name ){
    $match1 = array('name');
    $tabella = array('servizi');
    $value = array( $name );
    $res = SuperModels::ReturnArray( $this->serch_withsingoldate( $match1, $value, $tabella));
    return $res;
  }
   
  public function serch_brand2( $azienda,$settore,$email ){
    $match1 = 'azienda';
    $match2 = 'settore';
    $match3 = 'email';
    $tabella = array('brand');
    $res = SuperModels::ReturnArray( $this->serch( $match1, $match2, $match3, $azienda, $settore, $email, $tabella));
    return $res;
  }
  public function serch_cliente( $nome,$cognome,$email ){
    $match1 = 'name';
    $match2 = 'cognome';
    $match3 = 'email';
    $tabella = array('clienti');
    $res = SuperModels::ReturnArray( $this->serch( $match1, $match2, $match3, $nome, $cognome, $email, $tabella));
    return $res;
  }
}