<?php

	class usermodel extends SuperModels{

		public function getuser_pass(){
		$res = $this->getDbConnector()->select( array('username,password'), 
        	array('utenza')
        	);
      	$admin = array();
      	if( $res!==false){
        	while( $res_assoc = $this->getDbConnector()->fetch_assoc($res)){
          	$admin[] = $res_assoc;
        	}
      	}
      	return $admin;
    	}
    public function getUtenzaById($id){
      $res = $this-> select( 
        array('*'), 
        array('utenza'), 
        'cod_utente = '."'".$this->getDbConnector()-> escape( $id )."'"
      );

      $utenza = array();
        if( $res!==false){
          while( $res_assoc = $this->getDbConnector()-> fetch_assoc($res)){
            $utenza[] = $res_assoc;
          }
        }

        return $utenza;
    }

    public function addUtenza($cod_risorsa,$username,$password,$risorsa){
     return  $this-> insert('risorsa',
        array( 'cod_utente','username', 'password', 'id_risorsa' ),
        array("'".$this->getDbConnector()-> escape($cod_risorsa)."'","'".$this->getDbConnector()-> escape($username)."'", "'".$this->getDbConnector()-> escape($password)."'", "'".$this->getDbConnector()-> escape($risorsa)."'")
        );
    }

    public function updateUtenza($utenze){
      $username = '';
      $password = '';
      $cod_utente = '';
      $query = "UPDATE risorsa SET username = ?, password = ? WHERE cod_utente = ?";

      $param_type = 'ssi';
      $this-> getDbConnector()-> prepareStatement($query);
      $this-> getDbConnector()-> bindParamsToStatement($param_type, array(&$nome, &$cognome, &$cod_utente));
      foreach ($utenze as $key => $utenza) {
        extract($utenza);
        $this-> getDbConnector()-> execStatement();
      }
      $this-> getDbConnector()-> closeStatement();
    }
	}