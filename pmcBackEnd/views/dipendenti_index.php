<!DOCTYPE html>
<html>
<?php if( $user_controller->isLogged() ): ?> 
<div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
	<div class="serch">
        <div class="col-md-3 col-sm-3">
            <h1 style="text-align:center;margin-top:6px;">Ricerca Rapida:</h1>
        </div>
        <div class="col-md-9  col-sm-9">
		<form action="" method="POST">
                <input type="text" name="nome" placeholder="Nome Dipendente" value=""/>
                <input type="text" name="cognome" placeholder="Cognome Dipendente" value=""/>
                <input type="text" name="settore" placeholder="Settore Dipendente" value=""/>
                <input type="submit" class="btn btn-primary btn-material-blue-700" name="cerca" value="Cerca Dipendente"/>
        </form>
        </div>
    </div>
        
        <button class="btn btn-primary btn-lg btn-block btn-material-blue-700" data-toggle="modal" data-target="#simple-dialog">Aggiungi Nuovo Dipendente</button>
    <div id="simple-dialog" class="modal fade" tabindex="-1">
            <div class="modal-dialog">
              <div class="modal-content">
                <div class="modal-body">
     <form method="POST"  class="form-horizontal"  action="">
         <fieldset>
                    <div class="form-group">
                        <div class="col-md-12">
                        <label for="name">Nome</label></br>
                        <input type="text" class="form-control floating-label login-field" id="focusedInput" placeholder="Nome Dipendente" type="text"  name="nome" value="<?php echo isset($dipendente['nome']) ? $dipendente['nome']: '' ?>"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-12">
                        <label for="name">Cognome</label></br>
                        <input type="text" class="form-control floating-label login-field" id="focusedInput" placeholder="Cognome Dipendente" type="text"  name="cognome" value="<?php echo isset($dipendente['cognome']) ? $dipendente['cognome']: '' ?>"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-12">
                        <label for="name">Settore</label></br>
                        <input type="text" class="form-control floating-label login-field" placeholder="Settore Dipendente"  name="settore"  value="<?php echo isset($dipendente['settore']) ? $dipendente['settore']: '' ?>"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-12">
                        <label for="name">Ruolo</label></br>
                        <input type="text" class="form-control floating-label login-field" placeholder="Ruolo Dipendente"  name="ruolo"  value="<?php echo isset($dipendente['ruolo']) ? $dipendente['ruolo']: '' ?>"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-12">
                        <label for="name">Telefono</label></br>
                        <input type="text" name="telefono" class="form-control floating-label login-field" placeholder="Telefono Dipendente"  value="<?php echo isset($dipendente['telefono']) ? $dipendente['telefono']: '' ?>"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-12">
                        <label for="name">Email</label>
                        <input type="text" name="email" class="form-control floating-label login-field" placeholder="Email Dipendente" value="<?php echo isset($dipendente['email']) ? $dipendente['email']: '' ?>"/>
                        </div>
                      </div>
                    <div class="form-group">
                        <div class="col-md-12">
                        <label for="name">Codice Fiscale</label>
                        <input type="text" name="codfiscale" class="form-control floating-label login-field" placeholder="Cod.Fiscale Dipendente" value="<?php echo isset($dipendente['codfiscale']) ? $dipendente['codfiscale']: '' ?>"/>
                        </div>
                      </div>
                    <div class="form-group">
                        <div class="col-md-12">
                        <label for="name">Password</label>
                        <input type="text" name="pass" class="form-control floating-label login-field" placeholder="Password Dipendente" value="<?php echo isset($dipendente['password']) ? $dipendente['password']: '' ?>"/>
                        </div>
                      </div>

        <input type="submit" class="btn btn-primary btn-lg btn-block btn-material-blue-700" name="submit" value="Aggiungi Dipendente"/>
        </fieldset>
    </form>
    </div>
              </div>
            </div>
          </div>
          <?php if( isset( $_SESSION[ 'message' ] ) ){
                        echo '<h1 style="color:green;text-align:center;">'.$_SESSION[ 'message' ].'</h1>';
                        unset( $_SESSION[ 'message' ] );
                    } ?>
<?php if( !empty( $dipendenti )):?>
<form method="POST" action="">
        <input type="submit" class="btn btn-primary btn-material-blue-700" name="update" value="modifica dipendente"/>
        <input type="submit" class="btn btn-primary btn-material-blue-700" name="delete" value="elimina dipendente"/>
    <div class="table-responsive">
    <table class="table table-striped">
        <thead> 
        <tr> 
            <th>#</th>
            <th>Dipendente</th>
            <th>Settore</th>
            <th>Ruolo</th>
            <th>Email</th>
            <th>Modifica</th>
            <th>Seleziona</th>
        </tr>
        </thead>    
        <tbody>
            <?php $indice = 1;?>

        <?php foreach($dipendenti as $dipendente):?>
        <tr>  
            <td>
                <?php echo $indice; $indice++; ?>
            </td> 
            <td>
                <input type="text" name="dipendenti[<?php echo $dipendente['id'];?>][name]" value="<?php echo isset($dipendente['name']) ? $dipendente['name']: '' ?>"/>
                <input type="text" name="dipendenti[<?php echo $dipendente['id'];?>][cognome]" value="<?php echo isset($dipendente['cognome']) ? $dipendente['cognome']: '' ?>"/>
            </td>
            <td>
                <input type="text" name="dipendenti[<?php echo $dipendente['id'];?>][settore]" value="<?php echo isset($dipendente['settore']) ? $dipendente['settore']: '' ?>"/>
            </td>
            <td>
                <input type="text" name="dipendenti[<?php echo $dipendente['id'];?>][ruolo]" value="<?php echo isset($dipendente['ruolo']) ? $dipendente['ruolo']: '' ?>"/>
            </td>
            <td>
                <input type="text" name="dipendenti[<?php echo $dipendente['id'];?>][email]" value="<?php echo isset($dipendente['email']) ? $dipendente['email']: '' ?>"/>
            </td>
            <td>
                <a href="dipendenti.php?action=detail&dipendente_id=<?php echo $dipendente['id']?>">
                            <input type="button" class="btn btn-lg btn-primary btn-material-blue-grey-500" value="visualizza">
                        </a>
            </td>
            <td>
                <div class="togglebutton">
                  <label>
                    <input type="checkbox" name="id" value="<?php echo isset($dipendente['id']) ? $dipendente['id']: ''?>"/>
                      
                    </label>
                  </div>
            </td>  
        </tr>    
                <?php endforeach;?> 
    </tbody>
    </table>
    </div>
        <input type="submit" class="btn btn-primary btn-material-blue-700" name="update" value="modifica dipendente"/>
        <input type="submit" class="btn btn-primary btn-material-blue-700" name="delete" value="elimina dipendente"/>
    </form>
<?php endif; ?>
</div>
</body>

<?php else: header('Location:./login.php');?>

<?php endif;?>
</html>