<!DOCTYPE html>
<html>
<?php if( $user_controller->isLogged() ): ?> 
<div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
	<div class="serch">
        <div class="col-md-3 col-sm-3">
            <h1 style="text-align:center;margin-top:6px;">Ricerca Rapida:</h1>
        </div>
        <div class="col-md-9  col-sm-9">
		<form action="" method="POST">
                <input type="text" name="nome" placeholder="Nome Servizio" value=""/>
                <input type="submit" class="btn btn-primary btn-material-blue-700" name="cerca" value="Cerca Servizio"/>
        </form>
        </div>
    </div>
        
        <button class="btn btn-primary btn-lg btn-block btn-material-blue-700" data-toggle="modal" data-target="#simple-dialog">Aggiungi Nuovo Servizio</button>
    <div id="simple-dialog" class="modal fade" tabindex="-1">
            <div class="modal-dialog">
              <div class="modal-content">
                <div class="modal-body">
     <form method="POST"  class="form-horizontal"  action="">
         <fieldset>
                    <div class="form-group">
                        <div class="col-md-12">
                        <label for="name">Nome</label></br>
                        <input type="text" class="form-control floating-label login-field" id="focusedInput" placeholder="Nome Servizio" type="text"  name="nome" value="<?php echo isset($dipendente['nome']) ? $dipendente['nome']: '' ?>"/>
                        </div>
                    </div>
        <input type="submit" class="btn btn-primary btn-lg btn-block btn-material-blue-700" name="submit" value="Aggiungi Servizio"/>
        </fieldset>
    </form>
    </div>
              </div>
            </div>
          </div>
          <?php if( isset( $_SESSION[ 'message' ] ) ){
                        echo '<h1 style="color:green;text-align:center;">'.$_SESSION[ 'message' ].'</h1>';
                        unset( $_SESSION[ 'message' ] );
                    } ?>
<?php if( !empty( $servizi )):?>
<form method="POST" action="">
        <input type="submit" class="btn btn-primary btn-material-blue-700" name="update" value="Modifica Servizio"/>
        <input type="submit" class="btn btn-primary btn-material-blue-700" name="delete" value="Elimina Servizio"/>
    <div class="table-responsive">
    <table class="table table-striped">
        <thead> 
        <tr> 
            <th>#</th>
            <th>Servizio</th>
        </tr>
        </thead>    
        <tbody>
            <?php $indice = 1;?>

        <?php foreach($servizi as $servizio):?>
        <tr>  
            <td>
                <?php echo $indice; $indice++; ?>
            </td> 
            <td>
                <input type="text" name="servizi[<?php echo $servizio['id'];?>][name]" value="<?php echo isset($servizio['name']) ? $servizio['name']: '' ?>"/>
            </td>
            <td>
                <div class="togglebutton">
                  <label>
                    <input type="checkbox" name="id" value="<?php echo isset($servizio['id']) ? $servizio['id']: ''?>"/>
                      
                    </label>
                  </div>
            </td>  
        </tr>    
                <?php endforeach;?> 
    </tbody>
    </table>
    </div>
        <input type="submit" class="btn btn-primary btn-material-blue-700" name="update" value="Modifica Servizio"/>
        <input type="submit" class="btn btn-primary btn-material-blue-700" name="delete" value="Elimina Servizio"/>
    </form>
<?php endif; ?>
</div>
</body>

<?php else: header('Location:./login.php');?>

<?php endif;?>
</html>