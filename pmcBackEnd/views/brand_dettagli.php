<?php
$stato_servizi[] = 'Saldato Acconto';
$stato_servizi[] = 'Saldato Totale';
$stato_servizi[] = 'Non Saldato';
include('./mail/adminSendMail.php');
?>
<!DOCTYPE html>
<?php if( $user_controller->isLogged() ): ?> 
  <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main custom-main">
    

          <div class="row placeholders custom-desc">
            <div class="col-md-12 col-xs-12 col-sm-12 placeholder">
              <?php foreach ($brand_dati as $brand): ?>
              <h1 class="page-header"><?php echo htmlspecialchars(stripcslashes($brand['azienda']));?>(Controlli sull update da fare)</h1>
            </div>
          </div>
          <form action="" class="form-signin" method="POST">
              <h3 class="text-left">info:</h3>
              <label  for="password">Telefono:</label>
            <input type="text" autofocus=""  placeholder="Non disponibile" class="form-control" name="telefono" value="<?php echo isset($brand['telefono']) ? $brand['telefono']: '' ?>"/></br>
            <label  for="password">Email:</label>
            <input type="text" autofocus=""  placeholder="Non disponibile" class="form-control" name="email" value="<?php echo isset($brand['email']) ? $brand['email']: '' ?>"/></br>
            <label  for="password">Descrizione dell'attivit&agrave;:</label>
              <p class="text-left"><textarea type="text" name="descrizione"><?php echo htmlspecialchars(stripcslashes($brand['descrizione']));?></textarea></p></br>
              <input type="submit" name="update" value="modifica" class="btn btn-primary btn-lg btn-block btn-material-blue-700">
          </form>
          <div class="message">
          <?php if( isset( $_SESSION[ 'message' ] ) ){
                        echo '<h1 text-align:center;">'.$_SESSION[ 'message' ].'</h1>';
                        unset( $_SESSION[ 'message' ] );
                    } ?>
                  </div>
            <?php endforeach;?>
            <h2 class="text-left">Servizi</h2>
  <button class="btn btn-primary btn-lg btn-block btn-material-blue-700" data-toggle="modal" data-target="#simple-dialog">Aggiungi Nuovo Servizio</button>
    <div id="simple-dialog" class="modal fade" tabindex="-1">
            <div class="modal-dialog">
              <div class="modal-content">
                <div class="modal-body">
     <form method="POST"  class="form-horizontal"  action="">
         <fieldset>
                    <div class="form-group">
                        <div class="col-md-12">
                        <label for="name">Nome</label></br>
                        <input type="hidden" name="brand_id" value="<?php echo isset($id) ? $id: '' ?>"/></br>
                        <select class="form-control floating-label login-field" name="servizi_id">
                          <?php foreach ($servizi as $servizio):?>
                            <option value="<?php echo $servizio['id'];?>"><?php echo $servizio['name'];?></option> 
                          <?php endforeach;?> 
                        </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-12">
                        <label for="name">Prezzo</label></br>
                        <input type="number" name="prezzo" class="form-control floating-label login-field" placeholder="Prezzo" value=""/>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-12">
                        <label for="name">Stato Pagamento</label></br>
                        <select class="form-control floating-label login-field" name="stato_pag">
                          <?php foreach ($stato_servizi as $key => $stato_servizio) :?>
                            <option value="<?php echo $stato_servizio ?>"><?php echo $stato_servizio;?></option>
                          <?php endforeach;?>
                        </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-12">
                        <label for="name">Data</label>
                        <input  type="datapilcker" value="<?php echo date ('d/m/Y');?>" class="form-control floating-label login-field" placeholder="Data" name="data_inizio">
                        </div>
                      </div>
                    <div class="form-group">
                        <div class="col-md-12">
                        <div class="togglebutton">
                  <label>Stato Servizio
                    <input type="checkbox" checked="" name="stato_serv" value="attivo"/>
                    </label>
                  </div>
                        </div>
                    </div>
        <input type="submit" class="btn btn-primary btn-lg btn-block btn-material-blue-700" name="submit" value="aggiungi"/>
        </fieldset>
    </form>
    </div>
              </div>
            </div>
          </div>
<?php if(!empty($servizi_attivi)):?>
  <div class="row placeholders custom-desc">
            <div class="col-md-12 col-xs-12 col-sm-12 placeholder">
  <form method="POST" action="">
    <input type="submit" name="update2" class="btn btn-primary btn-material-blue-700" value="modifica stato">
  <input type="submit" name="del" class="btn btn-primary btn-material-blue-700" value="elimina servizio">
  <div class="table-responsive">
    <table class="table table-striped">  
        <thead> 
        <tr> 
            <th>Servizi Attivi</th>
            <th>Prezzo</th>
            <th>Stato Pagamento</th>
            <th>Data inizio</th>
            <th>Stato Servizio</th>
            <th>Seleziona</th>
        </tr>
        </thead>    
    <?php foreach ($servizi_attivi as $servizio_attivo) :?>
    <tbody>  
      <?php foreach ($servizi as $servizio) :?>
        <?php if($servizio['id']==$servizio_attivo['servizi_id']):?>
        <tr>        
            <td>
               
                <?php echo htmlspecialchars(stripslashes($servizio['name']))?>
            </td>
            <td>
                <input type="number" step="10.0" name="servizi_stato[<?php echo $servizio_attivo['id'];?>][prezzo]" value="<?php echo isset($servizio_attivo['prezzo']) ? $servizio_attivo['prezzo']: '' ?>"/>
            </td>
            <td>

                <select name="servizi_stato[<?php echo $servizio_attivo['id'];?>][stato_pag]">
                   <?php foreach ($stato_servizi as $key => $stato_servizio) :?>
                  <option value="<?php echo $stato_servizio ?>" <?php echo ($servizio_attivo['stato_pag']==$stato_servizio) ? 'selected': '' ?>><?php echo $stato_servizio;?></option>
                  <?php endforeach;?>
                </select> 
              
                
            </td>
            <td>
                <input type="text" name="servizi_stato[<?php echo $servizio_attivo['id'];?>][data_inizio]" value="<?php echo isset($servizio_attivo['data_inizio']) ? $servizio_attivo['data_inizio']: '' ?>"/>
            </td>
            <td>
              <input type="hidden" name="servizi_stato[<?php echo $servizio_attivo['id'];?>][data_fine]" value="<?php echo date("d-m-Y") ?>"/></br>
              <select name="servizi_stato[<?php echo $servizio_attivo['id'];?>][stato_serv]">
                  <option value="<?php echo isset($servizio_attivo['stato_serv']) ?$servizio_attivo['stato_serv']: '' ?>"><?php echo $servizio_attivo['stato_serv'];?></option>
                  <option value="passato">Termina Servizio</option>
                </select> 
            <td>
              <td>
              <div class="togglebutton">
                  <label>
                    <input type="checkbox" name="id" value="<?php echo isset($servizio_attivo['id']) ? $servizio_attivo['id']: ''?>"/>
            
                    </label>
                  </div>
            </td>
        </tr>  
        <?php endif;?> 
    <?php endforeach;?> 
        </tbody>
     
    <?php endforeach;?> 
      </table>
    </div>
      <input type="submit" name="update2" class="btn btn-primary btn-material-blue-700" value="modifica stato">
  <input type="submit" name="del" class="btn btn-primary btn-material-blue-700" value="elimina servizio">
  </form> 
   </div>
  </div>
<?php endif; ?>
<?php if( !empty($servizi_passati) ):?>
   <div class="row placeholders custom-desc">
            <div class="col-md-12 col-xs-12 col-sm-12 placeholder">
  <form method="POST" action="" style=""> 
     <input type="submit" name="update2" class="btn btn-primary btn-material-blue-700" value="modifica stato">
  <input type="submit" name="del" class="btn btn-primary btn-material-blue-700" value="elimina servizio">
    <div class="table-responsive">
    <table class="table table-striped">
        <thead> 
        <tr> 
            <th>Servizi Passati</th>
            <th>Prezzo</th>
            <th>Stato Pagamento</th>
            <th>Data inizio</th>
            <th>Data Fine</th>
            <th>Stato Servizio</th>
            <th>Seleziona</th>
        </tr>
        </thead> 
    <?php foreach ($servizi_passati as $servizio_stato) :?>
    <tbody>
      <?php foreach ($servizi as $servizio) :?>
        <?php if($servizio['id']==$servizio_stato['servizi_id']):?>
        <tr>        
            <td>
                <?php echo htmlspecialchars(stripslashes($servizio['name']))?>
            </td>
            <td>
                <input type="number" step="10.0" name="servizi_stato[<?php echo $servizio_stato['id'];?>][prezzo]" value="<?php echo isset($servizio_stato['prezzo']) ? $servizio_stato['prezzo']: '' ?>"/>
            </td>
            <td>
                 <select name="servizi_stato[<?php echo $servizio_stato['id'];?>][stato_pag]">
                   <?php foreach ($stato_servizi as $key => $stato_servizio) :?>
                  <option value="<?php echo $stato_servizio ?>" <?php echo ($servizio_stato['stato_pag']==$stato_servizio) ? 'selected': '' ?>><?php echo $stato_servizio;?></option>
                  <?php endforeach;?>
                </select> 
            </td>
            <td>
                <input type="text" name="servizi_stato[<?php echo $servizio_stato['id'];?>][data_inizio]" value="<?php echo isset($servizio_stato['data_inizio']) ? $servizio_stato['data_inizio']: '' ?>"/>
              

            </td>
            <td>
              <?php echo htmlspecialchars(stripslashes($servizio_stato['data_fine']))?>
            </td>
            <td>
             <select name="servizi_stato[<?php echo $servizio_stato['id'];?>][stato_serv]">
                  <option value="<?php echo isset($servizio_stato['stato_serv']) ? $servizio_stato['stato_serv']: '' ?>"><?php echo $servizio_stato['stato_serv'];?></option>
                  <option value="attivo">Ripristina Servizio</option>
                </select> 
            <td>
              <td>
              <div class="togglebutton">
                  <label>
                    <input type="checkbox" name="id" value="<?php echo isset($servizio_stato['id']) ? $servizio_stato['id']: ''?>"/>
            
                    </label>
                  </div>
            </td>
        </tr>  
        <?php endif;?> 
    <?php endforeach;?>
     </tbody>
  <?php endforeach;?> 
      </table>
    </div>
  <input type="submit" name="update2" class="btn btn-primary btn-material-blue-700" value="modifica stato">
  <input type="submit" name="del" class="btn btn-primary btn-material-blue-700" value="elimina servizio">
  </form> 
   </div>
  </div>
<?php endif;?> 
<?php if(!empty($dipendenti)):?>
     <h3 class="text-left">Task</h3>
  <button class="btn btn-primary btn-lg btn-block btn-material-blue-700" data-toggle="modal" data-target="#simple-dialog2">Aggiungi Task</button>
     <div id="simple-dialog2" class="modal fade" tabindex="-1">
            <div class="modal-dialog">
              <div class="modal-content">
                <div class="modal-body">
     <form method="POST"  class="form-horizontal"  action="">
         <fieldset>
                    <div class="form-group">
                        <div class="col-md-12">
                        <label for="name">Titolo</label></br>
                         <input type="hidden" name="brand_id" value="<?php echo isset($id) ? $id: '' ?>"/>
                        <input type="text" name="titolo" class="form-control floating-label login-field" placeholder="Titolo" value="<?php echo isset($task['titolo']) ? $task['titolo']: '' ?>"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-12">
                        <label for="name">Descrizione</label>
                        <textarea type="text" class="form-control floating-label login-field" placeholder="Descrizione" name="descrizione" value="<?php echo isset($task['descrizione']) ? $task['descrizione']: '' ?>"/></textarea>
                        </div>
                      </div>
        <input type="submit" class="btn btn-primary btn-lg btn-block btn-material-blue-700" name="submit2" value="aggiungi"/>
        </fieldset>
    </form>
    </div>
              </div>
            </div>
          </div>
<?php if(!empty($tasks)):?>
<button class="btn btn-primary btn-lg btn-block btn-material-blue-700" data-toggle="modal" data-target="#simple-dialog3">assegna Task a dipendente</button>
     <div id="simple-dialog3" class="modal fade" tabindex="-1">
            <div class="modal-dialog">
              <div class="modal-content">
                <div class="modal-body">
     <form method="POST"  class="form-horizontal"  action="">
         <fieldset>
          <div class="form-group">
                        <div class="col-md-12">
                        <label for="name">Destinatario</label></br>
                  <?php foreach ($dipendenti as $dipendente):?>
                  <div class="togglebutton">
                    <label> 
                  <?php echo $dipendente['name']." ".$dipendente['cognome'];?>
                  <input type="checkbox" name="dipendenti[<?php echo $dipendente['id'];?>]" value="<?php echo isset($dipendente['id']) ? $dipendente['id']: ''?>">
        
                  <input type="hidden" value="<?php echo $dipendente['email'];?>" name="email<?php echo $dipendente['id'];?>">
                  <input type="hidden" value="<?php echo $dipendente['name'];?>" name="name_dest<?php echo $dipendente['id'];?>">
                  </label>
                </div>
                <?php endforeach;?>
                    </div>
                  </div>
                    <div class="form-group">
                        <div class="col-md-12">
                        <label for="name">Task Assegnata</label></br>
                  <?php foreach ($tasks as $task):?>
                  <div class="togglebutton">
                    <label> 
                  <?php echo $task['titolo'];?>
                  <input type="checkbox" name="task[<?php echo $task['id'];?>]" value="<?php echo isset($task['id']) ? $task['id']: ''?>">
          
                  </label>
                </div>
                  <?php endforeach;?>  
                        </div>
                    </div>
                    <input type="submit" class="btn btn-primary btn-lg btn-block btn-material-blue-700" name="assign" value="assegna"/>
        </fieldset>
    </form>
    </div>
              </div>
            </div>
          </div>
   
  <form action="" method="POST">
   <div class="row placeholders custom-desc">
              <div class="col-md-12 col-xs-12 col-sm-12 placeholder">
    <input type="submit" name="update3" value="modifica task" class="btn btn-primary btn-material-blue-700" data-toggle="modal" />
    <input type="submit" name="del2" value="elimina task" class="btn btn-primary btn-material-blue-700" data-toggle="modal"/>
    <input type="submit" name="ripristina" value="Ripristina" class="btn btn-primary btn-material-blue-700" data-toggle="modal">
  </div></div>
  <div class="row placeholders custom-desc">
              <div class="col-md-12 col-xs-12 col-sm-12 placeholder">
<?php $collapseList = 0; ?>
                  
<?php foreach ($tasks as $task):?>

  <?php  $collapseList++;?>
  <div class="col-md-2 col-xs-2 col-sm-2 placeholder">
                <div class="checkbox">
                  <label>
                    <input type="checkbox" name="id" value="<?php echo isset($task['id']) ? $task['id']: ''?>"/>
                    <span class="ripple"></span>
                     Checkbox
                  </label>
                </div>
   
</div>
 <div class="col-md-10 col-xs-10 col-sm-10 placeholder">               
                <!-- ACCORDION TASK -->

                <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                  

                  <div class="panel panel-default">
                    <?php if( $task['stato']== 'completata'):?>
                    <div class="panel-heading" style="background-color: #62B864;" role="tab" id="headingOne<?php echo $collapseList;?>">
                      <h4 class="panel-title">
                        <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne<?php echo $collapseList;?>" aria-expanded="" aria-controls="collapseOne<?php echo $collapseList;?>">
                          <input type="text" name="tasks[<?php echo $task['id']?>][titolo]" value="<?php echo isset($task['titolo']) ? $task['titolo']: '' ?>"/>
                        </a>
                      </h4>
                    </div>
                    <?php elseif ( $task['stato'] !== 'completata' && !empty($task['data_scadenza']) && $task['data_scadenza'] >= date('d/m/y') ) :
                     foreach ($assign as  $dipendenteinfo):
                          if($dipendenteinfo['id_task']==$task['id']):
                            $mail = new AdminSendMail( $admin[0]['email'],$task['titolo'],$dipendenteinfo['name'],$dipendenteinfo['id'] );
                             $mail->Scadenza();
                            endif;endforeach;
                          ?>
                          

                  <div class="panel-heading" style="background-color: red;" role="tab" id="headingOne<?php echo $collapseList;?>">
                      <h4 class="panel-title">
                        <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne<?php echo $collapseList;?>" aria-expanded="" aria-controls="collapseOne<?php echo $collapseList;?>">
                          <input type="text" name="tasks[<?php echo $task['id']?>][titolo]" value="<?php echo isset($task['titolo']) ? $task['titolo']: '' ?>"/>
                        </a>
                      </h4>
                    </div>
                  <?php else:?>
                  <div class="panel-heading" role="tab" id="headingOne<?php echo $collapseList;?>">
                      <h4 class="panel-title">
                        <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne<?php echo $collapseList;?>" aria-expanded="" aria-controls="collapseOne<?php echo $collapseList;?>">
                          <input type="text" name="tasks[<?php echo $task['id']?>][titolo]" value="<?php echo isset($task['titolo']) ? $task['titolo']: '' ?>"/>
                        </a>
                      </h4>
                    </div>
                <?php endif;?>
    <div id="collapseOne<?php echo $collapseList;?>" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
                      <div class="panel-body">
                        <div class="col-md-6">
                        <label for="name">Data Inizio</label>
                        <input type="text" autofocus=""  placeholder="Non disponibile" class="form-control" name="tasks[<?php echo $task['id']?>][data_inizio]" value="<?php echo isset($task['data_inizio']) ? $task['data_inizio']: '' ?>"/>
                        </div>
                        <div class="col-md-6">
                        <label for="name">Data Scadenza</label>
                        <input type="text" autofocus=""  placeholder="Non disponibile" class="form-control" name="tasks[<?php echo $task['id']?>][data_scadenza]" value="<?php echo isset($task['data_scadenza']) ? $task['data_scadenza']: '' ?>"/>
                        </div>
                        <label for="name">Appunti</label>
                        <textarea type="text" name="tasks[<?php echo $task['id']?>][descrizione]" value="<?php echo isset($task['descrizione']) ? $task['descrizione']: '' ?>"/><?php echo $task['descrizione'];?></textarea>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
               <?php endforeach; ?>
            </div>
                <input type="submit" name="update3" value="modifica task" class="btn btn-primary btn-material-blue-700" data-toggle="modal" />
                <input type="submit" name="del2" value="elimina task" class="btn btn-primary btn-material-blue-700" data-toggle="modal"/>
                <input type="submit" name="ripristina" value="Ripristina" class="btn btn-primary btn-material-blue-700" data-toggle="modal">
              </div>
            </div>
            </form>
  </div> 
    <?php endif; ?>
  <?php else: echo '<h1>Non ci sono Dipendenti a cui assegnare le Task</h1>'?>
<?php endif;?>
<?php endif;?>
</html>
