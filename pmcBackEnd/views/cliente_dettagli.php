<!DOCTYPE html>
<html>  
 <?php if( $user_controller->isLogged() ): ?> 
<div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
     <h1 style="text-align:center;"><?php echo htmlspecialchars(stripslashes($cliente[0]['name'])).' '.htmlspecialchars(stripslashes($cliente[0]['cognome']));?>(Controlli sull update da fare)</h1>
    <form method="POST" action="" class="form-signin">
            <h2 class="form-signin-heading">Info:</h2>
            <label  for="usermane">Indirizzo:</label>
            <input type="text" autofocus="" placeholder="Non disponibile" class="form-control" value="<?php echo isset($cliente[0]['indirizzo']) ? $cliente[0]['indirizzo']: '' ?>" name="indirizzo"/><br/>
            <label  for="password">Partita Iva:</label>
            <input type="text" autofocus=""  placeholder="Non disponibile" class="form-control" name="iva" value="<?php echo isset($cliente[0]['iva']) ? $cliente[0]['iva']: '' ?>"/></br>
            <label  for="password">Telefono:</label>
            <input type="text" autofocus=""  placeholder="Non disponibile" class="form-control" name="telefono" value="<?php echo isset($cliente[0]['telefono']) ? $cliente[0]['telefono']: '' ?>"/></br>
            <label  for="password">Email:</label>
            <input type="text" autofocus=""  placeholder="Non disponibile" class="form-control" name="email" value="<?php echo isset($cliente[0]['email']) ? $cliente[0]['email']: '' ?>"/></br>
            <input type="submit" name="update" value="modifica" class="btn btn-lg btn-primary btn-block btn-material-blue-700">
        </form>
    <?php if( isset( $_SESSION[ 'message' ] ) ){
                        echo '<h1 style="color:green;text-align:center;">'.$_SESSION[ 'message' ].'</h1>';
                        unset( $_SESSION[ 'message' ] );
                    } ?>
    <h1 class="page-header">Dashboard</h1>
     <div class="row placeholders">
            <div class="col-xs-6 col-sm-3 placeholder">
              <img data-src="holder.js/200x200/auto/sky" class="img-responsive" alt="200x200" src="data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9InllcyI/PjxzdmcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB3aWR0aD0iMjAwIiBoZWlnaHQ9IjIwMCIgdmlld0JveD0iMCAwIDIwMCAyMDAiIHByZXNlcnZlQXNwZWN0UmF0aW89Im5vbmUiPjxkZWZzLz48cmVjdCB3aWR0aD0iMjAwIiBoZWlnaHQ9IjIwMCIgZmlsbD0iIzBEOEZEQiIvPjxnPjx0ZXh0IHg9Ijc1LjUiIHk9IjEwMCIgc3R5bGU9ImZpbGw6I0ZGRkZGRjtmb250LXdlaWdodDpib2xkO2ZvbnQtZmFtaWx5OkFyaWFsLCBIZWx2ZXRpY2EsIE9wZW4gU2Fucywgc2Fucy1zZXJpZiwgbW9ub3NwYWNlO2ZvbnQtc2l6ZToxMHB0O2RvbWluYW50LWJhc2VsaW5lOmNlbnRyYWwiPjIwMHgyMDA8L3RleHQ+PC9nPjwvc3ZnPg==" data-holder-rendered="true">
              <h4>brand raggiunti=<?php echo count($brands);?></h4>
            </div>
            <div class="col-xs-6 col-sm-3 placeholder">
              <img data-src="holder.js/200x200/auto/vine" class="img-responsive" alt="200x200" src="data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9InllcyI/PjxzdmcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB3aWR0aD0iMjAwIiBoZWlnaHQ9IjIwMCIgdmlld0JveD0iMCAwIDIwMCAyMDAiIHByZXNlcnZlQXNwZWN0UmF0aW89Im5vbmUiPjxkZWZzLz48cmVjdCB3aWR0aD0iMjAwIiBoZWlnaHQ9IjIwMCIgZmlsbD0iIzM5REJBQyIvPjxnPjx0ZXh0IHg9Ijc1LjUiIHk9IjEwMCIgc3R5bGU9ImZpbGw6IzFFMjkyQztmb250LXdlaWdodDpib2xkO2ZvbnQtZmFtaWx5OkFyaWFsLCBIZWx2ZXRpY2EsIE9wZW4gU2Fucywgc2Fucy1zZXJpZiwgbW9ub3NwYWNlO2ZvbnQtc2l6ZToxMHB0O2RvbWluYW50LWJhc2VsaW5lOmNlbnRyYWwiPjIwMHgyMDA8L3RleHQ+PC9nPjwvc3ZnPg==" data-holder-rendered="true">
              <?php $num_serv_att = 0;?>
                 <?php foreach ($dati_brand as $dato):?>
                    <?php if($dato['clienti_id']==$id && $dato['stato_serv'] =='attivo'):?>
                        <?php $num_serv_att++;?>
                    <?php endif;?>
                <?php endforeach;?>
                <h4>Servizi attivi = <?php echo  $num_serv_att;?></h4>
            </div>
          </div>
    <h2 style="text-align:center;">Lista Brand:</h2>
    <div class="serch">
        <div class="col-md-3 col-sm-3">
            <h1 style="text-align:center;margin-top:6px;">Ricerca Rapida:</h1>
        </div>
        <div class="col-md-9  col-sm-9">
        <form action="" method="POST">
                <input type="text" name="azienda" placeholder="Nome Azienda" value=""/>
                <input type="text" name="settore" placeholder="Settore Azienda" value=""/>
                <input type="text" name="email" placeholder="Email" value=""/>
                <input type="submit" class="btn btn-primary btn-material-blue-700" name="cerca" value="Cerca Brand"/>
        </form>
        </div>
    </div>
    <button class="btn btn-lg btn-primary btn-block btn-material-blue-700" data-toggle="modal" data-target="#simple-dialog">Aggiungi Nuovo Brand</button>
    <div id="simple-dialog" class="modal fade" tabindex="-1">
            <div class="modal-dialog">
              <div class="modal-content">
                <div class="modal-body">
     <form method="POST"  class="form-horizontal"  action="">
         <fieldset>
                    <div class="form-group">
                        <div class="col-md-12">
                        <label for="name">Nome</label></br>
                        <input type="hidden" name="cliente_id" value="<?php echo isset($cliente[0]['id']) ? $cliente[0]['id']: '' ?>"/>
                        <input type="text" class="form-control floating-label login-field" id="focusedInput" placeholder="Nome Brand" type="text" id="login-azienda" name="bazienda" value="<?php echo isset($brand['bazienda']) ? $brand['bazienda']: '' ?>"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-12">
                        <label for="name">Settore</label></br>
                        <input type="text" class="form-control floating-label login-field" placeholder="Settore Brand"  name="bsettore"  value="<?php echo isset($brand['bsettore']) ? $brand['bsettore']: '' ?>"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-12">
                        <label for="name">Telefono</label></br>
                        <input type="text" name="btelefono" class="form-control floating-label login-field" placeholder="Telefono Brand"  value="<?php echo isset($brand['btelefono']) ? $brand['btelefono']: '' ?>"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-12">
                        <label for="name">Email</label>
                        <input type="text" name="bemail" class="form-control floating-label login-field" placeholder="Email Brand" value="<?php echo isset($brand['bemail']) ? $brand['bemail']: '' ?>"/>
                        </div>
                      </div>
                    <div class="form-group">
                        <div class="col-md-12">
                        <label for="name">Descrizione</label></br>
                        <textarea type="text" name="bdescrizione" class="form-control floating-label login-field" placeholder="Descrizione Brand"  value="<?php echo isset($brand['bdescrizione']) ? $brand['bdescrizione']: '' ?>"/></textarea>
                        </div>
                    </div>
        <input type="submit" class="btn btn-primary btn-lg btn-block btn-material-blue-700" name="submit" value="aggiungi brand"/>
        </fieldset>
    </form>
    </div>
              </div>
            </div>
          </div>
    <?php if(!empty($brands)):?>
    <form method="POST" action="">
        <input type="submit" class="btn btn-primary btn-material-blue-700" name="update2" value="modifica brand"/>
        <input type="submit" class="btn btn-primary btn-material-blue-700" name="del" value="elimina brand"/>
    <div class="table-responsive">
    <table class="table table-striped">
        <thead> 
        <tr> 
            <th>#</th>
            <th>Azienda</th>
            <th>Settore</th>
            <th>Email</th>
            <th>Servizi Attivi</th>
            <th>Modifica</th>
            <th>Seleziona Cliente</th>
        </tr>
        </thead>    
        <tbody>
            <?php $indice = 0; ?>
            <?php foreach ($brands as $brand):?>
        <tr> 
            <td>
                <?php  $indice ++; echo $indice; ?>
            </td>       
            <td>
                <input type="text" name="brands[<?php echo $brand['id'];?>][azienda]" value="<?php echo isset($brand['azienda']) ? $brand['azienda']: '' ?>"/>
            </td>
            <td>
                <input type="text" name="brands[<?php echo $brand['id'];?>][settore]" value="<?php echo isset($brand['settore']) ? $brand['settore']: '' ?>"/>
            </td>
            <td>
                <input type="text" name="brands[<?php echo $brand['id'];?>][email]" value="<?php echo isset($brand['email']) ? $brand['email']: '' ?>"/>
            </td>
            <td>
                <?php $num=0;?>
                <?php foreach ($dati_brand as $dato):?>
                    <?php if($dato['brand_id']==$brand['id']&&$dato['stato_serv']=='attivo'):?>
                        <?php $num++;?>
                    <?php endif;?>
                <?php endforeach;?>
                 <?php echo $num;?>
            </td>
            <td>
                <a href="brand.php?action=detail&brand_id=<?php echo $brand['id']?>">
                             <input type="button" class="btn btn-lg btn-primary btn-material-blue-grey-500" value="visualizza">
                        </a>
            </td>
            <td>
                <div class="togglebutton">
                  <label>Seleziona Cliente
                    <input type="checkbox" name="id" value="<?php echo isset($brand['id']) ? $brand['id']: ''?>"/>
                      
                    </label>
                  </div>
            </td>  
        </tr>  
        <?php endforeach;?>   
        </tbody>
    </table>
    <input type="submit" class="btn btn-primary btn-material-blue-700" name="update2" value="modifica brand"/>
        <input type="submit" class="btn btn-primary btn-material-blue-700" name="del" value="elimina brand"/>
    </form>
       
    <?php endif;?>   
</div>             
</body>
 <?php endif;?> 
</html>	